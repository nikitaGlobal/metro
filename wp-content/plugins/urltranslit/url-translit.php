<?php
/*
Plugin Name: UrlTranslit
Description: This plugin converts Cyrillic characters in post slugs to Latin characters. Very useful for Russian-speaking users of WordPress. You can use this plugin for creating human-readable links.
Author: SAYri
Author URI: https://sayri.work/
Version: 0.1
*/

$iso = array(
	"Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"#","є"=>"ye","ѓ"=>"g",
	"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
	"Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
	"З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
	"М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
	"С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"H",
	"Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
	"Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
	"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
	"е"=>"e","ё"=>"yo","ж"=>"zh",
	"з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
	"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	"ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
	"ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	"—"=>"-","«"=>"","»"=>"","…"=>""
);

function sanitize_title_with_translit( $title ) {
	global $iso;

	return strtr( $title, $iso );
}

add_action('sanitize_title', 'sanitize_title_with_translit', 0);
