<?php
namespace Cf7\Bitrix\Lead\Generation\Admin;

use Cf7\Bitrix\Lead\Generation\Includes\Bootstrap;
use Cf7\Bitrix\Lead\Generation\Includes\CrmFields;

class ContactSettings
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {

    }

    public function render($meta)
    {
        $renderFields = new RenderFields('contact');
        $currentValues = isset($meta['contact']) ? $meta['contact'] : [];
        ?>
        <table class="form-table">
            <?php
            $contactFields = (array) get_option(Bootstrap::CONTACT_FIELDS_KEY);
            $crmFields = new CrmFields();
            $crmFields = $crmFields->fields;

            foreach ($contactFields as $key => $field) {
                // Not show fields
                if (in_array($key, CrmFields::$breakFields['contact'])) {
                    continue;
                }

                // Not show read only fields
                if ($field['isReadOnly'] === true) {
                    continue;
                }

                $title = isset($crmFields[$key])
                    ? $crmFields[$key]
                    : (
                    isset($field['formLabel'])
                        ? $field['formLabel']
                        : $key
                    );
                ?>
                <tr>
                    <th>
                        <?php echo esc_html($title . ' (' . $key . ')'); ?>
                        <?php echo $field['isRequired'] ? '<span style="color:red;"> * </span>' : ''; ?>
                    </th>
                    <td>
                        <?php
                        $currentValue = isset($currentValues[$key]) ? $currentValues[$key] : '';
                        $currentValuePopulate = isset($currentValues[$key . '-populate'])
                            ? $currentValues[$key . '-populate']
                            : '';

                        if ($field['type'] === 'enumeration' && !empty($field['items'])) {
                            $selectItems = [];

                            foreach ($field['items'] as $item) {
                                $selectItems[$item['ID']] = $item['VALUE'];
                            }

                            $renderFields->selectField(
                                $selectItems,
                                $key,
                                $title,
                                $currentValue
                            );
                        } elseif ($field['type'] === 'char' || $field['type'] === 'boolean') {
                            $renderFields->inputCheckboxField(
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'TYPE_ID') {
                            $renderFields->statusField(
                                'CONTACT_TYPE',
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'SOURCE_ID') {
                            $renderFields->statusField(
                                'SOURCE',
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'HONORIFIC') {
                            $renderFields->statusField(
                                'HONORIFIC',
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'CURRENCY_ID') {
                            $renderFields->selectField(
                                get_option(Bootstrap::CURRENCY_LIST_KEY),
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif (in_array($key, ['COMMENTS', 'SOURCE_DESCRIPTION', 'STATUS_DESCRIPTION'])) {
                            $renderFields->textareaField(
                                $key,
                                $title,
                                $currentValue
                            );
                        } else {
                            $renderFields->inputTextField(
                                $key,
                                $title,
                                $currentValue
                            );
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
}
