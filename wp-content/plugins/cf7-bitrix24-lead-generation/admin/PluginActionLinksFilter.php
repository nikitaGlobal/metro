<?php
namespace Cf7\Bitrix\Lead\Generation\Admin;

class PluginActionLinksFilter
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_filter('plugin_action_links', [$this, 'pluginActionLinks'], 10, 2);
    }

    public function pluginActionLinks($actions, $pluginFile)
    {
        if (strpos($pluginFile, 'cf7-bitrix24-lead-generation.php') === false) {
            return $actions;
        }

        $settingsLink = '<a href="'
            . admin_url()
            . 'admin.php?page=wpcf7-integration&service=cf7-bitrix-lead-generation&action=setup">'
            . esc_html__('Settings', 'cf7-bitrix24-lg')
            . '</a>';

        $documentationLink = '<a href="'
            . esc_url(
                __(
                    'https://itgalaxy.company/en/software/contact-form-7-bitrix24-crm-integration/contact-form-7-bitr'
                    . 'ix24-crm-integration-instructions/',
                    'cf7-bitrix24-lg'
                )
            )
            . '" target="_blank">'
            . esc_html__('Documentation', 'cf7-bitrix24-lg')
            . '</a>';


        array_push(
            $actions,
            $settingsLink,
            $documentationLink
        );

        return $actions;
    }
}
