<?php
namespace Cf7\Bitrix\Lead\Generation\Admin;

use Cf7\Bitrix\Lead\Generation\Includes\Bootstrap;
use Cf7\Bitrix\Lead\Generation\Includes\Bitrix24;

class CF7 extends \WPCF7_Service
{
    private static $instance = false;

    public static $serviceName = 'cf7-bitrix-lead-generation';

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        // add links to plugin list
        PluginActionLinksFilter::getInstance();

        add_action('wpcf7_init', [$this, 'registerService']);

        add_action('wp_ajax_cf7Bitrix24AjaxSaveSettings', [$this, 'cf7Bitrix24AjaxSaveSettings']);

        if ($this->is_active()) {
            add_filter('wpcf7_editor_panels', [$this, 'settingsPanels']);
            add_action('save_post_' . \WPCF7_ContactForm::post_type, [$this, 'saveSettings']);

            if (isset($_GET['page']) && $_GET['page'] === 'wpcf7' && !empty($_GET['post'])) {
                add_action('admin_enqueue_scripts', function () {
                    wp_enqueue_script('jquery-ui-tabs');
                    wp_enqueue_style('jquery-ui-tabs', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', false, '1.8.8');
                });

                add_action('admin_footer', function () {
                    ?>
                    <script>
                        jQuery(document).on("ready", function () {
                            jQuery("#cf7-bx-tabs").tabs();
                        });
                    </script>
                    <?php
                }, PHP_INT_MAX);
            }
        }

        if (!empty($_GET['page'])
            && $_GET['page'] === 'wpcf7-integration'
            && !empty($_GET['service'])
            && $_GET['service'] === self::$serviceName
        ) {
            add_action('admin_footer', function () {
                ?>
                <script>
                    jQuery(document).on("ready", function () {
                        jQuery('[data-ui-component="cf7-bitrix24-save-settings"]').on('click', function () {
                            var $ = jQuery;
                            var $element = $(this);
                            var ajaxUrl = typeof ajaxurl !== 'undefined' ? ajaxurl : '/wp-admin/admin-ajax.php';

                            $element.attr('disabled', 'true');

                            $('[data-ui-component="cf7bitrix24noticesettings"]').remove();

                            $.post(ajaxUrl, {
                                action: 'cf7Bitrix24AjaxSaveSettings',
                                webhook: $('[name="webhook"]').val(),
                                enabled_logging: $('[name="enabled_logging"]').is(':checked') ? 1 : 0,
                            })
                                .success(function (response) {
                                    $element.removeAttr('disabled');
                                    $('div#cf7-bitrix-lead-generation').before(response);
                                });

                            return false;
                        });
                    });
                </script>
                <?php
            }, PHP_INT_MAX);
        }
    }

    public function cf7Bitrix24AjaxSaveSettings()
    {
        $response = '';
        $webhook = isset($_POST['webhook']) ? trim(wp_unslash($_POST['webhook'])) : '';
        $enabledLogging = isset($_POST['enabled_logging']) ? trim(wp_unslash($_POST['enabled_logging'])) : '';
        $webhook = trailingslashit($webhook);

        if (empty($webhook)) {
            $response = sprintf(
                '<div data-ui-component="cf7bitrix24noticesettings" class="error notice notice-error is-dismissible"><p><strong>%1$s</strong>: %2$s</p></div>',
                esc_html__('ERROR', 'cf7-bitrix24-lg'),
                esc_html__('To integrate with Bitrix24, your must fill in all fields.', 'cf7-bitrix24-lg')
            );
        } elseif (filter_var($webhook, FILTER_VALIDATE_URL) === false) {
            $response = sprintf(
                '<div data-ui-component="cf7bitrix24noticesettings" class="error notice notice-error"><p><strong>%1$s</strong>: %2$s</p></div>',
                esc_html__('ERROR', 'cf7-bitrix24-lg'),
                esc_html__('Web hook url is not valid.', 'cf7-bitrix24-lg')
            );
        } else {
            update_option(
                Bootstrap::OPTIONS_KEY,
                [
                    'webhook' => $webhook,
                    'enabled_logging' => $enabledLogging,
                ]
            );

            $check = Bitrix24::checkConnection();

            if ($check < 3) {
                if ($check === 1) {
                    $response = sprintf(
                        '<div data-ui-component="cf7bitrix24noticesettings" class="error notice notice-error"><p><strong>%1$s</strong>: %2$s</p></div>',
                        esc_html__('ERROR', 'cf7-bitrix24-lg'),
                        esc_html__('Insufficient permissions. Check CRM settings.', 'cf7-bitrix24-lg')
                    );
                } elseif ($check === 2) {
                    $response = sprintf(
                        '<div data-ui-component="cf7bitrix24noticesettings" class="error notice notice-error"><p><strong>%1$s</strong>: %2$s</p></div>',
                        esc_html__('ERROR', 'cf7-bitrix24-lg'),
                        esc_html__('Response CRM is not valid. Please check web hook link.', 'cf7-bitrix24-lg')
                    );
                }
            } else {
                Bitrix24::updateInformation();

                $response = sprintf(
                    '<div data-ui-component="cf7bitrix24noticesettings" class="updated notice notice-success is-dismissible"><p>%s</p></div>',
                    esc_html__('Settings successfully updated.', 'cf7-bitrix24-lg')
                );
            }
        }

        echo $response;

        exit();
    }

    public function registerService()
    {
        $integration = \WPCF7_Integration::get_instance();
        $categories = ['crm' => $this->get_title()];

        foreach ($categories as $name => $category) {
            $integration->add_category($name, $category);
        }

        $services = [self::$serviceName => self::getInstance()];

        foreach ($services as $name => $service) {
            $integration->add_service($name, $service);
        }
    }

    // @codingStandardsIgnoreStart
    public function is_active()
    {
        // @codingStandardsIgnoreEnd
        $settings = get_option(Bootstrap::OPTIONS_KEY);

        return !empty($settings['webhook']);
    }

    // @codingStandardsIgnoreStart
    public function get_title()
    {
        // @codingStandardsIgnoreEnd
        return esc_html__('Integration with Bitrix24', 'cf7-bitrix24-lg');
    }

    // @codingStandardsIgnoreStart
    public function get_categories()
    {
        // @codingStandardsIgnoreEnd
        return ['crm'];
    }

    public function icon()
    {
    }

    public function link()
    {
        echo '<a href="https://codecanyon.net/user/itgalaxycompany">itgalaxycompany</a>';
    }

    public function load($action = '')
    {
        if ('setup' == $action) {
            if (isset($_SERVER['REQUEST_METHOD']) && 'POST' == $_SERVER['REQUEST_METHOD']) {
                if (isset($_POST['purchase-code'])) {
                    check_admin_referer('wpcf7-bitrix24-lg-setup-license');
                    $code = trim(wp_unslash($_POST['purchase-code']));

                    $response = \wp_remote_post(
                        'https://wordpress-plugins.xyz/envato/license.php',
                        [
                            'body' => [
                                'purchaseCode' => $code,
                                'itemID' => '20288214',
                                'action' => isset($_POST['verify']) ? 'activate' : 'deactivate',
                                'domain' => site_url()
                            ],
                            'timeout' => 20
                        ]
                    );

                    if (is_wp_error($response)) {
                        $messageContent = '(Code - '
                            . $response->get_error_code()
                            . ') '
                            . $response->get_error_message();

                        $message = 'failedCheck';
                    } else {
                        $response = json_decode(wp_remote_retrieve_body($response));

                        if ($response->status == 'successCheck') {
                            if (isset($_POST['verify'])) {
                                update_site_option(Bootstrap::PURCHASE_CODE_OPTIONS_KEY, $code);
                            } else {
                                update_site_option(Bootstrap::PURCHASE_CODE_OPTIONS_KEY, '');
                            }
                        } elseif (!isset($_POST['verify']) && $response->status == 'alreadyInactive') {
                            update_site_option(Bootstrap::PURCHASE_CODE_OPTIONS_KEY, '');
                        }

                        $messageContent = $response->message;
                        $message = $response->status;
                    }

                    wp_safe_redirect(
                        $this->menuPageUrl(
                            [
                                'action' => 'setup',
                                'message' => $message,
                                'messageContent' => rawurlencode($messageContent)
                            ]
                        )
                    );

                    exit();
                }
            }
        }
    }

    // @codingStandardsIgnoreStart
    public function admin_notice($message = '')
    {
        // @codingStandardsIgnoreEnd
        if ($message == 'successCheck') {
            echo sprintf(
                '<div class="updated notice notice-success is-dismissible"><p>%s</p></div>',
                esc_html(isset($_GET['messageContent']) ? $_GET['messageContent'] : '')
            );
        } elseif (isset($_GET['messageContent'])) {
            echo sprintf(
                '<div class="error notice notice-error is-dismissible"><p>%s</p></div>',
                esc_html(isset($_GET['messageContent']) ? $_GET['messageContent'] : '')
            );
        }
    }

    public function display($action = '')
    {
        $settings = get_option(Bootstrap::OPTIONS_KEY);
        ?>
        <p>
            <?php
            esc_html_e(
                'Formation of leads in Bitrix24 from the hits that users leave on your site, using '
                . 'the Contact Form 7 plugin.',
                'cf7-bitrix24-lg'
            );
            ?>
        </p>
        <?php
        if ('setup' == $action) {
            ?>
            <form method="post" action="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>">
                <?php wp_nonce_field('wpcf7-bitrix-lead-generation-setup'); ?>
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row">
                                <label for="webhook">
                                    <?php esc_html_e('Inbound web hook', 'cf7-bitrix24-lg'); ?>
                                </label>
                            </th>
                            <td>
                                <input type="text"
                                    aria-required="true"
                                    value="<?php
                                    echo isset($settings['webhook'])
                                        ? esc_attr($settings['webhook'])
                                        : '';
                                    ?>"
                                    id="webhook"
                                    placeholder="https://your.bitrix24.ru/rest/*/**********/"
                                    name="webhook"
                                    class="large-text">
                                <small>
                                    <?php
                                    esc_html_e(
                                        'The following permissions are required: CRM, Tasks, Tasks extended, Chat and Notifications.',
                                        'cf7-bitrix24-lg'
                                    );
                                    ?>
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="enabled_logging">
                                    <?php esc_html_e('Enable logging', 'cf7-bitrix24-lg'); ?>
                                </label>
                            </th>
                            <td>
                                <input type="checkbox"
                                    value="1"
                                    <?php echo isset($settings['enabled_logging']) && $settings['enabled_logging'] == '1' ? 'checked' : ''; ?>
                                    id="enabled_logging"
                                    name="enabled_logging">
                                <br>
                                <small><?php echo esc_html(CF7_BITRIX24_PLUGIN_LOG_FILE); ?></small>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php
                echo sprintf(
                    '%1$s <a href="%2$s" target="_blank">%3$s</a>. %4$s.',
                    esc_html__('Plugin documentation: ', 'cf7-bitrix24-lg'),
                    esc_url(CF7_BITIX24_PLUGIN_URL . 'documentation/index.html#step-1'),
                    esc_html__('open', 'cf7-bitrix24-lg'),
                    esc_html__('Or open the folder `documentation` in the plugin and open index.html', 'cf7-bitrix24-lg')
                );
                ?>
                <p class="submit">
                    <input type="submit"
                        data-ui-component="cf7-bitrix24-save-settings"
                        class="button button-primary"
                        value="<?php esc_attr_e('Save settings', 'cf7-bitrix24-lg'); ?>"
                        name="submit">
                </p>
            </form>
            <?php $code = get_site_option(Bootstrap::PURCHASE_CODE_OPTIONS_KEY); ?>
            <h1>
                <?php esc_html_e('License verification', 'cf7-bitrix24-lg'); ?>
                <?php if ($code) { ?>
                    - <small style="color: green;">
                        <?php esc_html_e('verified', 'cf7-bitrix24-lg'); ?>
                    </small>
                <?php } else { ?>
                    - <small style="color: red;">
                        <?php esc_html_e('please verify your purchase code', 'cf7-bitrix24-lg'); ?>
                    </small>
                <?php } ?>
            </h1>
            <form method="post" action="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>">
                <?php wp_nonce_field('wpcf7-bitrix24-lg-setup-license'); ?>
                <table class="form-table">
                    <tr>
                        <th scope="row">
                            <label for="purchase-code">
                                <?php esc_html_e('Purchase code', 'cf7-bitrix24-lg'); ?>
                            </label>
                        </th>
                        <td>
                            <input type="text"
                                aria-required="true"
                                required
                                value="<?php
                                echo !empty($code)
                                    ? esc_attr($code)
                                    : '';
                                ?>"
                                id="purchase-code"
                                name="purchase-code"
                                class="large-text">
                            <small>
                                <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-Is-My-Purchase-Code-"
                                    target="_blank">
                                    <?php esc_html_e('Where Is My Purchase Code?', 'cf7-bitrix24-lg'); ?>
                                </a>
                            </small>
                        </td>
                    </tr>
                </table>
                <p>
                    <input type="submit"
                        class="button button-primary"
                        value="<?php esc_attr_e('Verify', 'cf7-bitrix24-lg'); ?>"
                        name="verify">
                    <?php if ($code) { ?>
                        <input type="submit"
                            class="button button-primary"
                            value="<?php esc_attr_e('Unverify', 'cf7-bitrix24-lg'); ?>"
                            name="unverify">
                    <?php } ?>
                </p>
            </form>
            <?php
        } else {
            if ($this->is_active()) {
                ?>
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row"><?php esc_html_e('Inbound web hook', 'cf7-bitrix24-lg'); ?></th>
                            <td class="code"><?php echo esc_html($settings['webhook']); ?></td>
                        </tr>
                    </tbody>
                </table>
                <p>
                    <a href="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>"
                        class="button">
                        <?php esc_html_e('Change settings', 'cf7-bitrix24-lg'); ?>
                    </a>
                </p>
                <?php
            } else {
                ?>
                <p>
                    <?php
                    esc_html_e(
                        'To work with the plugin, you must configure integration with Bitrix24.',
                        'cf7-bitrix24-lg'
                    );
                    ?>
                </p>
                <p>
                    <a href="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>"
                        class="button">
                        <?php esc_html_e('Go to setup', 'cf7-bitrix24-lg'); ?>
                    </a>
                </p>
                <p class="description">
                    <?php
                    esc_html_e(
                        'The fields sent to the CRM are configured on the form editing page, on the "Bitrix24" tab.',
                        'cf7-bitrix24-lg'
                    );
                    ?>
                </p>
                <?php
            }
        }
    }

    public function settingsPanels($panels)
    {
        $panels['bitrix-panel'] = [
            'title' => esc_html__('Bitrix24', 'cf7-bitrix24-lg'),
            'callback' => [$this, 'panel']
        ];

        return $panels;
    }

    public function panel(\WPCF7_ContactForm $post)
    {
        $meta = get_post_meta($post->id(), Bootstrap::META_KEY, true);

        $enabled = isset($meta['ENABLED']) ? $meta['ENABLED'] : false;
        ?>
        <input type="hidden" name="cf7Bitrix[ENABLED]" value="0">
        <input
            type="checkbox"
            name="cf7Bitrix[ENABLED]" value="1"
            <?php checked($enabled, true); ?>
            title="<?php
            esc_attr_e('Enable send the lead', 'cf7-bitrix24-lg');
            ?>">
        <strong>
            <?php
            esc_html_e(
                'Enable send the lead',
                'cf7-bitrix24-lg'
            );
            ?>
        </strong>
        <br><br>
        <?php
        echo esc_html(__(
            'In the following fields, you can use these mail-tags:',
            'contact-form-7'
        ));
        ?>
        <br>
        <?php
        $post->suggest_mail_tags();
        ?>
        <br><br>
        Utm-fields:<br>
        <span class="mailtag code">[utm_source]</span>
        <span class="mailtag code">[utm_medium]</span>
        <span class="mailtag code">[utm_campaign]</span>
        <span class="mailtag code">[utm_term]</span>
        <span class="mailtag code">[utm_content]</span>
        <span class="mailtag code">and etc.</span>
        <br><br>
        Roistat-fields:<br>
        <span class="mailtag code">[roistat_visit]</span>
        <br><br>
        GA fields:<br>
        <span class="mailtag code">[gaClientID]</span>
        <hr>
        <p>
            <?php $currentType = isset($meta['TYPE']) ? $meta['TYPE'] : 'lead'; ?>
            <strong>
                <?php
                esc_html_e(
                    'Choose the type of lead that will be generated in CRM:',
                    'cf7-bitrix24-lg'
                );
                ?>
            </strong>
            <br>
            <label>
                <input type="radio"
                    value="lead"
                    name="cf7Bitrix[TYPE]"
                    title="<?php esc_attr_e('Lead', 'cf7-bitrix24-lg'); ?>"
                    <?php checked($currentType, 'lead'); ?>> <?php esc_html_e('Lead', 'cf7-bitrix24-lg'); ?>
                <br><small>
                    <?php
                    esc_html_e(
                        'Used tabs: Lead fields - main, Task fields - additional.',
                        'cf7-bitrix24-lg'
                    );
                    ?>
                </small>
            </label>
            <br>
            <label>
                <input type="radio"
                    value="deal"
                    name="cf7Bitrix[TYPE]"
                    title="<?php esc_html_e('Deal', 'cf7-bitrix24-lg'); ?>"
                    <?php checked($currentType, 'deal'); ?>> <?php
                esc_html_e(
                    'Deal (Auto create/connect existing contact and company. '
                    . 'Search contact and company by phone and email.)',
                    'cf7-bitrix24-lg');
                ?>
                <br><small>
                    <?php
                    esc_html_e(
                        'Used tabs: Deal fields - main, Contact fields, Company fields, Task fields - additional.',
                        'cf7-bitrix24-lg'
                    );
                    ?>
                </small>
            </label>
            <br>
            <label>
                <input type="radio"
                    value="task"
                    name="cf7Bitrix[TYPE]"
                    title="<?php esc_html_e('Task', 'cf7-bitrix24-lg'); ?>"
                    <?php checked($currentType, 'task'); ?>> <?php
                esc_html_e(
                    'Task (Auto create/connect existing contact and company. '
                    . 'Search contact and company by phone and email.)',
                    'cf7-bitrix24-lg');
                ?>
                <br><small>
                    <?php
                    esc_html_e(
                        'Used tabs: Task fields - main, Contact fields, Company fields - additional.',
                        'cf7-bitrix24-lg'
                    );
                    ?>
                </small>
            </label>
            <br>
            <label>
                <input type="radio"
                    value="contact"
                    name="cf7Bitrix[TYPE]"
                    title="<?php esc_html_e('Contact', 'cf7-bitrix24-lg'); ?>"
                    <?php checked($currentType, 'contact'); ?>> <?php esc_html_e('Contact', 'cf7-bitrix24-lg'); ?>
                <br><small><?php esc_html_e('Used tab: Contact fields.', 'cf7-bitrix24-lg'); ?></small>
            </label>
            <br>
            <label>
                <input type="radio"
                    value="company"
                    name="cf7Bitrix[TYPE]"
                    title="<?php esc_html_e('Company', 'cf7-bitrix24-lg'); ?>"
                    <?php checked($currentType, 'company'); ?>>
                <?php esc_html_e('Company', 'cf7-bitrix24-lg'); ?>
                <br><small><?php esc_html_e('Used tab: Company fields.', 'cf7-bitrix24-lg'); ?></small>
            </label>
        </p>
        <hr>
        <div id="cf7-bx-tabs">
            <ul>
                <li>
                    <a href="#lead-fields">
                        <?php esc_html_e('Lead fields', 'cf7-bitrix24-lg'); ?>
                    </a>
                </li>
                <li>
                    <a href="#deal-fields">
                        <?php esc_html_e('Deal fields', 'cf7-bitrix24-lg'); ?>
                    </a>
                </li>
                <li>
                    <a href="#task-fields">
                        <?php esc_html_e('Task fields', 'cf7-bitrix24-lg'); ?>
                    </a>
                </li>
                <li>
                    <a href="#contact-fields">
                        <?php esc_html_e('Contact fields', 'cf7-bitrix24-lg'); ?>
                    </a>
                </li>
                <li>
                    <a href="#company-fields">
                        <?php esc_html_e('Company fields', 'cf7-bitrix24-lg'); ?>
                    </a>
                </li>
                <li>
                    <a href="#livefeed-fields">
                        <?php esc_html_e('Live feed entry message', 'cf7-bitrix24-lg'); ?>
                    </a>
                </li>
            </ul>
            <div id="lead-fields">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th style="width: 150px;">
                                <?php esc_html_e('Create task', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['lead_create_task'])
                                    ? $meta['lead_create_task']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[lead_create_task]" value="false">
                                <input id="__lead_create_task"
                                    type="checkbox"
                                    title="<?php esc_html_e('Create task', 'cf7-bitrix24-lg'); ?>"
                                    <?php checked($value, 'true'); ?>
                                    name="cf7Bitrix[lead_create_task]"
                                    value="true">
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 150px;">
                                <?php esc_html_e('Update an existing lead (search by phone and mail)', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['lead_update_exists'])
                                    ? $meta['lead_update_exists']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[lead_update_exists]" value="false">
                                <input id="__lead_update_exists"
                                    type="checkbox"
                                    title="<?php esc_html_e('Update an existing lead (search by phone and mail)', 'cf7-bitrix24-lg'); ?>"
                                    <?php checked($value, 'true'); ?>
                                    name="cf7Bitrix[lead_update_exists]"
                                    value="true">
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 150px;">
                                <?php esc_html_e('Send uploaded files', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['send_files_lead'])
                                    ? $meta['send_files_lead']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[send_files_lead]" value="false">
                                <input id="__send_files_lead"
                                    type="checkbox"
                                    title="<?php esc_html_e('Send uploaded files', 'cf7-bitrix24-lg'); ?>"
                                    <?php checked($value, 'true'); ?>
                                    name="cf7Bitrix[send_files_lead]"
                                    value="true">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <?php LeadSettings::getInstance()->render($meta); ?>
            </div>
            <div id="deal-fields">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th style="width: 150px;">
                                <?php esc_html_e('Create task', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['deal_create_task'])
                                    ? $meta['deal_create_task']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[deal_create_task]" value="false">
                                <input id="__deal_create_task"
                                    type="checkbox"
                                    title="<?php esc_html_e('Create task', 'cf7-bitrix24-lg'); ?>"
                                    <?php checked($value, 'true'); ?>
                                    name="cf7Bitrix[deal_create_task]"
                                    value="true">
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 150px;">
                                <?php esc_html_e('Send uploaded files', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['send_files_deal'])
                                    ? $meta['send_files_deal']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[send_files_deal]" value="false">
                                <input id="__send_files_deal"
                                    type="checkbox"
                                    title="<?php esc_html_e('Send uploaded files', 'cf7-bitrix24-lg'); ?>"
                                    <?php checked($value, 'true'); ?>
                                    name="cf7Bitrix[send_files_deal]"
                                    value="true">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <?php DealSettings::getInstance()->render($meta); ?>
            </div>
            <div id="task-fields">
                <?php TaskSettings::getInstance()->render($meta); ?>
            </div>
            <div id="contact-fields">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th style="width: 300px;">
                                <?php esc_html_e('Always create a new contact and create live feed entry message in all exists (search by phone and mail)', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['always_create_new_contact'])
                                    ? $meta['always_create_new_contact']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[always_create_new_contact]" value="false">
                                <label>
                                    <input type="checkbox"
                                        name="cf7Bitrix[always_create_new_contact]"
                                        value="true"
                                        <?php checked($value, 'true'); ?>>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 300px;">
                                <?php esc_html_e('Update an existing contact (search by phone and mail)', 'cf7-bitrix24-lg'); ?>
                            </th>
                            <td>
                                <?php
                                $value = isset($meta['contact_update_exists'])
                                    ? $meta['contact_update_exists']
                                    : '';
                                ?>
                                <input type="hidden" name="cf7Bitrix[contact_update_exists]" value="false">
                                <label>
                                    <input type="checkbox"
                                        name="cf7Bitrix[contact_update_exists]"
                                        value="true"
                                        <?php checked($value, 'true'); ?>>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <?php ContactSettings::getInstance()->render($meta); ?>
            </div>
            <div id="company-fields">
                <?php CompanySettings::getInstance()->render($meta); ?>
            </div>
            <div id="livefeed-fields">
                <table class="form-table">
                    <tr>
                        <th>
                            <?php esc_html_e('Message content', 'cf7-bitrix24-lg'); ?>
                        </th>
                        <td>
                            <textarea
                                id="__livefeedmessage"
                                class="large-text code"
                                title="<?php echo esc_attr_e('Message content', 'cf7-bitrix24-lg'); ?>"
                                name="cf7Bitrix[livefeedmessage]"
                                rows="4"><?php echo esc_html(isset($meta['livefeedmessage']) ? $meta['livefeedmessage'] : ''); ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <?php
    }

    public function saveSettings($postID)
    {
        if (isset($_POST['cf7Bitrix'])) {
            update_post_meta($postID, Bootstrap::META_KEY, wp_unslash($_POST['cf7Bitrix']));
        }
    }

    protected function __clone()
    {
    }

    private function menuPageUrl($args = '')
    {
        $args = wp_parse_args($args, []);
        $url = menu_page_url('wpcf7-integration', false);
        $url = add_query_arg(['service' => self::$serviceName], $url);

        if (!empty($args)) {
            $url = add_query_arg($args, $url);
        }

        return $url;
    }
}
