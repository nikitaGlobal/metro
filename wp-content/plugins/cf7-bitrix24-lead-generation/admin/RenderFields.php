<?php
namespace Cf7\Bitrix\Lead\Generation\Admin;

use Cf7\Bitrix\Lead\Generation\Includes\Bitrix24;
use Cf7\Bitrix\Lead\Generation\Includes\Bootstrap;

class RenderFields
{
    public $fieldNameStart = 'cf7Bitrix[';

    public function __construct($type = '')
    {
        $this->fieldNameStart .= $type . '][';
    }

    public function selectField($list, $name, $title, $currentValue, $currentValuePopulate = '')
    {
        ?>
        <table width="100%">
            <tr>
                <td style="width: 50%;">
                    <label><?php esc_html_e('Default value', 'cf7-bitrix24-lg'); ?></label>
                    <br>
                    <select id="__<?php echo esc_attr($name); ?>"
                        title="<?php echo esc_attr($title); ?>"
                        name="<?php echo esc_attr($this->fieldNameStart . $name); ?>]">
                        <option value=""><?php esc_html_e('Not chosen', 'cf7-bitrix24-lg'); ?></option>
                        <?php
                        foreach ((array) $list as $value => $label) {
                            echo '<option value="'
                                . esc_attr($value)
                                . '"'
                                . ($currentValue == $value ? ' selected' : '')
                                . '>'
                                . esc_html($value . ' - ' . $label)
                                . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <label><?php esc_html_e('Form value', 'cf7-bitrix24-lg'); ?></label>
                    <a href="<?php echo esc_url(CF7_BITIX24_PLUGIN_URL . 'documentation/index.html#additional'); ?>"
                        target="_blank"
                        style="text-decoration: none;">
                        <span class="dashicons dashicons-editor-help"></span>
                    </a>
                    <br>
                    <input id="__<?php echo esc_attr($name); ?>-populate"
                        type="text"
                        class="large-text code"
                        placeholder="Use `bitrix24_select` field"
                        title="<?php echo esc_attr($title); ?>"
                        name="<?php echo esc_attr($this->fieldNameStart . $name); ?>-populate]"
                        value="<?php echo esc_attr($currentValuePopulate); ?>">
                </td>
            </tr>
        </table>
        <?php
    }

    public function statusField($type, $name, $title, $currentValue, $currentValuePopulate = '')
    {
        $list = $this->getStatusListByType($type);
        ?>
        <table width="100%">
            <tr>
                <td style="width: 50%;">
                    <label><?php esc_html_e('Default value', 'cf7-bitrix24-lg'); ?></label>
                    <br>
                    <select id="__<?php echo esc_attr($name); ?>"
                        title="<?php echo esc_attr($title); ?>"
                        name="<?php echo esc_attr($this->fieldNameStart . $name); ?>]">
                        <option value=""><?php esc_html_e('Not chosen', 'cf7-bitrix24-lg'); ?></option>
                        <?php
                        foreach ((array) $list as $value => $label) {
                            echo '<option value="'
                                . esc_attr($value)
                                . '"'
                                . ($currentValue == $value ? ' selected' : '')
                                . '>'
                                . esc_html($value . ' - ' . $label)
                                . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <label><?php esc_html_e('Form value', 'cf7-bitrix24-lg'); ?></label>
                    <a href="<?php echo esc_url(CF7_BITIX24_PLUGIN_URL . 'documentation/index.html#additional'); ?>"
                        target="_blank"
                        style="text-decoration: none;">
                        <span class="dashicons dashicons-editor-help"></span>
                    </a>
                    <br>
                        <input id="__<?php echo esc_attr($name); ?>-populate"
                            type="text"
                            class="large-text code"
                            placeholder="Use `bitrix24_select` field"
                            title="<?php echo esc_attr($title); ?>"
                            name="<?php echo esc_attr($this->fieldNameStart . $name); ?>-populate]"
                            value="<?php echo esc_attr($currentValuePopulate); ?>">
                </td>
            </tr>
        </table>
        <?php
    }

    public function inputTextField($name, $title, $currentValue)
    {
        ?>
        <input id="__<?php echo esc_attr($name); ?>"
            type="text"
            class="large-text code"
            title="<?php echo esc_attr($title); ?>"
            name="<?php echo esc_attr($this->fieldNameStart . $name); ?>]"
            value="<?php echo esc_attr($currentValue); ?>">
        <?php
    }

    public function inputCheckboxField($name, $title, $currentValue, $currentValuePopulate = '')
    {
        ?>
        <table width="100%">
            <tr>
                <td style="width: 50%;">
                    <label><?php esc_html_e('Default value', 'cf7-bitrix24-lg'); ?></label>
                    <br>
                    <input type="hidden"
                        name="<?php echo esc_attr($this->fieldNameStart . $name); ?>]"
                        value="N">
                    <input id="__<?php echo esc_attr($name); ?>"
                        type="checkbox"
                        title="<?php echo esc_attr($title); ?>"
                        name="<?php echo esc_attr($this->fieldNameStart . $name); ?>]"
                        value="Y"
                        <?php echo $currentValue === 'Y' ? 'checked' : ''; ?>>
                </td>
                <td>
                    <label><?php esc_html_e('Form value', 'cf7-bitrix24-lg'); ?></label>
                    <br>
                    <small>
                        <?php esc_html_e('Support values', 'cf7-bitrix24-lg'); ?>: yes/no, on/off, 1/0, true/false
                    </small>
                    <input id="__<?php echo esc_attr($name); ?>-populate"
                        type="text"
                        class="large-text code"
                        title="<?php echo esc_attr($title); ?>"
                        name="<?php echo esc_attr($this->fieldNameStart . $name); ?>-populate]"
                        value="<?php echo esc_attr($currentValuePopulate); ?>">
                </td>
            </tr>
        </table>
        <?php
    }

    public function textareaField($name, $title, $currentValue)
    {
        ?>
        <textarea
            id="__<?php echo esc_attr($name); ?>"
            class="large-text code"
            title="<?php echo esc_attr($title); ?>"
            name="<?php echo esc_attr($this->fieldNameStart . $name); ?>]"
            rows="4"><?php echo esc_attr($currentValue); ?></textarea>
        <?php
    }

    private function getStatusListByType($type)
    {
        $statusList = get_option(Bootstrap::STATUS_LIST_KEY);

        $returnList = [];

         if ($type == 'DEAL_STAGE') {
             // Default pipeline
            foreach ($statusList as $status) {
                if ($status['ENTITY_ID'] === $type) {
                    $returnList[$status['STATUS_ID']] = esc_html__('Default pipeline', 'cf7-bitrix24-lg')
                        . ' - '
                        . $status['NAME'];
                }
            }

            $pipelines = get_option(Bootstrap::DEAL_CATEGORY_LIST_KEY);

            if (empty($pipelines)) {
                Bitrix24::updateFieldsList('crm.dealcategory.list', Bootstrap::DEAL_CATEGORY_LIST_KEY);
            }

            $pipelines = (array) get_option(Bootstrap::DEAL_CATEGORY_LIST_KEY);

            if ($pipelines) {
                foreach ($pipelines as $pipeline) {
                    foreach ($statusList as $status) {
                        if ($status['ENTITY_ID'] === $type . '_' . $pipeline['ID']) {
                            $returnList[$status['STATUS_ID']] = $pipeline['NAME'] . ' - ' . $status['NAME'];
                        }
                    }
                }
            }
        } else {
            foreach ($statusList as $status) {
                if ($status['ENTITY_ID'] === $type) {
                    $returnList[$status['STATUS_ID']] = $status['NAME'];
                }
            }
        }

        return $returnList;
    }
}
