<?php
namespace Cf7\Bitrix\Lead\Generation\Admin;

use Cf7\Bitrix\Lead\Generation\Includes\Bootstrap;
use Cf7\Bitrix\Lead\Generation\Includes\CrmFields;

class DealSettings
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {

    }

    public function render($meta)
    {
        $renderFields = new RenderFields('deal');
        $currentValues = isset($meta['deal']) ? $meta['deal'] : [];
        ?>
        <table class="form-table">
            <?php
            $dealFields = (array) get_option(Bootstrap::DEAL_FIELDS_KEY);

            $crmFields = new CrmFields();
            $crmFields = $crmFields->fields;

            foreach ($dealFields as $key => $field) {
                // Not show fields
                if (in_array($key, CrmFields::$breakFields['deal'])) {
                    continue;
                }

                // Not show read only fields
                if ($field['isReadOnly'] === true) {
                    continue;
                }

                $title = isset($crmFields[$key])
                    ? $crmFields[$key]
                    : (
                    isset($field['formLabel'])
                        ? $field['formLabel']
                        : $key
                    );
                ?>
                <tr>
                    <th>
                        <?php echo esc_html($title . ' (' . $key . ')'); ?>
                        <?php echo $field['isRequired'] ? '<span style="color:red;"> * </span>' : ''; ?>
                    </th>
                    <td>
                        <?php
                        $currentValue = isset($currentValues[$key]) ? $currentValues[$key] : '';
                        $currentValuePopulate = isset($currentValues[$key . '-populate'])
                            ? $currentValues[$key . '-populate']
                            : '';

                        if ($field['type'] === 'enumeration' && !empty($field['items'])) {
                            $selectItems = [];

                            foreach ($field['items'] as $item) {
                                $selectItems[$item['ID']] = $item['VALUE'];
                            }

                            $renderFields->selectField(
                                $selectItems,
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($field['type'] === 'char' || $field['type'] === 'boolean') {
                            $renderFields->inputCheckboxField(
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'TYPE_ID') {
                            $renderFields->statusField(
                                'DEAL_TYPE',
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'STAGE_ID') {
                            $renderFields->statusField(
                                'DEAL_STAGE',
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($key === 'CURRENCY_ID') {
                            $renderFields->selectField(
                                get_option(Bootstrap::CURRENCY_LIST_KEY),
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif (in_array($key, ['COMMENTS', 'SOURCE_DESCRIPTION', 'STATUS_DESCRIPTION'])) {
                            $renderFields->textareaField(
                                $key,
                                $title,
                                $currentValue
                            );
                        } else {
                            $renderFields->inputTextField(
                                $key,
                                $title,
                                $currentValue
                            );

                            if (in_array($key, ['ASSIGNED_BY_ID', 'RESPONSIBLE_ID'])) {
                                ?>
                                <p class="description">
                                    <?php
                                    esc_html_e(
                                        'you can specify several, separated by commas, then the requests will be distributed sequentially',
                                        'cf7-bitrix24-lg'
                                    );
                                    ?>
                                </p>
                                <?php
                            }
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
}
