<?php
namespace Cf7\Bitrix\Lead\Generation\Admin;

use Cf7\Bitrix\Lead\Generation\Includes\Bootstrap;
use Cf7\Bitrix\Lead\Generation\Includes\CrmFields;

class TaskSettings
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {

    }

    public function render($meta)
    {
        $renderFields = new RenderFields('task');
        $currentValues = isset($meta['task']) ? $meta['task'] : [];
        ?>
        <table class="form-table">
            <?php
            $taskFields = (array) get_option(Bootstrap::TASK_FIELDS_KEY);
            $crmFields = new CrmFields();
            $crmFields = $crmFields->fields;

            foreach ($taskFields as $key => $field) {
                // Not show fields
                if (in_array($key, CrmFields::$breakFields['task'])) {
                    continue;
                }

                // Not show read only fields
                if ($field['isReadOnly'] === true) {
                    continue;
                }

                $title = isset($crmFields[$key])
                    ? $crmFields[$key]
                    : (
                    isset($field['formLabel'])
                        ? $field['formLabel']
                        : $key
                    );
                ?>
                <tr>
                    <th>
                        <?php echo esc_html($title . ' (' . $key . ')'); ?>
                        <?php echo $field['isRequired'] ? '<span style="color:red;"> * </span>' : ''; ?>
                    </th>
                    <td>
                        <?php
                        $currentValue = isset($currentValues[$key]) ? $currentValues[$key] : '';
                        $currentValuePopulate = isset($currentValues[$key . '-populate'])
                            ? $currentValues[$key . '-populate']
                            : '';

                        if ($field['type'] === 'enumeration' && !empty($field['items'])) {
                            $selectItems = [];

                            foreach ($field['items'] as $item) {
                                $selectItems[$item['ID']] = $item['VALUE'];
                            }

                            $renderFields->selectField(
                                $selectItems,
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif ($field['type'] === 'char' || $field['type'] === 'boolean') {
                            $renderFields->inputCheckboxField(
                                $key,
                                $title,
                                $currentValue,
                                $currentValuePopulate
                            );
                        } elseif (in_array($key, ['DESCRIPTION'])) {
                            $renderFields->textareaField(
                                $key,
                                $title,
                                $currentValue
                            );
                        } else {
                            $renderFields->inputTextField(
                                $key,
                                $title,
                                $currentValue
                            );
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
}
