<?php
namespace Cf7\Bitrix\Lead\Generation\Includes;

class Bootstrap
{
    const OPTIONS_KEY = 'cf7-bitrix-lead-generation-settings';
    const PURCHASE_CODE_OPTIONS_KEY = 'cf7-bitrix-purchase-code';

    const META_KEY = '_cf7-bitrix-lead-generation';

    const LEAD_FIELDS_KEY = '_cf7-bitrix-lead-fields';

    const DEAL_FIELDS_KEY = '_cf7-bitrix-deal-fields';
    const DEAL_CATEGORY_LIST_KEY = '_cf7-bitrix-deal-category-list';

    const TASK_FIELDS_KEY = '_cf7-bitrix-task-fields';
    const CONTACT_FIELDS_KEY = '_cf7-bitrix-contact-fields';
    const COMPANY_FIELDS_KEY = '_cf7-bitrix-company-fields';
    const STATUS_LIST_KEY = '_cf7-bitrix-status-list';
    const CURRENCY_LIST_KEY = '_cf7-bitrix-currency-list';
    const CRON_TASK = 'cf7-bitrix-cron-task';

    const UTM_COOKIES = 'cf7-bitrix-utm-cookie';

    public static $plugin = '';
    private static $instance = false;

    protected function __construct($file)
    {
        self::$plugin = $file;

        register_activation_hook(
            self::$plugin,
            ['Cf7\Bitrix\Lead\Generation\Includes\Bootstrap', 'pluginActivation']
        );
        register_deactivation_hook(
            self::$plugin,
            ['Cf7\Bitrix\Lead\Generation\Includes\Bootstrap', 'pluginDeactivation']
        );
        register_uninstall_hook(
            self::$plugin,
            ['Cf7\Bitrix\Lead\Generation\Includes\Bootstrap', 'pluginUninstall']
        );

        add_action('init', [$this, 'utmCookies']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);

        add_action('wp_ajax_cf7Bitrix24AjaxSetUtm', [$this, 'utmCookies']);
        add_action('wp_ajax_nopriv_cf7Bitrix24AjaxSetUtm', [$this, 'utmCookies']);
    }

    public static function getInstance($file)
    {
        if (!self::$instance) {
            self::$instance = new self($file);
        }

        return self::$instance;
    }

    public function utmCookies()
    {
        $strposFunction = 'mb_strpos';

        if (!function_exists('mb_strpos')) {
            $strposFunction = 'strpos';
        }

        if (!empty($_GET) && is_array($_GET)) {
            $utmParams = [];

            foreach ($_GET as $key => $value) {
                if ($strposFunction($key, 'utm_') === 0) {
                    $utmParams[$key] = wp_unslash($value);
                }

                if ($strposFunction($key, 'pm_') === 0) {
                    $utmParams[$key] = wp_unslash($value);
                }
            }

            if (!empty($utmParams)) {
                setcookie(
                    self::UTM_COOKIES,
                    wp_json_encode($utmParams),
                    time() + 86400,
                    '/'
                );
            }
        }
    }

    public function enqueueScripts()
    {
        if (
            !is_plugin_active('wp-fastest-cache/wpFastestCache.php') &&
            (!defined('WP_CACHE') || !WP_CACHE)
        ) {
            return;
        }

        wp_enqueue_script(
            'cf7-bitrix24-theme-js',
            CF7_BITIX24_PLUGIN_URL . 'theme/js/theme.js',
            ['jquery'],
            CF7_BITIX24_PLUGIN_VERSION,
            true
        );
    }


    public static function pluginActivation()
    {
        if (!is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
            wp_die(
                esc_html__(
                    'To run the plug-in, you must first install and activate the Contact Form 7 plugin.',
                    'cf7-bitrix24-lg'
                ),
                esc_html__(
                    'Error while activating the Contact Form 7 - Bitrix24 CRM - Integration',
                    'cf7-bitrix24-lg'
                ),
                [
                    'back_link' => true
                ]
            );
            // Escape ok
        }

        $roles = new \WP_Roles();

        foreach (self::capabilities() as $capGroup) {
            foreach ($capGroup as $cap) {
                $roles->add_cap('administrator', $cap);

                if (is_multisite()) {
                    $roles->add_cap('super_admin', $cap);
                }
            }
        }
    }

    public static function pluginDeactivation()
    {
        wp_clear_scheduled_hook(self::CRON_TASK);
    }

    public static function pluginUninstall()
    {
        // Nothing
    }

    public static function capabilities()
    {
        $capabilities = [];
        $capabilities['core'] = ['manage_' . self::OPTIONS_KEY];
        flush_rewrite_rules(true);

        return $capabilities;
    }

    private function __clone()
    {
    }
}
