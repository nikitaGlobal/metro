<?php
namespace Cf7\Bitrix\Lead\Generation\Includes;

class CrmFields
{
    public $fields;

    public $taskFields;

    public static $breakFields = [
        'lead' => [
            'STATUS_SEMANTIC_ID',
            'ADDRESS_COUNTRY_CODE',
            'ORIGINATOR_ID',
            'ORIGIN_ID'
        ],
        'deal' => [
            'CATEGORY_ID',
            'STAGE_SEMANTIC_ID',
            'IS_NEW',
            'TAX_VALUE',
            'COMPANY_ID',
            'CONTACT_ID',
            'CONTACT_IDS',
            'BEGINDATE',
            'CLOSEDATE',
            'CLOSED',
            'ADDITIONAL_INFO',
            'LOCATION_ID',
            'ORIGINATOR_ID',
            'ORIGIN_ID'
        ],
        'contact' => [
            'ADDRESS_COUNTRY_CODE',
            'EXPORT',
            'COMPANY_ID',
            'COMPANY_IDS',
            'ORIGINATOR_ID',
            'ORIGIN_ID',
            'ORIGIN_VERSION',
            'FACE_ID'
        ],
        'company' => [
            'ADDRESS_COUNTRY_CODE',
            'ADDRESS_LEGAL',
            'REG_ADDRESS',
            'REG_ADDRESS_2',
            'REG_ADDRESS_CITY',
            'REG_ADDRESS_POSTAL_CODE',
            'REG_ADDRESS_REGION',
            'REG_ADDRESS_PROVINCE',
            'REG_ADDRESS_COUNTRY',
            'REG_ADDRESS_COUNTRY_CODE',
            'LOGO',
            'IS_MY_COMPANY',
            'ORIGINATOR_ID',
            'ORIGIN_ID',
            'ORIGIN_VERSION'
        ],
        'task' => []
    ];

    public function __construct()
    {
        $this->fields = [
            'ADDRESS' => esc_html__('Street, building', 'cf7-bitrix24-lg'),
            'ADDRESS_2' => esc_html__('Suite / Apartment', 'cf7-bitrix24-lg'),
            'ADDRESS_CITY' => esc_html__('City', 'cf7-bitrix24-lg'),
            'ADDRESS_POSTAL_CODE' => esc_html__('Zip', 'cf7-bitrix24-lg'),
            'ADDRESS_REGION' => esc_html__('Region', 'cf7-bitrix24-lg'),
            'ADDRESS_PROVINCE' => esc_html__('State / Province', 'cf7-bitrix24-lg'),
            'ADDRESS_COUNTRY' => esc_html__('Country', 'cf7-bitrix24-lg'),
            'BIRTHDATE' => esc_html__('Birth date', 'cf7-bitrix24-lg'),
            'BANKING_DETAILS' => esc_html__('Payment details', 'cf7-bitrix24-lg'),
            'INDUSTRY' => esc_html__('Industry', 'cf7-bitrix24-lg'),
            'EMPLOYEES' => esc_html__('Employees', 'cf7-bitrix24-lg'),
            'REVENUE' => esc_html__('Annual income', 'cf7-bitrix24-lg'),
            'COMPANY_TITLE' => esc_html__('Company Name', 'cf7-bitrix24-lg'),
            'COMPANY_TYPE' => esc_html__('Company type', 'cf7-bitrix24-lg'),
            'OPENED' => esc_html__('Visible to everyone', 'cf7-bitrix24-lg'),
            'TITLE' => esc_html__('Title', 'cf7-bitrix24-lg'),
            'TYPE_ID' => esc_html__('Type', 'cf7-bitrix24-lg'),
            'STAGE_ID' => esc_html__('Stage / Pipeline', 'cf7-bitrix24-lg'),
            'PROBABILITY' => esc_html__('Probability, %', 'cf7-bitrix24-lg'),
            'NAME' => esc_html__('First name', 'cf7-bitrix24-lg'),
            'HONORIFIC' => esc_html__('Honorific', 'cf7-bitrix24-lg'),
            'LAST_NAME' => esc_html__('Last name', 'cf7-bitrix24-lg'),
            'SECOND_NAME' => esc_html__('Middle name', 'cf7-bitrix24-lg'),
            'POST' => esc_html__('Position', 'cf7-bitrix24-lg'),
            'RESPONSIBLE_ID' => esc_html__('Responsible user ID', 'cf7-bitrix24-lg'),
            'COMMENTS' => esc_html__('Comments', 'cf7-bitrix24-lg'),
            'DESCRIPTION' => esc_html__('Description', 'cf7-bitrix24-lg'),
            'SOURCE_DESCRIPTION' => esc_html__('Source Description', 'cf7-bitrix24-lg'),
            'STATUS_DESCRIPTION' => esc_html__('Status description', 'cf7-bitrix24-lg'),
            'OPPORTUNITY' => esc_html__('Opportunity', 'cf7-bitrix24-lg'),
            'CURRENCY_ID' => esc_html__('Currency', 'cf7-bitrix24-lg'),
            'PRODUCT_ID' => esc_html__('Product ID from CRM', 'cf7-bitrix24-lg'),
            'SOURCE_ID' => esc_html__('Source', 'cf7-bitrix24-lg'),
            'STATUS_ID' => esc_html__('Status', 'cf7-bitrix24-lg'),
            'PHONE' => esc_html__('Phone', 'cf7-bitrix24-lg'),
            'PHOTO' => esc_html__('Photo', 'cf7-bitrix24-lg'),
            'EMAIL' => esc_html__('E-mail', 'cf7-bitrix24-lg'),
            'WEB' => esc_html__('Site', 'cf7-bitrix24-lg'),
            'TAGS' => esc_html__('Tags', 'cf7-bitrix24-lg'),
            'DEADLINE' => esc_html__('Deadline (use the date field type)', 'cf7-bitrix24-lg'),
            'ALLOW_CHANGE_DEADLINE' => esc_html__('Responsible person can change deadline', 'cf7-bitrix24-lg'),
            'TASK_CONTROL' => esc_html__('Approve task when completed', 'cf7-bitrix24-lg'),
            'ALLOW_TIME_TRACKING' => esc_html__('Task planned time', 'cf7-bitrix24-lg'),
            'ASSIGNED_BY_ID' => esc_html__('Responsible', 'cf7-bitrix24-lg')
        ];

        $this->taskFields = [
            'TITLE' => [
                'type' => 'string',
                'isRequired' => true,
                'isReadOnly' => false
            ],
            'DESCRIPTION' => [
                'type' => 'string',
                'isRequired' => false,
                'isReadOnly' => false
            ],
            'RESPONSIBLE_ID' => [
                'type' => 'user',
                'isRequired' => true,
                'isReadOnly' => false
            ],
            'TAGS' => [
                'type' => 'string',
                'isRequired' => false,
                'isReadOnly' => false
            ],
            'DEADLINE' => [
                'type' => 'date',
                'isRequired' => false,
                'isReadOnly' => false
            ],
            'ALLOW_CHANGE_DEADLINE' => [
                'type' => 'char',
                'isRequired' => false,
                'isReadOnly' => false
            ],
            'TASK_CONTROL' => [
                'type' => 'char',
                'isRequired' => false,
                'isReadOnly' => false
            ],
            'ALLOW_TIME_TRACKING' => [
                'type' => 'char',
                'isRequired' => false,
                'isReadOnly' => false
            ]
        ];
    }

    private function __clone()
    {
    }
}