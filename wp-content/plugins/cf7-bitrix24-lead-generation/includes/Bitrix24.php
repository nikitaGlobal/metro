<?php
namespace Cf7\Bitrix\Lead\Generation\Includes;

class Bitrix24
{
    public static $scope = [
        'crm',
        'task',
        'tasks_extended'
    ];

    public static function send($sendFields, $crmFields, $formID, $currentType = 'lead')
    {
        $settings = get_option(Bootstrap::OPTIONS_KEY);
        $startLink = explode('rest', $settings['webhook']);

        $preparedFields = self::prepareFields($crmFields[$currentType], $sendFields[$currentType]);

        if (empty($preparedFields)) {
            return [];
        }

        $result = [];

        switch ($currentType) {
            case 'lead':
                $existLead = '';

                if (isset($sendFields['lead_update_exists'])) {
                    $leadID = self::findItemByField($sendFields, 'lead', 'PHONE');

                    if (!$leadID) {
                        $leadID = self::findItemByField($sendFields, 'lead', 'EMAIL');
                    }

                    if ($leadID) {
                        $existLead = self::sendApiRequest(
                            'crm.lead.get',
                            false,
                            [
                                'id' => $leadID
                            ]
                        );
                    }
                }

                if ($existLead) {
                    // fix duplicate phone
                    if (!empty($existLead['PHONE'])
                        && !empty($preparedFields['PHONE'])
                        && self::existEmailPhone($existLead['PHONE'], $preparedFields['PHONE'][0]['VALUE'])
                    ) {
                        unset($preparedFields['PHONE']);
                    }

                    // fix duplicate email
                    if (!empty($existLead['EMAIL'])
                        && !empty($preparedFields['EMAIL'])
                        && self::existEmailPhone($existLead['EMAIL'], $preparedFields['EMAIL'][0]['VALUE'])
                    ) {
                        unset($preparedFields['EMAIL']);
                    }

                    $result = self::sendApiRequest(
                        'crm.lead.update',
                        false,
                        [
                            'id' => $existLead['ID'],
                            'fields' => $preparedFields
                        ]
                    );

                    if (!empty($sendFields['uploads'])) {
                        self::sendUploadFiles($sendFields['uploads'], $existLead['ID'], 1);
                    }

                    self::sendLiveMessage($existLead['ID'], 1, $sendFields);
                } else {
                    if (!empty($preparedFields['ASSIGNED_BY_ID'])) {
                        $preparedFields['ASSIGNED_BY_ID'] = self::resolveNextResponsible(
                            $preparedFields['ASSIGNED_BY_ID'],
                            'lead',
                            $formID
                        );
                    }

                    if (!Helper::isVerify()) {
                        if (!empty($preparedFields['COMMENTS'])) {
                            $preparedFields['COMMENTS'] = Helper::nonVerifyText()
                                    . '<br>'
                                    . $preparedFields['COMMENTS'];
                        } else {
                            $preparedFields['COMMENTS'] = Helper::nonVerifyText();
                        }
                    }

                    $result = self::sendApiRequest('crm.lead.add', false, ['fields' => $preparedFields]);

                    if (isset($result[0]) && is_numeric($result[0]) && $startLink[0]) {
                        self::sendLiveMessage($result[0], 1, $sendFields);

                        if (!empty($sendFields['uploads'])) {
                            self::sendUploadFiles($sendFields['uploads'], $result[0], 1);
                        }

                        $userNotify = esc_html__('New lead', 'cf7-bitrix24-lg')
                            . ' [b]#'
                            . $result[0]
                            . '[/b] [url='
                            . $startLink[0] . 'crm/lead/show/' . $result[0] . '/'
                            . ']'
                            . $preparedFields['TITLE']
                            . '[/url] '
                            . esc_html__('from the site', 'cf7-bitrix24-lg')
                            . ' '
                            . get_home_url();

                        if (!empty($sendFields['task'])) {
                            $preparedFieldsTask = self::prepareFields($crmFields['task'], $sendFields['task']);
                            $preparedFieldsTask['UF_CRM_TASK'] = ['L_' . $result[0]];

                            $result = self::sendApiRequest('task.item.add', false, ['fields' => $preparedFieldsTask]);
                        }

                        self::sendApiRequest(
                            'im.notify',
                            false,
                            [
                                'to' => !empty($preparedFields['ASSIGNED_BY_ID']) ? $preparedFields['ASSIGNED_BY_ID'] : 1,
                                'message' => $userNotify,
                                'type' => 'SYSTEM'
                            ]
                        );
                    }
                }

                break;
            case 'deal':
                // Find or create contact
                if (!empty($sendFields['contact'])) {
                    $contactID = self::contactProcessing(
                        $sendFields,
                        self::prepareFields($crmFields['contact'], $sendFields['contact']),
                        $crmFields
                    );

                    // Set contact for deal
                    if ($contactID) {
                        $preparedFields['CONTACT_ID'] = $contactID;
                    }
                }

                // Find or create company
                if (!empty($sendFields['company'])) {
                    $companyID = self::findItemByField($sendFields, 'company', 'PHONE');

                    if (!$companyID) {
                        $companyID = self::findItemByField($sendFields, 'company', 'EMAIL');
                    }

                    if (!$companyID) {
                        $preparedFieldsCompany = self::prepareFields($crmFields['company'], $sendFields['company']);

                        if ($preparedFieldsCompany) {
                            $result = self::sendApiRequest(
                                'crm.company.add',
                                false,
                                ['fields' => $preparedFieldsCompany]
                            );

                            if ($result) {
                                $companyID = $result[0];
                            }
                        }
                    }

                    // Set company for deal
                    if ($companyID) {
                        $preparedFields['COMPANY_ID'] = $companyID;
                    }
                }

                // Pipeline support
                $isPipelineSatus = explode(':', $preparedFields['STAGE_ID']);

                if (count($isPipelineSatus) === 2) {
                    $preparedFields['CATEGORY_ID'] = str_replace('C', '', $isPipelineSatus[0]);
                }
                // Pipeline support

                if (!empty($preparedFields['ASSIGNED_BY_ID'])) {
                    $preparedFields['ASSIGNED_BY_ID'] = self::resolveNextResponsible(
                        $preparedFields['ASSIGNED_BY_ID'],
                        'deal',
                        $formID
                    );
                }

                if (!Helper::isVerify()) {
                    if (!empty($preparedFields['COMMENTS'])) {
                        $preparedFields['COMMENTS'] = Helper::nonVerifyText()
                            . '<br>'
                            . $preparedFields['COMMENTS'];
                    } else {
                        $preparedFields['COMMENTS'] = Helper::nonVerifyText();
                    }
                }

                $result = self::sendApiRequest('crm.deal.add', false, ['fields' => $preparedFields]);

                if (isset($result[0]) && is_numeric($result[0]) && $startLink[0]) {
                    self::sendLiveMessage($result[0], 2, $sendFields);

                    if (!empty($sendFields['uploads'])) {
                        self::sendUploadFiles($sendFields['uploads'], $result[0], 2);
                    }

                    $userNotify = esc_html__('New deal', 'cf7-bitrix24-lg')
                        . ' [b]#'
                        . $result[0]
                        . '[/b] [url='
                        . $startLink[0] . 'crm/deal/show/' . $result[0] . '/'
                        . ']'
                        . $preparedFields['TITLE']
                        . '[/url] '
                        . esc_html__('from the site', 'cf7-bitrix24-lg')
                        . ' '
                        . get_home_url();

                    if (!empty($sendFields['task'])) {
                        $preparedFieldsTask = self::prepareFields($crmFields['task'], $sendFields['task']);
                        $preparedFieldsTask['UF_CRM_TASK'] = ['D_' . $result[0]];

                        $result = self::sendApiRequest('task.item.add', false, ['fields' => $preparedFieldsTask]);
                    }

                    self::sendApiRequest(
                        'im.notify',
                        false,
                        [
                            'to' => !empty($preparedFields['ASSIGNED_BY_ID']) ? $preparedFields['ASSIGNED_BY_ID'] : 1,
                            'message' => $userNotify,
                            'type' => 'SYSTEM'
                        ]
                    );
                }
                break;
            case 'task':
                $preparedFields['UF_CRM_TASK'] = [];

                // Find or create contact
                if (!empty($sendFields['contact'])) {
                    $contactID = self::contactProcessing(
                        $sendFields,
                        self::prepareFields($crmFields['contact'], $sendFields['contact']),
                        $crmFields
                    );

                    // Set contact for task
                    if ($contactID) {
                        if (!empty($sendFields['uploads'])) {
                            self::sendUploadFiles($sendFields['uploads'], $contactID, 3);
                        }

                        $preparedFields['UF_CRM_TASK'][] = 'C_' . $contactID;
                    }
                }

                // Find or create company
                if (!empty($sendFields['company'])) {
                    $companyID = self::findItemByField($sendFields, 'company', 'PHONE');

                    if (!$companyID) {
                        $companyID = self::findItemByField($sendFields, 'company', 'EMAIL');
                    }

                    if (!$companyID) {
                        $preparedFieldsCompany = self::prepareFields($crmFields['company'], $sendFields['company']);

                        if ($preparedFieldsCompany) {
                            $result = self::sendApiRequest(
                                'crm.company.add',
                                false,
                                ['fields' => $preparedFieldsCompany]
                            );

                            if ($result) {
                                $companyID = $result[0];
                            }
                        }
                    }

                    // Set company for task
                    if ($companyID) {
                        $preparedFields['UF_CRM_TASK'][] = 'CO_' . $companyID;
                    }
                }

                if (!Helper::isVerify()) {
                    if (!empty($preparedFields['DESCRIPTION'])) {
                        $preparedFields['DESCRIPTION'] = Helper::nonVerifyText()
                            . '<br>'
                            . $preparedFields['DESCRIPTION'];
                    } else {
                        $preparedFields['DESCRIPTION'] = Helper::nonVerifyText();
                    }
                }

                $result = self::sendApiRequest('task.item.add', false, ['fields' => $preparedFields]);

                if (isset($result[0]) && is_numeric($result[0]) && $startLink[0]) {
                    $userNotify = esc_html__('New task', 'cf7-bitrix24-lg')
                        . ' [b]#'
                        . $result[0]
                        . '[/b] [url='
                        . $startLink[0] . 'company/personal/user/'
                        . (!empty($preparedFields['RESPONSIBLE_ID']) ? $preparedFields['RESPONSIBLE_ID'] : 1)
                        . '/tasks/task/view/'
                        . $result[0]
                        . '/'
                        . ']'
                        . $preparedFields['TITLE']
                        . '[/url] '
                        . esc_html__('from the site', 'cf7-bitrix24-lg')
                        . ' '
                        . get_home_url();

                    self::sendApiRequest(
                        'im.notify',
                        false,
                        [
                            'to' => !empty($preparedFields['RESPONSIBLE_ID']) ? $preparedFields['RESPONSIBLE_ID'] : 1,
                            'message' => $userNotify,
                            'type' => 'SYSTEM'
                        ]
                    );
                }

                break;
            case 'contact':
                $contactID = self::contactProcessing($sendFields, $preparedFields, $crmFields);

                if ($contactID) {
                    self::sendLiveMessage($contactID, 3, $sendFields);
                }
                break;
            case 'company':
                if (!Helper::isVerify()) {
                    if (!empty($preparedFields['COMMENTS'])) {
                        $preparedFields['COMMENTS'] = Helper::nonVerifyText()
                            . '<br>'
                            . $preparedFields['COMMENTS'];
                    } else {
                        $preparedFields['COMMENTS'] = Helper::nonVerifyText();
                    }
                }

                $result = self::sendApiRequest('crm.company.add', false, ['fields' => $preparedFields]);

                self::sendLiveMessage($result[0], 4, $sendFields);
                break;
            default:
                // Nothing
                break;
        }

        return $result;
    }

    public static function checkConnection()
    {
        $apiResponse = self::sendApiRequest('scope', true);

        if ($apiResponse && $apiResponse != self::$scope) {
            $errorScope = false;

            foreach (self::$scope as $scope) {
                if (!in_array($scope, $apiResponse)) {
                    $errorScope = true;
                }
            }

            if ($errorScope) {
                update_option(Bootstrap::OPTIONS_KEY, []);

                return 1;
            }
        }

        if (empty($apiResponse)) {
            update_option(Bootstrap::OPTIONS_KEY, []);

            return 2;
        }

        return 3;
    }

    public static function updateInformation()
    {
        self::updateFieldsList('crm.lead.fields', Bootstrap::LEAD_FIELDS_KEY);

        self::updateFieldsList('crm.deal.fields', Bootstrap::DEAL_FIELDS_KEY);
        self::updateFieldsList('crm.dealcategory.list', Bootstrap::DEAL_CATEGORY_LIST_KEY);

        self::updateFieldsList('crm.contact.fields', Bootstrap::CONTACT_FIELDS_KEY);
        self::updateFieldsList('crm.company.fields', Bootstrap::COMPANY_FIELDS_KEY);

        $crmFields = new CrmFields();
        update_option(Bootstrap::TASK_FIELDS_KEY, $crmFields->taskFields);

        self::updateCurrencyList();
        self::updateFieldsList('crm.status.list', Bootstrap::STATUS_LIST_KEY);
    }

    public static function updateFieldsList($method, $optionKey)
    {
        $apiResponse = self::sendApiRequest($method, true);

        if ($apiResponse) {
            update_option($optionKey, $apiResponse);
        }
    }

    public static function updateCurrencyList()
    {
        $apiResponse = self::sendApiRequest('crm.currency.list', true);

        if ($apiResponse) {
            $currencyList = [];

            foreach ($apiResponse as $currency) {
                $currencyList[$currency['CURRENCY']] = $currency['FULL_NAME'];
            }

            update_option(Bootstrap::CURRENCY_LIST_KEY, $currencyList);
        }
    }

    private static function contactProcessing($sendFields, $preparedFields, $crmFields)
    {
        $existContact = '';
        $contactID = '';
        $contacts = [];

        if (!isset($sendFields['always_create_new_contact'])) {
            $contactID = self::findItemByField($sendFields, 'contact', 'PHONE');

            if (!$contactID) {
                $contactID = self::findItemByField($sendFields, 'contact', 'EMAIL');
            }

            if ($contactID && isset($sendFields['contact_update_exists'])) {
                $existContact = self::sendApiRequest(
                    'crm.contact.get',
                    false,
                    [
                        'id' => $contactID
                    ]
                );
            }
        } else {
            $contacts = self::findAllItemsByField($sendFields, 'contact', 'PHONE');

            if (!$contacts) {
                $contacts = self::findAllItemsByField($sendFields, 'contact', 'EMAIL');
            }

            if ($contacts) {
                foreach ($contacts as $contact) {
                    self::sendLiveMessage($contact['ID'], 3, $sendFields);
                }
            }
        }

        if ($existContact) {
            $preparedFields = self::prepareFieldsToUpdate($crmFields['contact'], $sendFields['contact']);
            // fix duplicate phone
            if (!empty($existContact['PHONE'])
                && !empty($preparedFields['PHONE'])
                && self::existEmailPhone($existContact['PHONE'], $preparedFields['PHONE'][0]['VALUE'])
            ) {
                unset($preparedFields['PHONE']);
            }

            // fix duplicate email
            if (!empty($existContact['EMAIL'])
                && !empty($preparedFields['EMAIL'])
                && self::existEmailPhone($existContact['EMAIL'], $preparedFields['EMAIL'][0]['VALUE'])
            ) {
                unset($preparedFields['EMAIL']);
            }

            self::sendApiRequest(
                'crm.contact.update',
                false,
                [
                    'id' => $existContact['ID'],
                    'fields' => $preparedFields
                ]
            );

            return $existContact['ID'];
        } elseif (!$contactID && $preparedFields) {
            if (!Helper::isVerify()) {
                if (!empty($preparedFields['COMMENTS'])) {
                    $preparedFields['COMMENTS'] = Helper::nonVerifyText()
                        . "\n"
                        . $preparedFields['COMMENTS'];
                } else {
                    $preparedFields['COMMENTS'] = Helper::nonVerifyText();
                }
            }

            if ($contacts) {
                $preparedFields = apply_filters('cf7_bx_fields_for_duplicate', $preparedFields);
            }

            $result = self::sendApiRequest('crm.contact.add', false, ['fields' => $preparedFields]);

            if ($result) {
                return $result[0];
            }
        }

        return $contactID ? $contactID : false;
    }

    public static function existEmailPhone($values, $currentValue)
    {
        foreach ($values as $field) {
            if ($field['VALUE'] === $currentValue) {
                return true;
            }
        }

        return false;
    }

    public static function findItemByField($sendFields, $type, $field)
    {
        if (!empty($sendFields[$type][$field])) {
            $findParams = [
                'FILTER' => [
                    $field => $sendFields[$type][$field]
                ],
                'SELECT' => [
                    'ID'
                ]
            ];

            $findItem = self::sendApiRequest('crm.' . $type . '.list', false, $findParams);

            if ($findItem) {
                return $findItem[0]['ID'];
            }
        }

        return false;
    }

    public static function findAllItemsByField($sendFields, $type, $field)
    {
        if (!empty($sendFields[$type][$field])) {
            $findParams = [
                'FILTER' => [
                    $field => $sendFields[$type][$field]
                ],
                'SELECT' => [
                    'ID'
                ]
            ];

            $findItems = self::sendApiRequest('crm.' . $type . '.list', false, $findParams);

            if ($findItems) {
                return $findItems;
            }
        }

        return false;
    }

    public static function prepareFields($crmFields, $sendFields)
    {
        foreach ($crmFields as $key => $field) {
            if ($field['isRequired'] === true && empty($sendFields[$key])) {
                Helper::log('empty required field - ' . $key);

                return [];
            }

            if (in_array($key, ['PHONE', 'EMAIL', 'WEB']) && !empty($sendFields[$key])) {
                $sendFields[$key] = [
                    [
                        'VALUE' => $sendFields[$key],
                        'VALUE_TYPE' => 'WORK'
                    ]
                ];
            }
        }

        if (!empty($sendFields['COMMENTS'])) {
            $sendFields['COMMENTS'] = str_replace(
                "\n",
                '<br>',
                strip_tags($sendFields['COMMENTS'])
            );
        }

        return $sendFields;
    }

    public static function prepareFieldsToUpdate($crmFields, $sendFields)
    {
        foreach ($crmFields as $key => $field) {
            if ($sendFields[$key] === '') {
                unset($sendFields[$key]);
            }

            if (in_array($key, ['PHONE', 'EMAIL', 'WEB']) && !empty($sendFields[$key])) {
                $sendFields[$key] = [
                    [
                        'VALUE' => $sendFields[$key],
                        'VALUE_TYPE' => 'WORK'
                    ]
                ];
            }
        }

        if (!empty($sendFields['COMMENTS'])) {
            $sendFields['COMMENTS'] = str_replace(
                "\n",
                '<br>',
                strip_tags($sendFields['COMMENTS'])
            );
        }

        return $sendFields;
    }

    public static function resolveNextResponsible($list, $type, $formID)
    {
        $list = explode(',', $list);
        $list = array_map('trim', $list);

        if (count($list) === 1) {
            return $list[0];
        }

        $last = get_post_meta($formID, '_last_' . $type . '_responsible', true);
        $lastKey = array_search($last, $list);

        if (empty($last) || $lastKey === false || ($lastKey + 1) >= count($list)) {
            update_post_meta($formID, '_last_' . $type . '_responsible', $list[0]);

            return $list[0];
        }

        update_post_meta($formID, '_last_' . $type . '_responsible', $list[$lastKey + 1]);

        return $list[$lastKey + 1];
    }

    public static function sendLiveMessage($entityID, $entityType, $message)
    {
        if (!empty($message['livefeedmessage'])) {
            self::sendApiRequest(
                'crm.livefeedmessage.add',
                false,
                [
                    'fields' => [
                        'POST_TITLE' => 'Сообщение',
                        'MESSAGE' => $message['livefeedmessage'],
                        'ENTITYTYPEID' => $entityType, // 1 - lead, 2 - deal, 3 - contact, 4 - company
                        'ENTITYID' => $entityID
                    ]
                ]
            );
        }
    }

    public static function sendApiRequest($method, $showError = false, $fields = [])
    {
        $settings = get_option(Bootstrap::OPTIONS_KEY);

        Helper::log('POST - ' . $method, $fields);

        try {
            $response = wp_remote_post(
                $settings['webhook'] . $method,
                [
                    'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
                    'body' => $fields
                ]
            );

            Helper::log('bitrix raw response', $response);

            if (is_wp_error($response)) {
                throw new \Exception(
                    $response->get_error_message(),
                    (int) $response->get_error_code()
                );
            }

            $body = $response['body'];

            if (!empty($body)) {
                $result = json_decode(str_replace('\'', '"', $body), true);

                Helper::log('bitrix decode response', $result);

                if (isset($result['result'])) {
                    return (array) $result['result'];
                }

                if (!empty($result['error'])) {
                    if ($showError) {
                        throw new \Exception(
                            isset($result['error_message'])
                                ? esc_html($result['error_message'])
                                : esc_html($result['error_description']),
                            (int) $result['error']
                        );
                    }
                }
            }

            Helper::log('bitrix empty response', [], 'warning');
        } catch (\Exception $error) {
            Helper::log($error->getCode() . ': ' . $error->getMessage(), [], 'error');

            if ($showError) {
                printf(
                    '<div data-ui-component="cf7bitrix24noticesettings" class="error notice notice-error">'
                    . '<p><strong>Error (%d)</strong>: %s</p></div>',
                    (int) $error->getCode(),
                    esc_html($error->getMessage())
                );
            }
        }

        return [];
    }

    private static function sendUploadFiles($sendFiles, $entityID, $entityType)
    {
        $files = [];

        foreach ($sendFiles as $path) {
            $files[] = [
                basename($path),
                base64_encode(file_get_contents($path))
            ];
        }

        self::sendApiRequest(
            'crm.livefeedmessage.add',
            false,
            [
                'fields' => [
                    'MESSAGE' => esc_html__('Uploaded files', 'cf7-bitrix24-lg'),
                    'ENTITYTYPEID' => $entityType, // LEAD
                    'ENTITYID' => $entityID,
                    'FILES' => $files
                ]
            ]
        );
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }
}
