<?php
namespace Cf7\Bitrix\Lead\Generation\Includes;

class CF7
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        if ($this->isActive()) {
            add_action('wpcf7_mail_sent', [$this, 'onFormSubmit'], 10, 1);
        }
    }

    public function onFormSubmit(\WPCF7_ContactForm $contactForm)
    {
        if (!class_exists('\\WPCF7_Submission')) {
            return;
        }

        $submission = \WPCF7_Submission::get_instance();

        if (!$submission) {
            return;
        }

        $postedData = $submission->get_posted_data();

        if (!$postedData) {
            return;
        }

        // Compatible `Contact Form 7 Multi-Step Forms`
        if (function_exists('\\cf7msm_get')) {
            if (!empty($_POST['step'])) {
                $step = wp_unslash($_POST['step']);

                if (preg_match('/(\d+)-(\d+)/', $step, $matches)) {
                    $curr_step = $matches[1];
                    $last_step = $matches[2];

                    if ($curr_step != $last_step) {
                        return;
                    }
                }
            }

            $prevData = \cf7msm_get('cf7msm_posted_data', '');

            if (!is_array($prevData)) {
                $prevData = [];
            }

            $postedData = array_merge($prevData, $postedData);
        }

        $meta = get_post_meta($contactForm->id(), Bootstrap::META_KEY, true);

        // If empty form setting or not enabled send lead
        if (empty($meta) || !$meta['ENABLED']) {
            return;
        }

        $currentType = isset($meta['TYPE']) ? $meta['TYPE'] : 'lead';

        // If empty settings by type
        if (empty($meta[$currentType])) {
            return;
        }

        $postedData = $this->prepareSpecialMailTags($postedData);
        $postedData = $this->parseUtmCookie($postedData);

        // Set roistat client id
        $postedData['roistat_visit'] = isset($_COOKIE['roistat_visit'])
            ? $_COOKIE['roistat_visit']
            : '';

        // Set ga client id
        $postedData['gaClientID'] = '';

        if (!empty($_COOKIE['_ga'])) {
            $clientId = explode('.', wp_unslash($_COOKIE['_ga']));
            $postedData['gaClientID'] = $clientId[2] . '.' . $clientId[3];
        }

        $keys = array_map(function ($key) {
            return '[' . $key . ']';
        }, array_keys($postedData));
        $values = array_values($postedData);
        array_walk($values, function (&$value) {
            if (is_array($value)) {
                $value = implode('; ', $value);
            }
        });

        $data = [];
        $data['keys'] = $keys;
        $data['values'] = $values;

        $sendFields = [];
        $crmFields = [];

        if (isset($meta['contact_update_exists']) && $meta['contact_update_exists'] == 'true') {
            $sendFields['contact_update_exists'] = true;
        }

        if (isset($meta['always_create_new_contact']) && $meta['always_create_new_contact'] == 'true') {
            $sendFields['always_create_new_contact'] = true;
        }

        if (!empty($meta['livefeedmessage'])) {
            $sendFields['livefeedmessage'] = $this->replaceTagsToValue($postedData, $meta['livefeedmessage']);
            $sendFields['livefeedmessage'] = str_replace(
                ['<br>', '<br/>', '<br />'],
                '\n',
                $sendFields['livefeedmessage']
            );

            $sendFields['livefeedmessage'] = \strip_tags($sendFields['livefeedmessage']);
        }

        $data['files_for_contact_photo'] = $submission->uploaded_files();

        switch ($currentType) {
            case 'lead':
                if (isset($meta['send_files_lead']) && $meta['send_files_lead'] === 'true') {
                    $sendFields['uploads'] = $submission->uploaded_files();
                }

                $crmFields['lead'] = (array) get_option(Bootstrap::LEAD_FIELDS_KEY);
                $sendFields['lead'] = $this->prepareSendFields($crmFields['lead'], $meta[$currentType], $data);

                if (isset($meta['lead_create_task']) && $meta['lead_create_task'] == 'true') {
                    $crmFields['task'] = (array) get_option(Bootstrap::TASK_FIELDS_KEY);
                    $sendFields['task'] = $this->prepareSendFields($crmFields['task'], $meta['task'], $data);
                }

                if (isset($meta['lead_update_exists']) && $meta['lead_update_exists'] == 'true') {
                    $sendFields['lead_update_exists'] = true;
                }
                break;
            case 'deal':
                if (isset($meta['send_files_deal']) && $meta['send_files_deal'] === 'true') {
                    $sendFields['uploads'] = $submission->uploaded_files();
                }

                $crmFields['deal'] = (array) get_option(Bootstrap::DEAL_FIELDS_KEY);
                $crmFields['contact'] = (array) get_option(Bootstrap::CONTACT_FIELDS_KEY);
                $crmFields['company'] = (array) get_option(Bootstrap::COMPANY_FIELDS_KEY);
                $sendFields['deal'] = $this->prepareSendFields($crmFields['deal'], $meta[$currentType], $data);
                $sendFields['contact'] = $this->prepareSendFields($crmFields['contact'], $meta['contact'], $data);
                $sendFields['company'] = $this->prepareSendFields($crmFields['company'], $meta['company'], $data);

                if (isset($meta['deal_create_task']) && $meta['deal_create_task'] == 'true') {
                    $crmFields['task'] = (array) get_option(Bootstrap::TASK_FIELDS_KEY);
                    $sendFields['task'] = $this->prepareSendFields($crmFields['task'], $meta['task'], $data);
                }
                break;
            case 'task':
                $crmFields['task'] = (array) get_option(Bootstrap::TASK_FIELDS_KEY);
                $crmFields['contact'] = (array) get_option(Bootstrap::CONTACT_FIELDS_KEY);
                $crmFields['company'] = (array) get_option(Bootstrap::COMPANY_FIELDS_KEY);
                $sendFields['task'] = $this->prepareSendFields($crmFields['task'], $meta[$currentType], $data);
                $sendFields['contact'] = $this->prepareSendFields($crmFields['contact'], $meta['contact'], $data);
                $sendFields['company'] = $this->prepareSendFields($crmFields['company'], $meta['company'], $data);
                break;
            case 'contact':
                $crmFields['contact'] = (array) get_option(Bootstrap::CONTACT_FIELDS_KEY);
                $sendFields['contact'] = $this->prepareSendFields($crmFields['contact'], $meta[$currentType], $data);

                break;
            case 'company':
                $crmFields['company'] = (array) get_option(Bootstrap::COMPANY_FIELDS_KEY);
                $sendFields['company'] = $this->prepareSendFields($crmFields['company'], $meta[$currentType], $data);
                break;
            default:
                // Nothing
                break;
        }

        // Not use API if empty data
        if (empty($sendFields[$currentType])) {
            Helper::log('send empty form event by type- ' . $contactForm->id() . ' - ' . $currentType, []);
            Helper::log('send form event data - ' . $contactForm->id() . ' - ' . $currentType, $sendFields);

            return;
        }

        Helper::log('send form event - ' . $contactForm->id() . ' - ' . $currentType, $sendFields);
        Bitrix24::send($sendFields, $crmFields, $contactForm->id(), $currentType);
    }

    public function prepareSendFields($fields, $meta, $postedData)
    {
        $prepareFields = [];

        foreach ($fields as $key => $field) {
            $populateValue = isset($meta[$key . '-populate']) ? $meta[$key . '-populate'] : '';
            $value = isset($meta[$key]) ? $meta[$key] : '';

            if ($populateValue) {
                $populateValue = $this->replaceTagsToValue($postedData, $populateValue);
            }

            if ($populateValue) {
                $prepareFields[$key] = $populateValue;
            } elseif ($value) {
                $prepareFields[$key] = $this->replaceTagsToValue($postedData, $value);
            }

            if (isset($prepareFields[$key])) {
                // Prepare and populate value to list field
                if (
                    $field['type'] === 'enumeration' &&
                    !empty($field['items']) &&
                    !empty($prepareFields[$key])
                ) {
                    $explodedField = explode('; ', $prepareFields[$key]);
                    $resolveValues = [];

                    $ids = \array_column($field['items'], 'ID');
                    $values = \array_column($field['items'], 'VALUE');

                    foreach ($explodedField as $explodeValue) {
                        if (array_search($explodeValue, $ids) !== false) {
                            $resolveValues[] = $explodeValue;
                        } elseif (array_search($explodeValue, $values) !== false) {
                            $resolveValues[] = $ids[array_search($explodeValue, $values)];
                        }
                    }

                    $prepareFields[$key] = $field['isMultiple']
                        ? $resolveValues
                        : current($resolveValues);
                } elseif ($field['type'] === 'char' || $field['type'] === 'boolean') {
                    if (filter_var($prepareFields[$key], FILTER_VALIDATE_BOOLEAN)) {
                        $prepareFields[$key] = $field['type'] === 'char' ? 'Y' : 1;
                    } else {
                        $prepareFields[$key] = $field['type'] === 'char' ? 'N' : 0;
                    }
                }

                if ($key === 'HONORIFIC' && $populateValue) {
                    $prepareFields[$key] = $this->resolveStatusListValue('HONORIFIC', $prepareFields[$key]);
                }

                if (in_array($field['type'], ['string', 'url'], true) && $field['isMultiple']) {
                    $prepareFields[$key] = [$prepareFields[$key]];
                }
            }

            // resolve file field
            if ($field['type'] === 'file' && $prepareFields[$key] && !empty($postedData['files_for_contact_photo'])) {
                $resolvedFile = false;

                foreach ($postedData['files_for_contact_photo'] as $file) {
                    if (strpos($file, $prepareFields[$key]) !== false) {
                        $resolvedFile = [
                            'fileData' => [
                                basename($file),
                                base64_encode(file_get_contents($file))
                            ]
                        ];
                    }
                }

                if ($resolvedFile) {
                    $prepareFields[$key] = $resolvedFile;
                }
            }
        }

        return $prepareFields;
    }

    public function replaceTagsToValue($postedData, $value)
    {
        $value = trim(
            str_replace($postedData['keys'], $postedData['values'], $value)
        );

        if (function_exists('wpcf7_mail_replace_tags')) {
            $value = \wpcf7_mail_replace_tags($value);
        }

        return $value;
    }

    public function prepareSpecialMailTags($postedData)
    {
        $specialMailTags = [
            '_date',
            'utm_source',
            'utm_medium',
            'utm_campaign',
            'utm_content',
            'utm_term',
            'geoip_detect2_user_info', // Support GeoIP Detection plugin
            'tracking-info'  // Support Contact Form 7 Lead info with country plugin
        ];

        foreach ($specialMailTags as $smt) {
            // Support Contact Form 7 Lead info with country plugin
            if ($smt === 'tracking-info') {
                if (function_exists('\\wpshore_wpcf7_before_send_mail')) {
                    $result = \wpshore_wpcf7_before_send_mail(['body' => '[tracking-info]'], 1, new \stdClass());

                    $postedData[$smt] = $result['body'];
                }

                // another method since version 1.5.0
                if (function_exists('\\apa_wpcf7_before_send_mail')) {
                    $result = \apa_wpcf7_before_send_mail(['body' => '[tracking-info]'], 1, new \stdClass());

                    $postedData[$smt] = $result['body'];
                }
            } elseif ($smt === '_date') {
                $postedData[$smt] = date_i18n('Y-m-d');
            } else {
                $postedData[$smt] = apply_filters(
                    'wpcf7_special_mail_tags',
                    '',
                    $smt,
                    false
                );
            }
        }

        return $postedData;
    }

    public function parseUtmCookie($postedData)
    {
        $basedUtm = [
            'utm_source',
            'utm_medium',
            'utm_campaign',
            'utm_term',
            'utm_content'
        ];

        foreach ($basedUtm as $utm) {
            if (!isset($postedData[$utm])) {
                $postedData[$utm] = '';
            }
        }

        if (!empty($_COOKIE[Bootstrap::UTM_COOKIES])) {
            $utmParams = json_decode(wp_unslash($_COOKIE[Bootstrap::UTM_COOKIES]), true);

            foreach ($utmParams as $key => $value) {
                $postedData[$key] = rawurldecode(wp_unslash($value));
            }
        }

        return $postedData;
    }

    public function isActive()
    {
        $settings = get_option(Bootstrap::OPTIONS_KEY);

        return !empty($settings['webhook']);
    }

    private function resolveStatusListValue($type, $value)
    {
        $statusList = get_option(Bootstrap::STATUS_LIST_KEY);
        $checkList = [];

        foreach ($statusList as $status) {
            if ($status['ENTITY_ID'] === $type) {
                $checkList[$status['STATUS_ID']] = $status['NAME'];
            }
        }

        $ids = array_keys($checkList);
        $values = array_values($checkList);

        if (array_search($value, $ids) !== false) {
            return $value;
        } elseif (array_search($value, $values) !== false) {
            return $ids[array_search($value, $values)];
        }

        return '';
    }

    protected function __clone()
    {
    }
}
