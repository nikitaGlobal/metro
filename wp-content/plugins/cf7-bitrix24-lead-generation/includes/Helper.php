<?php
namespace Cf7\Bitrix\Lead\Generation\Includes;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Helper
{
    public static $log;

    public static function log($message, $data = [], $type = 'info')
    {
        $settings = get_option(Bootstrap::OPTIONS_KEY);
        $enableLogging = isset($settings['enabled_logging']) && (int) $settings['enabled_logging'] === 1;

        if ($enableLogging) {
            try {
                if (empty(self::$log)) {
                    self::$log = new Logger('cf7bx');
                    self::$log->pushHandler(
                        new StreamHandler(CF7_BITRIX24_PLUGIN_LOG_FILE, Logger::INFO)
                    );
                }

                self::$log->$type($message, (array) $data);
            } catch (\Exception $exception) {
                if (is_super_admin()) {
                    wp_die(
                        sprintf(
                            esc_html__(
                                'Error code (%s): %s.',
                                'cf7-bitrix24-lg'
                            ),
                            $exception->getCode(),
                            $exception->getMessage()
                        ),
                        esc_html__(
                            'An error occurred while writing the log file.',
                            'cf7-bitrix24-lg'
                        ),
                        [
                            'back_link' => true
                        ]
                    );
                    // escape ok
                }
            }
        }
    }

    public static function isVerify()
    {
        $value = get_site_option(Bootstrap::PURCHASE_CODE_OPTIONS_KEY);

        if (!empty($value)) {
            return true;
        }

        return false;
    }

    public static function nonVerifyText()
    {
        return esc_html__(
                'Please verify the purchase code on the plugin integration settings page - ',
                'cf7-bitrix24-lg'
            )
            . '<a href="'
            . admin_url()
            . 'admin.php?page=wpcf7-integration&service=cf7-bitrix-lead-generation&action=setup">'
            . admin_url()
            . 'admin.php?page=wpcf7-integration&service=cf7-bitrix-lead-generation&action=setup</a>';
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }
}
