<?php
namespace Cf7\Bitrix\Lead\Generation\Includes;

class Cron
{
    private static $instance = false;

    protected function __construct()
    {
        add_action('init', [$this, 'createCron']);
        add_action(Bootstrap::CRON_TASK, [$this, 'cronAction']);
        add_filter('cron_schedules', [$this, 'addSchedule']);
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function createCron()
    {
        if (!wp_next_scheduled(Bootstrap::CRON_TASK)) {
            wp_schedule_event(time(), 'every5minutes', Bootstrap::CRON_TASK);
        }
    }

    public function cronAction()
    {
        $last = get_option('cf7-bx24-integration-last-cron');

        if (!empty($last) && date_i18n('Y-m-d') == $last) {
            return;
        }

        $settings = get_option(Bootstrap::OPTIONS_KEY);

        if (!empty($settings['webhook'])) {
            update_option('cf7-bx24-integration-last-cron', date_i18n('Y-m-d'));

            Bitrix24::updateInformation();
        }
    }

    public function addSchedule($schedules)
    {
        $schedules['every5minutes'] = ['interval' => 300]; // 300 seconds

        return $schedules;
    }

    private function __clone()
    {
    }
}
