<?php
/**
 * Plugin Name: Contact Form 7 - Bitrix24 CRM - Integration
 * Description: Allows you to integrate your forms and Bitrix24 CRM
 * Version: 2.21.0
 * Author: itgalaxycompany
 * Author URI: https://codecanyon.net/user/itgalaxycompany
 * Network: true
 * License: GPLv3
 * Text Domain: cf7-bitrix24-lg
 * Domain Path: /languages/
 */

use Cf7\Bitrix\Lead\Generation\Admin\CF7 as CF7Admin;
use Cf7\Bitrix\Lead\Generation\Includes\Bitrix24Select;
use Cf7\Bitrix\Lead\Generation\Includes\Bootstrap;
use Cf7\Bitrix\Lead\Generation\Includes\CF7 as CF7Includes;
use Cf7\Bitrix\Lead\Generation\Includes\Cron;

if (!defined('ABSPATH')) {
    exit();
}

/*
 * Require for `is_plugin_active` function.
 */
require_once ABSPATH . 'wp-admin/includes/plugin.php';

load_theme_textdomain('cf7-bitrix24-lg', __DIR__ . '/languages');

define('CF7_BITIX24_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CF7_BITIX24_PLUGIN_VERSION', '2.21.0');
define('CF7_BITRIX24_PLUGIN_DIR', __DIR__);

if (!defined('CF7_BITRIX24_PLUGIN_LOG_FILE')) {
    define('CF7_BITRIX24_PLUGIN_LOG_FILE', __DIR__ . '/logs/.cf7bx.log');
}

require __DIR__ . '/vendor/autoload.php';

Bootstrap::getInstance(__FILE__);
CF7Includes::getInstance();
Bitrix24Select::getInstance();

if (defined('DOING_CRON') && DOING_CRON) {
    Cron::getInstance();
}

if (is_admin() && is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
    add_action('plugins_loaded', function () {
        CF7Admin::getInstance();
    });
}
