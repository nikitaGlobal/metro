=== Contact Form 7 - Bitrix24 CRM - Integration ===
Contributors: https://codecanyon.net/user/itgalaxycompany
Tags: bitrix24, bitrix24 leads, business leads, contact form 7, contact form 7 bitrix24, form, integration, lead finder, lead management, lead scraper, leads, marketing leads, sales leads, crm

== Description ==

The main task of this plugin is a send your Contact Form 7 forms directly to your Bitrix24 account.

= Features =

* Integrate your `Contact Form 7` forms with Bitrix24 CRM;
* Works with any edition of Bitrix24 CRM;
* Your can choice that your want to generate - lead, deal, task, contact or company;
* Creation of the deal and the task, occurs together with the creation / binding of the contact and the company. (if their fields are filled);
* Creation of notifications in Bitrix24 CRM when adding a lead, deal and task.
* Fields are loaded from the CRM (including custom fields) (except for tasks);
* You can set up each form personally, specify which information you want to get;
* Integrate unlimited Contact Form 7 forms;
* Multiple deal pipeline support;
* Supports getting `utm` params from the `URL`;
* Supports for sending `GA Client ID`;
* Supports for sending `roistat_visit` cookie;
* Supports for uploaded files for types `lead` and `deal`;
* Compatible with `Contact Form 7 Multi-Step Forms`. (when configuring, you need to fill in the fields with all the steps in the last form);
* Image previews;
* Super easy to set-up;

== Installation ==

1. Extract `cf7-bitrix24-lead-generation.zip` and upload it to your `WordPress` plugin directory
(usually /wp-content/plugins ), or upload the zip file directly from the WordPress plugins page.
Once completed, visit your plugins page.
2. Be sure `Contact Form 7` Plugin is enabled.
3. Activate the plugin through the `Plugins` menu in WordPress.
4. Go to your `Bitrix24` -> `Applications` -> `Web hooks`.
5. Click `ADD WEB HOOK`. Choose `Inbound web hook`.
6. Check `Tasks`, `Tasks (extended permissions)`, `CRM` and `Chat and Notifications (im)`. Click the button `SAVE`.
7. Copy value from `REST call example URL` without `profile/`.
8. Go to the `Contact Form 7` -> `Integration`.
9. Find `Integration with Bitrix24` and click the button `Go to setup`.
10. Insert in the field `Inbound web hook` copied value.
11. Save settings.
12. When editing forms your can see the tab `Bitrix24`.

== Changelog ==

= 2.21.0 =
Feature: support set contact photo by uploaded image.

= 2.20.2 =
Fixed: compatibility with `WP Fastest Cache`.
Сhore: support additional tags started with `pm_`.
Feature: support for processing utm tags when using caching plugins.
Chore: use composer autoloader.
Feature: if "always create new" is selected, then the plugin, in addition to creating a new one, searches for existing ones and adds an entry to their live feed.
Feature: ability to always create a new contact without searching for an existing one.

= 2.17.1 =
Fixed: live feed message clean html.
Feature: ability to create a comment in the live feed of the entry (lead/deal/contact/company).
Feature: format for special tag `_date` now `Y-m-d`.

= 2.15.2 =
Fixed: skip empty required fields when updating a contact.
Fixed: possible problem when filling out a boolean field.
Feature: update for an existing contact (search by phone and email), before creating a new one.

= 2.14.3 =
Chore: ability to override the log file path.
Fixed: filling in the value for the text field, if multiple is enabled.
Fixed: populate value `HONORIFIC` from the form field.

= 2.14.0 =
Feature: more support "pipes".

= 2.13.4 =
Fixed: populate multiple values to list.

= 2.13.3 =
Fixed: populate values to list not from field `bitrix24_select`.

= 2.13.2 =
Chore: more error handling.

= 2.13.1 =
Fixed: blank values for missing utm.
Fixed: check maybe no extension `php-mbstring`.

= 2.13.0 =
Feature: Support sending cookie `roistat_visit` to CRM.

= 2.12.1 =
Fixed: compatibility `Contact Form 7 Lead info with country`.

= 2.12.0 =
Feature: Support for uploaded files for types `lead` and `deal`.

= 2.11.0 =
Feature: added compatibility `Contact Form 7 Lead info with country`.
Fixed: possible problem with duplication of phone / mail when updating the lead.

= 2.10.1 =
Feature: use any `utm_` params in `URL`.
Chore: more logs.

= 2.10.0 =
Feature: added the ability to log requests to CRM (disabled by default).

= 2.9.0 =
Feature: multiple responsible for type lead and deal.

= 2.8.2 =
Fixed: special mail tags support.
Chore: added `user-agent` header to requests.

= 2.8.1 =
Fixed: Populate the value of the `bitrix` list.

= 2.8.0 =
Feature: Support for `multiple` parameter in `bitrix24_select`.
Feature: Ability to send value in the yes / no field from the form.

= 2.7.0 =
Feature: Update for an existing lead (search by phone and email), before creating a new one.

= 2.6.0 =
Feature: Support for `GA Client ID`.
Fixed: special mail tags support.

= 2.5.0 =
Feature: Support for multi pipeline for the deal.

= 2.4.0 =
Feature: Support for `utm` params in `URL`.
Feature: Creating a task together with a deal or lead.
Feature: A new shortcode `bitrix24_select` is added to generate a selection field from Bitrix24 values.

= 2.3.1 =
Fixed: API creation of the `contact` is changed.

= 2.3.0 =
Changed: Saving integration settings on ajax.
Fixed: Automatic set of the trailing slash for Webhook.

= 2.2.0 =
Feature: Added the creation of notifications in Bitrix24 CRM when adding a lead, deal and task.
Updated: Documentation.
Updated: Preview screenshots.

= 2.1.1 =
Fixed: Check whether plugin `Contact Form 7` is active on the `Network`.

= 2.1.0 =
Feature: Added compatibility `Contact Form 7 Multi-Step Forms`.

= 2.0.0 =
Changed: Use `web hooks`.
Changed: The fields are now loaded from the CRM (including custom fields) (except for tasks).
Added: Ability to send `deal`, `task`, `company` or `contact`.
Added: Wordpress cron task to update fields from CRM.
Updated: Preview screenshots.
Updated: Translate.

= 1.0.0 =
Initial public release
