<?php
/*
Plugin Name: AntiBot for Contact Form 7
Plugin URI: N/A
Description: A custom wordpress form to make wp contact form more security
Version: 0.2
Author: Yuri P.
Author URI: https://prikhodko.pro
Website: https://prikhodko.pro
Email: hello@prikhodko.pro
License: Close Source/Private
Last Updated: 16-Aug-2019
*/


/* Include customized contact form 7 */

function form_includes(){
	//Add custom functions to CF7
	require_once plugin_dir_path(__FILE__) . 'includes/captcha.php';
}
add_action('init','form_includes');


/* Activation plugin */
register_activation_hook( __FILE__,function(){
	$file = realpath('../')."/wp-content/plugins/antibot-cf7/includes/config.php";
	$key = generateRandomString($length = rand(12, 36));
		file_put_contents($file, '<?php define("KEY", "'.$key.'"); ?>' );
 });

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
