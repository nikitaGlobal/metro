<?php
/*
 * Bot Protection
 *
 * Copyright 2019, Yuri P.
 * https://prikhodko.pro
 *
 * License: Close Source/Private
 */

require_once 'config.php';
define("WKEY", "05IFg0uaG38p");

function add_new_tag($arr){
	$form = WPCF7_ContactForm::get_current();
	$hash = createHash();
	$arr['_wpcf7_key'] = WKEY;
	return $arr;
}
add_filter('wpcf7_form_hidden_fields', 'add_new_tag');

function field_check($result) { 
	$form = WPCF7_Submission::get_instance();
	$hash  = $form->get_posted_data('_wpcf7_key');
	$message = "Ошибка Captcha! Обновите страницк и попробуйте еще раз!";
 	if (!detectBot($hash)) {
      $result->invalidate(array('type' => 'captcha', 'name' => 'antibot-cf7'), $message);
   }
   return $result;
}; 
add_filter('wpcf7_validate', 'field_check'); 

function createHash(){
   $date = (new DateTime)
   	->setTimezone(new DateTimeZone('Europe/Moscow'));
   $hash = md5(KEY.$date->format('d/m/Y:H'));
   return $hash;
}
function detectBot($hash){
   $date = (new DateTime)
   	->setTimezone(new DateTimeZone('Europe/Moscow'));
   $now = md5(KEY.$date->format('d/m/Y:H'));
   $past = md5(KEY.$date->sub(new DateInterval('PT60M'))->format('d/m/Y:H'));
   if ($hash == $now || $hash == $past){
      return true;
   }else{
   	return false;
   }
}

function key_send(){
   $wkey = $_POST['key'];
   if ($wkey == WKEY) {
      echo createHash();
   }
   die();
}
 
add_action('wp_ajax_CF7_test_key', 'key_send');
add_action('wp_ajax_nopriv_CF7_test_key', 'key_send');

add_action( 'wp_footer', 'hook_javascript', 10);
function hook_javascript(){ ?>
   
   <script type='text/javascript'>
      jQuery(document).ready(function($){
         $('.wpcf7-validates-as-required').blur(function(){
            var form = $(this).closest('form'),
               key = form.find('input[name="_wpcf7_key"]');
            if(!key.prop('readonly')){
               $.ajax({
                  type:'POST',
                  url:'/wp-admin/admin-ajax.php',
                  data:'action=CF7_test_key&key='+key.val(),
                  success:function(data){
                     key.val(data);
                     key.prop('readonly',true);
                  }
               });
            }
         });
      });
   </script>

<?php }