=== WordPress Currency Switcher (WPCS) ===
Contributors: RealMag777
Donate link: https://pluginus.net/affiliate/wordpress-currency-switcher
Tags: currency, switcher, currency switcher, converter, marketing
Requires at least: 3.5.0
Tested up to: 5.2
Requires PHP: 5.4
Stable tag: 1.1.4

WordPress Currency Switcher is WordPress currency plugin that allows to switch prices currencies in your site content and get their rates converted in the real time!

== Description ==

**WordPress Currency Switcher** is WordPress currency plugin, that allows your site visitors switch prices currencies in your site content according to set currencies rates in the real time!

Power marketing tool for WordPress sites where its necessary display content in more than one currency. Ideal solution to make your site commercial suggestion more clear for customers from different countries! Good for any marketing programs, catalogs, portfolios and any commercial text-content (propositions). 

WordPress Currency Switcher is available as shortcode **[[wpcs]](https://wordpress.currency-switcher.com/shortcode/wpcs/)** so as the widget. Insert prices into your content by shortcode [[wpcs_price value=20]](https://wordpress.currency-switcher.com/shortcode/wpcs_price/)

Demo: [demo.currency-switcher.com](https://wordpress.currency-switcher.com/wordpress-currency-switcher-action/)

FAQ: [currency-switcher.com/category/faq](https://wordpress.currency-switcher.com/category/faq/)

API: [currency-switcher.com/codex](https://wordpress.currency-switcher.com/codex/)

Latest PHP 7.3.x – FULL COMPATIBILITY!


### WordPress Currency Switcher Features:

&#9989;&nbsp;**Representation:** Currency Switcher is available as a widget and works in any widgetized area, for flexibility the shortcode is also available [[wpcs]](https://wordpress.currency-switcher.com/shortcode/wpcs/).  You can insert shortcode [wpcs] in any place of your site, [even in the top menu](https://wordpress.currency-switcher.com/how-to-drop-wordpress-currency-switcher-in-menu/).

Also the plugin has ajaxed shortcode/widget of [currency converter](https://wordpress.currency-switcher.com/currency-converter-shortcode-demo/) and ajaxed shortcode/widget of [currency rates](https://wordpress.currency-switcher.com/exchange-rates-shortcode-demo/)

Insert prices into your content by shortcode [[wpcs_price value=20]](https://wordpress.currency-switcher.com/shortcode/wpcs_price/)

&#9989;&nbsp;**Design**: graphically WordPress Currency Switcher can be represented in 3 different ways: drop-down, flags, [side switcher](https://wordpress.currency-switcher.com/wordpress-currency-switcher-action/). For each currency it is possible to set its own flag.

&#9989;&nbsp;**Rates**: set of currencies aggregators for automatic rates changing. Also admin can set rates manually if it's necessary!

&#9989;&nbsp;**Rates auto update**: update currency rates hourly, twice daily, daily, weekly, monthly by cron. Or you can disable it and set your own currency rates by hands!

&#9989;&nbsp;**Price**: set price format which fit your needs - decimals count, usual money sign or custom sign, money sign position (4 variants). You can show or hide cents for each currency optionally.

&#9989;&nbsp;**Possible to take the price value directly from meta field**: [wpcs_price meta_value=my_price_field], value of meta field should be decimal or integer

&#9989;&nbsp;**GEO IP rules**: let your site visitors see prices in their country currency! Flexible options which allows to set what currency to display to each country

&#9989;&nbsp;**Fixed prices options**: [[wpcs_price]](https://wordpress.currency-switcher.com/shortcode/wpcs_price/) - If you want to set for each currency its own price and avoid recounting relatively of basic currency: [wpcs_price type="fixed" value="USD:15,EUR:20,GBP:45"] . If you want use meta field instead of attribute 'value' write shortcode as [wpcs_price meta_value=my_price_field type="fixed"] and value of meta field should has next syntax: USD:15,EUR:20,GBP:45

&#9989;&nbsp;**Custom money signs**: create and use your own money symbols you need. It is possible even use currency which not exists in the reality!

&#9989;&nbsp;**Custom price formats**: each currency can has its own format where price and money sign can be set on the side you want

&#9989;&nbsp;**Video to understand basics**:

https://www.youtube.com/watch?v=1CLRP_tDj0k

Note: for today design of the plugin is different of the video (is improved), see screenshots below!

&#9989;&nbsp;**Welcome currency**: allows to set any price currency you want for your site visitors first visit. So if your site currency is INR and you want let your customers on their first visit see prices converted to USD you just need to set 'Welcome currency' in WPCS options.

&#9989;&nbsp;**Compatibility with cache plugins**: if your site uses any cache plugins enable option 'I am using cache plugin on my site', reset the site cache and from now your site visitors can switch currencies without any problems!

&#9989;&nbsp;**Price info icon**: show info icon near the price which while its under mouse hover shows prices in all other currencies

&#9989;&nbsp;**Prices without cents**: recounts prices without cents for such currencies like JPY or TWD which by its nature have not cents. Test it for checkout after setup!

&#9989;&nbsp;**Possible to change currency according to the language**: if you you using WPML or Polylang plugins in your site and by business logic you want to set currency according to the current language [it is possible with WPCS API](https://wordpress.currency-switcher.com/switch-currency-with-language-change/)

&#9989;&nbsp;WPCS understand currency in the site link as [wordpress.currency-switcher.com/wordpress-currency-switcher-action/?currency=EUR](https://wordpress.currency-switcher.com/wordpress-currency-switcher-action/?currency=EUR)

&#9989;&nbsp;Decimal separator optional

&#9989;&nbsp;Thousandth separator optional

&#9989;&nbsp;**Wide API**: advanced [API functionality set](https://wordpress.currency-switcher.com/codex/) which allows to manipulate with prices and their rates on the fly using conditional logic

&#9989;&nbsp;Easy to use for administrators and site customers

&#9989;&nbsp;**Strong technical support which each day works with tones of code!**




### PREMIUM FEATURES
* All features above
* Unlimited count of currencies (in the free version 2 currencies available)
* Get premium version of the plugin on: [**CODECANYON**](https://pluginus.net/affiliate/wordpress-currency-switcher)



### Make your site more profitable with next powerful scripts:

&#9989;&nbsp;[WOOF - WooCommerce Products Filter](https://wordpress.org/plugins/woocommerce-products-filter/): extendable, flexible and robust plugin for WooCommerce that allows your site customers filter products by products categories, attributes, tags, custom taxonomies and price. Supports latest version of the WooCommerce plugin. A must have plugin for your WooCommerce powered online store! Maximum flexibility!

&#9989;&nbsp;[WOOBE - WooCommerce Bulk Editor Professional](https://wordpress.org/plugins/woo-bulk-editor/): WordPress plugin for managing and bulk edit WooCommerce Products data in robust and flexible way! Be professionals with managing data of your woocommerce e-shop!

&#9989;&nbsp;[WOOCS - WooCommerce Currency Switcher](https://wordpress.org/plugins/woocommerce-currency-switcher/): Woo currency plugin, that allows your site visitors switch products prices currencies according to set currencies rates in the real time and pay in the selected currency (optionally). Ideal solution to make the serious WooCommerce store site in multiple currencies!

&#9989;&nbsp;[MDTF - WordPress Meta Data Filter & Taxonomies Filter](https://wp-filter.com/): the plugin for filtering and searching WordPress content in posts and their custom types by taxonomies and meta data fields. The plugin has very high flexibility thanks to its rich filter elements and in-built meta fields constructor!

&#9989;&nbsp;[WPBE - WordPress Posts Bulk Editor Professional](https://wordpress.org/plugins/bulk-editor/): is WordPress plugin for managing and bulk edit WordPress posts, pages and custom post types data in robust and flexible way! Be professionals with managing data of your site!


== Installation ==
* Download to your plugin directory or simply install via WordPress admin interface.
* Activate.
* Use.


== Frequently Asked Questions ==

* Where to find the plugin options page? Here: wp-admin/options-general.php?page=currency-switcher-settings

* [Demo page](https://wordpress.currency-switcher.com/wordpress-currency-switcher-action/)

* [Documentation](https://wordpress.currency-switcher.com/codex/)

* How to add new currency? In the plugin settings page by 'Add currency' button

* Flags? [Here!](https://wordpress.currency-switcher.com/can-get-flags/)


== Screenshots ==

1. Currencies tab settings
2. The plugin options tab
3. The plugin options tab
4. Side switcher options tab
5. GeoIP rules tab
6. FAQ tab
7. Front: side switcher on the right, currency switcher as drop-down in the page content inserted by shortcode and as widget on the right, WordPress Currency Converter widget, WordPress Currency Rates widget


== Changelog ==

= 1.1.4 =
* small issues fixed
* removed currency aggregators which stopped to work
* added aggregators: Fixer, The Free Currency Converter by European Central Bank

= 1.1.3 =
* removed currency aggregators which stopped to work
* added back Google and Yahoo currency aggregators

= 1.1.2 =
* new feature: Side switcher - https://wordpress.currency-switcher.com/documentation/#section_1_1
* new hook https://wordpress.currency-switcher.com/hook/wpcs_currency_manipulation_before_show/
* added in options decimal separator
* added in options thousandth separator
* Google aggregator removed as the service been closed
* Added new aggregator XE Currency Converter

= 1.1.1 =
* Yahoo aggregator removed as it failed completely
* Google aggregator changed its API and its fixed in WPCS code
* New aggregator added: https://free.currencyconverterapi.com/

= 1.1.0 =
* some little fixes
* new feature GEO IP rules
* new attribute meta_value for shortcode [wpcs_price] https://wordpress.currency-switcher.com/shortcode/wpcs_price/
* new shortcode [wpcs_check_country]
* Fixed prices: Can I fix the prices for different currencies rather than auto-calculate? For example: US price: $100, Canadian price $75. -> [wpcs_price type="fixed" value="USD:15,EUR:20,GBP:45"]

= 1.0.2 =
* Some fixes and functionality changes

= 1.0.1 =
* One improvement to make currency selection permanent

= 1.0.0 =
* The plugin release


== Upgrade Notice ==



== License ==

This plugin is copyright pluginus.net &copy; 2012-2019 with [GNU General Public License][] by realmag777.

This program is free software; you can redistribute it and/or modify it under the terms of the [GNU General Public License][] as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY. See the GNU General Public License for more details.

[GNU General Public License]: http://www.gnu.org/copyleft/gpl.html



== Upgrade Notice ==
[Look here for ADVANCED version of the plugin](https://pluginus.net/affiliate/wordpress-currency-switcher)

