<?php
/**
 * Plugin Name: lightbox2
 * Description: The original lightbox script. Eight years later — still going strong! 
 * Plugin URI:  https://lokeshdhakar.com/projects/lightbox2/
 * Author URI:  https://sayri.work/
 * Author:      SAYri, Lokesh Dhakar
 * Version:     2.11.1
 */


//Loads javascript
function lightbox2_scripts() {
	wp_enqueue_script( 'lightbox2', plugin_dir_url( __FILE__ ) . 'js/lightbox.min.js', array('jquery'), '', true );

}
add_action( 'wp_enqueue_scripts', 'lightbox2_scripts' );

//Loads CSS
function lightbox2_styles() {
	wp_enqueue_style('lightbox', plugin_dir_url( __FILE__ ) . 'css/lightbox.min.css', array() );


}
add_action( 'wp_enqueue_scripts', 'lightbox2_styles' );
