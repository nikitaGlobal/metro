(function ( $, undefined ) {
  $( function () {
    var img_src = jQuery( '#stickyMenu .logoContainer img' ).attr( 'src' );

    img_src = img_src.substring( img_src.lastIndexOf( '/' ) + 1,  img_src.length );

    window.onscroll = function () {myFunction();};

// Get the header

// Get the offset position of the navbar
    var sticky = document.querySelector( '.featureProjects' ).offsetTop;

// Add the sticky class to the header when you reach its scroll position.
// Remove "sticky" when you leave the scroll position

    function myFunction () {

      if ( window.pageYOffset > sticky ) {
        $( '#stickyMenu' ).addClass( 'scroll' );
        $( '#stickyMenu .logoContainer img' ).
          attr( 'src',
            '/wp-content/themes/framework/assets/images/mpd-black-logo.svg' );
      }
      else {
        $( '#stickyMenu' ).removeClass( 'scroll' );
        $( '#stickyMenu .logoContainer img' ).
          attr( 'src',
            '/wp-content/themes/framework/assets/images/' + img_src );
      }
    }
    if (navigator.userAgent.match(/(iPad|iPhone|iPod|Android|Silk)/gi) && $('.full-width_block').length) {
      $('.full-width_block').addClass('mobile');
    }
    //Start open/closed menu
    $( '.menuOpen' ).on( 'click', function () {
      //$( '.mobileMenu' ).show();
      //$( this ).parent().parent().attr( 'id', '' );
      //$( this ).parent().parent().addClass( 'mobileMenu' );
      if($(this).hasClass('opened')){
        $(document).find('.mobileMenu').attr( 'id', 'stickyMenu' );
        $(document).find('.mobileMenu').removeClass( 'mobileMenu' );
        document.body.style.overflow = 'visible';
        if (! $(document).find('#stickyMenu').hasClass( 'scroll' ) ) {
          $( '#stickyMenu .logoContainer img' ).
          attr( 'src',
              '/wp-content/themes/framework/assets/images/' + img_src );
        }
      }else{
        $(document).find('#stickyMenu').addClass( 'mobileMenu' );
        $(document).find('#stickyMenu').attr('id', '');
        document.body.style.overflow = 'hidden';
        $( '.mobileMenu .logoContainer img' ).
        attr( 'src',
            '/wp-content/themes/framework/assets/images/mpd-black-logo.svg' );
      }
      $(this).toggleClass('opened');
    } );
    //End open/closed menu

    //Start bild search form home
    var searchAction = $( '#topSearch form' );
    $( '.searchCat' ).on( 'click', function () {

      let cat = $( this ).data( 'id' ),
        slug = $( this ).data( 'slug' ),
        data_site = {
          'action': 'ajax_load_community',
          'term_id': cat,
        };

      $( document ).find( '.searchCat' ).removeClass( 'active' );
      $( this ).addClass( 'active' );
      slug ? searchAction.attr( 'action', '/emirate/' + slug ) : '';
      $( '#searchType' ).val( $( this ).data( 'id' ) );

      $.ajax( {
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function ( data ) {
          $( '#topSearch .locationContainer' ).html( data );
          $( '#loc_select' ).select2( { placeholder: 'Location' } );
        },
        error: function () {
          console.log( 'Ajax error !!' );
        },
      } );

    } );

    //Set id in search category
    if ( $( '.home #topSearch' ).length ) {
      $( '#searchType' ).val( $( 'a.searchCat.active' ).data( 'id' ) );
    }

//---------------------------------------------------------------------------

    //Start add form currency class
    $( 'select.currency-switcher' ).addClass( 'form-control' );
    //End add form currency class

    //Start ajax load post agent
    $( '.agentsViewMore' ).on( 'click', function ( e ) {
      e.preventDefault();
      var img = $( this ).children().find( 'img' );
      var count = $( this ).data( 'count' );
      var data_site = {
        'action': 'ajax_load_agents',
        'current_page': current_page,
        'count': count,
      };

      img.addClass( 'active' );

      $.ajax( {
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function ( data ) {
          if ( data ) {
            current_page++;
            img.removeClass( 'active' );
            $( '.row.propertiesRow' ).append( data );
            if ( current_page === Number( max_pages ) ) {
              $( '.viewAll' ).
                remove();
            }
          }
          else {
            $( '.viewAll' ).remove();
          }
        },
        error: function () {
          console.log( 'Ajax error !!' );
          img.removeClass( 'animation' );
        },
      } );
    } );
    //END ajax load post agent

    //Start ajax load post object home
    $( '.objectViewMore' ).on( 'click', function ( e ) {
      e.preventDefault();
      var img = $( this ).children().find( 'img' ),
        count = $( this ).data( 'count' ),
        data_site = {
          'action': 'ajax_load_object',
          'current_page': current_page,
          'count': count,
        };

      img.addClass( 'active' );

      $.ajax( {
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function ( data ) {
          if ( data ) {
            current_page++;
            img.removeClass( 'active' );
            $( '.row.propertiesRow' ).append( data );
            if ( current_page === Number( max_pages ) ) {
              $( '.viewAll' ).
                remove();
            }
          }
          else {
            $( '.viewAll' ).remove();
          }
        },
        error: function () {
          console.log( 'Ajax error !!' );
          img.removeClass( 'animation' );
        },
      } );

    } );
    //END ajax load post object home

    //Start deep menu hover
    $( '.menu-item-has-children' ).hover( function () {
      $( this ).toggleClass( 'active' );
      $( this ).children( '.sub-menu' ).toggleClass( 'active' );
    } );//End deep menu hover

    $( '.mobileMenu .menu-item-has-children' ).
      on( 'click touchstart', function ( e ) {
        e.preventDefault();
        $( this ).children( '.sub-menu' ).toggleClass( 'active' );
      } );

    //Start animation for form label
    $( '.form-control' ).on( 'blur', function () {
      if ( !$( this ).val() ) {
        $( this ).parent().children( 'label' ).removeClass( 'filled' );
      }
    } );

    $( '.form-control' ).on( 'focus', function () {
      $( this ).
        parent().
        children( 'label' ).
        removeClass( 'filled' ).
        addClass( 'filled' );
    } );
    //End animation for form label

    //Start archive object map
    $( '.showmap' ).on( 'click', function () {
      var
        object = $( '.estate-object .propertiesRow' ).parent(),
        block_map = $( '.estate-object .mapControls' ).parent(),
        showmap = $( '.showmap' );

      // this_text.text() === 'SHOW MAP'
      //   ? this_text.addClass( 'active' )
      //   : this_text.text( 'SHOW MAP' );

      object.toggleClass( 'col-md-12 col-md-8' );
      block_map.toggleClass( 'd-none' );
      showmap.toggleClass( 'active' );

    } );
    //End archive object map

    //Start video background home
    var playerEl = document.getElementById( 'home_video' );
    if ( playerEl ) {
      function vidRescale () {
        var w = window.innerWidth,
          //h = window.innerHeight;
          h = document.querySelector( '.banner' ).clientHeight;

        if ( w / h > 16 / 9 ) {
          playerEl.style.width = w + 'px';
          playerEl.style.height = (w / 16 * 9) + 'px';
          //playerEl.style.left = '0px';
          //banner.style.height = (w / 16 * 9) + 'px';
        }
        else {
          playerEl.style.width = (h / 9 * 16) + 'px';
          playerEl.style.height = h + 'px';
          playerEl.style.left = (-(playerEl.outerWidth - w) / 2) + 'px';
        }

        playerEl.style.display = 'block';

      }

      function playVideo () {
        playerEl.play();

        vidRescale();
      }

      window.addEventListener( 'load', playVideo );
      window.addEventListener( 'resize', vidRescale );
    }
    //End video background home
    /*
    if($('#loc_select').length){
    $( '#loc_select' ).select2( {
        placeholder: 'Location',
      },
    ); //Initialization select2.js
    }*/

    //Start ajax home "Featured Projects"
    $( '#object-ajax-home a' ).on( 'click touchstart', function () {

      $( '#object-ajax-home a' ).removeClass( 'active' );
      $( this ).addClass( 'active' );

      let term_id = $( this ).data( 'ajax-id' ),
        data_site = {
          'action': 'object_ajax_home',
          'term_id': term_id,
        };

      $.ajax( {
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function ( data ) {
          $( '.propertiesRow .col-md-4' ).remove();
          $( '.propertiesRow' ).append( data );
        },
        error: function () {
          console.log( 'Ajax error !!' );
        },
      } );
    } );
    //End ajax home "Featured Projects"

    //Start ajax object sort
    $( '.pageInputCont .pageSelectSort option' ).
      on( 'click touchstart', function () {

        Cookies.set( 'sorting', $( this ).val(), { expires: 0.001, path: '' } );//Доробити

        if ( typeof community != 'undefined' || typeof propertyType !=
          'undefined' || typeof minBedroom != 'undefined' ||
          typeof maxBedroom != 'undefined' || typeof minPrice != 'undefined' ||
          typeof maxPrice != 'undefined' ) {
          var data_site = {
            'action': 'ajax_object_sort',
            'orderby': $( this ).val(),
            'community': community,
            'propertyType': propertyType,
            'minBedroom': minBedroom,
            'maxBedroom': maxBedroom,
            'minPrice': minPrice,
            'maxPrice': maxPrice,
            'paged': paged,
          };

          $.ajax( {
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: data_site,
            success: function ( data ) {
              $( '#object-post .col-md-6' ).remove();
              $( '#object-post' ).append( data );
            },
            error: function () {
              console.log( 'Ajax error !!' );
            },
          } );
        }

      } );
    //End ajax object sort

    if ( Cookies.get( 'sorting' ) ) {
      var sort = Cookies.get( 'sorting' ),
        option = $( 'select.pageSelectSort option' );

      option.map( function ( indx, element ) {
        //var _this = element.attributes.value.nodeValue;
        var _this = $( element );

        _this.prop( { 'selected': false } );

        if ( _this.val() === sort ) {
          _this.prop( { 'selected': true } );
        }

      } );
    }

    $( '.contactFtRT3 a[href="jivo_api.open();"]' ).
      on( 'click touchstart', function ( e ) {
        e.preventDefault();
        jivo_api.open();
      } );

    //Start conversation feet for meters
    function areaSelect () {
      var active = Cookies.get( 'convert-ft' ),
        _self = $( '.areaSelect .areaSelect-btn' );

      if ( active ) {
        _self.map( function ( indx, element ) {
          if ( active === $( element ).data( 'convert' ) ) {
            _self.removeClass( 'active' );
            $( element ).addClass( 'active' );
          }
        } );
      }

      _self.on( 'click touchstart', function () {
        var _this = $( this );

        _self.removeClass( 'active' );
        _this.addClass( 'active' );
        Cookies.set( 'convert-ft', _this.data( 'convert' ), { expires: 1 } );
        location.reload();
      } );
    }

    //Langauge swicher
    $('.lang-item.current-lang > a').on('click', function (e) {
      e.preventDefault();
      $('.langSwicher').toggleClass('show');
    });
    //End conversation feet for meters
    areaSelect();
  //CF7 add information for hidden input on Service pages
  if($('input[name="pageTitle"]').length){
    $('input[name="pageTitle"]').val(document.title);
    $('input[name="siteUrl"]').val(document.URL);
  }
    if($('.gallerySlider:not(.grid)').length) {
      $('.gallerySlider:not(.grid)').owlCarousel({
        loop: true,
        items: 1,
        nav: true
      });
    }
    if($('.gallerySlider.grid').length) {
      $('.gallerySlider.grid').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        items: 3,
        responsive:{
          0:{
            items:1
          },
          768:{
            items:2
          },
          1024:{
            items:3
          }
        }
      });
    }
    if($('.propertySlider').length) {
      $('.propertySlider').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        items: 3,
        margin: 20,
        responsive:{
          0:{
            items:1,
            margin: 0
          },
          768:{
            items:2,
            margin: 10
          },
          1024:{
            items:3,
            margin: 20
          }
        }
      });
    }
  //Add CV file form
  $('input[name="cv"]').change(function(){
      var filename = $(this).val().replace(/.*\\/, "");
    $(this).closest('div').find(".file_name").text(filename);
  });
    //ADDED BY GPS TEAM 03/05/2020
    $(document).ready(function(){
		
		if (window.location.href.indexOf("/ar/") > -1) {
			//FOR LISTING SEARCH ENGLISH
			// Need to replace it to Arabic Label
			if(jQuery(".sf-field-post-meta-price .sf-range-min").length){
				jQuery(".sf-field-post-meta-price .sf-range-min option:first-child").before('<option value="0"  selected>السعر الأدنى (درهم)</option>');
				jQuery(".sf-field-post-meta-price .sf-range-max option:first-child").before('<option value="50000000" selected>السعر الأقصى (درهم)</option>');
			} 
		}else{
			
			//FOR LISTING SEARCH ENGLISH
			if(jQuery(".sf-field-post-meta-price .sf-range-min").length){
				jQuery(".sf-field-post-meta-price .sf-range-min option:first-child").before('<option value="0"  selected>Min. Price</option>');
				jQuery(".sf-field-post-meta-price .sf-range-max option:first-child").before('<option value="50000000" selected>Max. Price</option>');
			} 
			
			//FOR LISTING SEARCH ENGLISH
			if(jQuery(".sf-field-post-meta-unit_builtup_area .sf-range-min").length){
				jQuery(".sf-field-post-meta-unit_builtup_area .sf-range-min option:first-child").before('<option value="400" selected> Min Area</option>');
				jQuery(".sf-field-post-meta-unit_builtup_area .sf-range-max option:first-child").before('<option value="32400"selected> Max Area</option>');
			} 
			
			//FOR LISTING SEARCH ENGLISH
			if(jQuery(".sf-field-post-meta-bedrooms .sf-range-min").length){
				jQuery(".sf-field-post-meta-bedrooms .sf-range-min option[value='0']").html('Studio');
				jQuery(".sf-field-post-meta-bedrooms .sf-range-min option:first-child").before('<option value="0"  selected>Min. Bedroom</option>');
				jQuery(".sf-field-post-meta-bedrooms .sf-range-max option:first-child").before('<option value="7" selected>Max. Bedroom</option>');
				jQuery(".sf-field-post-meta-bedrooms .sf-range-max option[value='0']").html('Studio');
			} 
		}

      /* For Template 2 Only */

      $(".y-proj-det-btn").click(function(){
        $(".y-tab-btn").removeClass("active");
        $(this).addClass("active")
        $(".y-agent-det-tbl").hide();
        $(".y-proj-det-tbl").fadeIn();
      });


      $(".y-proj-agn-btn").click(function(){
        $(".y-tab-btn").removeClass("active");
        $(this).addClass("active");
        $(".y-proj-det-tbl").hide();
        $(".y-agent-det-tbl").fadeIn();
      });

      $(".y-inquire-btn").click(function(){
        $(".y-inquiry-form-wrap .unit-contact-form .wpcf7-submit").click();
        return false;

      });
      /**/

      //Inquiry Modal Popup Trigger
      $(".x-inquire").click(function(){
        $(".inquiry-overlay, .inquiry-wrap").fadeIn();
      });


      $(".inquiry-overlay").click(function(){
        $(".inquiry-overlay, .inquiry-wrap").fadeOut();
      });
		
	  if($('.outside-select').length){
	    jQuery(function(){
        	jQuery('.outside-select').change(function(){ // when one changes
        		jQuery('.sf-field-sort_order select').val( jQuery('.outside-select').val() ) // they all change
        		jQuery('.sf-field-submit input[type="submit"]').click();
        	})
        });
	  }
		
		// Ajax Search Pro
		if ($("#ajaxsearchpro5_1").length){

			jQuery("input[aria-label='Search input 1']").bind('input', function () {
				var stt = jQuery(this).val();
				jQuery("#ajaxsearchpro5_1 input[aria-label='Search input 5']").val(stt);
				jQuery("#ajaxsearchpro5_1 .innericon").click();
			});

		}
		
		$(document).ajaxComplete(function() {
			
			
			if (window.location.href.indexOf("/ar/") > -1) {
				//FOR LISTING SEARCH ENGLISH
				// Need to replace it to Arabic Label
				if(jQuery(".sf-field-post-meta-price .sf-range-min").length){
					jQuery(".sf-field-post-meta-price .sf-range-min option:first-child").before('<option value="0"  selected>السعر الأدنى (درهم)</option>');
					jQuery(".sf-field-post-meta-price .sf-range-max option:first-child").before('<option value="50000000" >السعر الأقصى (درهم)</option>');
				} 
			}else{

				//FOR LISTING SEARCH ENGLISH
				if(jQuery(".sf-field-post-meta-price .sf-range-min").length){
					jQuery(".sf-field-post-meta-price .sf-range-min option:first-child").before('<option value="0"  >Min. Price</option>');
					jQuery(".sf-field-post-meta-price .sf-range-max option:first-child").before('<option value="50000000" >Max. Price</option>');
				} 

				//FOR LISTING SEARCH ENGLISH
				if(jQuery(".sf-field-post-meta-unit_builtup_area .sf-range-min").length){
					jQuery(".sf-field-post-meta-unit_builtup_area .sf-range-min option:first-child").before('<option value="400" > Min Area</option>');
					jQuery(".sf-field-post-meta-unit_builtup_area .sf-range-max option:first-child").before('<option value="32400"> Max Area</option>');
				} 

				//FOR LISTING SEARCH ENGLISH
				if(jQuery(".sf-field-post-meta-bedrooms .sf-range-min").length){
					jQuery(".sf-field-post-meta-bedrooms .sf-range-min option[value='0']").html('Studio');
					jQuery(".sf-field-post-meta-bedrooms .sf-range-min option:first-child").before('<option value="0"  >Min. Bedroom</option>');
					jQuery(".sf-field-post-meta-bedrooms .sf-range-max option:first-child").before('<option value="7" >Max. Bedroom</option>');
				} 
			}

		});
		
	  //OFFPLAN-PAGE SCRIPTS
	  if($('.offplan-floorplan-tabs').length){
	       $(".offplan-floorplan-tabs > ul > li").click(function(){
	           
	           $(".offplan-floorplan-tabs > ul > li").removeClass("active");
	           $(this).addClass("active")
	           $(".offplan-floorplan-entry").removeClass("active");
	           var triggerdID = $(this).attr("trigger"); 
	           $("#" + triggerdID).addClass("active");
	       });
	  }

      //we have errors on pages where there is no carousel, added verification
      if($('.owl-carousel').length){



        if (window.location.href.indexOf("/ar/") > -1) {

          //Template 2 Carousel
          $('.y-slider .owl-carousel').owlCarousel({
            rtl:true,
            loop:true,
            responsiveClass:true,
            responsive:{
              0:{
                items:1,
                nav:true
              },
              993:{
                items:3,
                nav:false
              }
            }
          });

          $('.x-slider .owl-carousel').owlCarousel({
            rtl:true,
            loop:true,
            responsive:{
              0:{
                items:1
              }
            }
          });

          $('.x-unit-gallery .owl-carousel').owlCarousel({
            center:true,
            rtl:true,
            loop:true,
            margin:10,
            responsive:{
              0:{
                items:1
              },
              1000:{
                items:3
              }

            }
          });
        }else{

          //Template 2 Carousel
          $('.y-slider .owl-carousel').owlCarousel({
            loop:true,
            responsiveClass:true,
            responsive:{
              0:{
                items:1,
                nav:true
              },
              993:{
                items:3,
                nav:false
              }
            }
          });

          $('.x-slider .owl-carousel').owlCarousel({
            loop:true,
            responsive:{
              0:{
                items:1
              }
            }
          });

          $('.x-unit-gallery .owl-carousel').owlCarousel({
              center:true,
            loop:true,
            margin:10,
            responsive:{
              0:{
                items:1
              },
              1000:{
                items:3
              }

            }
          });
        }
      }/*end if*/
    });
  });
})( jQuery );


jQuery(window).load(function(){
	
	function searchAddOns(){
	/*	if(jQuery('fieldset.asp_filter_cf_action').length){

		   if (window.location.href.indexOf("rent_properties")){
				jQuery("fieldset.asp_filter_cf_action select.asp_noselect2").val("Rent");
				jQuery(".asp_search_btn").click();
			}
			

			if(window.location.href.indexOf("buy_properties")){
				jQuery("fieldset.asp_filter_cf_action select.asp_noselect2").val("Buy");
				jQuery(".prp-search-btn").click();
			}
		}*/

        if (window.location.href.indexOf("/ar/") > -1){
            var item = jQuery('<span class="prp-search-btn">إبحث!</span>');
        }else{
            var item = jQuery('<span class="prp-search-btn">Find</span>');
        }
		
		item.click(function() { jQuery(".asp_search_btn").click(); }); 
		jQuery('div.property-search-filters fieldset.asp_filter_tax.asp_filter_tax_property_search.asp_multisearch_filter_box.asp_filter_id_1.asp_filter_n_0 .asp_select2').delay(20000).append(item);
	}
	
	function togglePaymentTerm(){
	    jQuery(function(){
			
			if (window.location.href.indexOf("/ar/") > -1){
				
				jQuery('select.asp_nochosen.asp_noselect2[aria-label="للإيجار"]').change(function(){ // when one changes
					if(jQuery('select.asp_nochosen.asp_noselect2[aria-label="للإيجار"]').val() == "للبيع"){
							jQuery("fieldset.asp_custom_f.asp_filter_cf_payment_term.asp_filter_id_3.asp_filter_n_2").hide();
						jQuery('select.asp_nochosen.asp_noselect2[name="aspf[payment_term__3]"]').val("");
						jQuery("div.property-search-filters fieldset.asp_filter_tax.asp_filter_tax_property_search.asp_multisearch_filter_box.asp_filter_id_1.asp_filter_n_0").addClass("search-expanded");
					}

					else{
						jQuery("div.property-search-filters fieldset.asp_filter_tax.asp_filter_tax_property_search.asp_multisearch_filter_box.asp_filter_id_1.asp_filter_n_0").removeClass("search-expanded");
							jQuery("fieldset.asp_custom_f.asp_filter_cf_payment_term.asp_filter_id_3.asp_filter_n_2").show();
						jQuery('select.asp_nochosen.asp_noselect2[name="aspf[payment_term__3]"]').val("سنوي");

					}
				});
				
            
			}else{
				jQuery('select.asp_nochosen.asp_noselect2[aria-label="Rent"]').change(function(){ // when one changes
					if(jQuery('select.asp_nochosen.asp_noselect2[aria-label="Rent"]').val() == "Buy"){
							jQuery("fieldset.asp_custom_f.asp_filter_cf_payment_term.asp_filter_id_3.asp_filter_n_2").hide();
						jQuery('select.asp_nochosen.asp_noselect2[name="aspf[payment_term__3]"]').val("");
						jQuery("div.property-search-filters fieldset.asp_filter_tax.asp_filter_tax_property_search.asp_multisearch_filter_box.asp_filter_id_1.asp_filter_n_0").addClass("search-expanded");
					}

					else{
						jQuery("div.property-search-filters fieldset.asp_filter_tax.asp_filter_tax_property_search.asp_multisearch_filter_box.asp_filter_id_1.asp_filter_n_0").removeClass("search-expanded");
							jQuery("fieldset.asp_custom_f.asp_filter_cf_payment_term.asp_filter_id_3.asp_filter_n_2").show();
						jQuery('select.asp_nochosen.asp_noselect2[name="aspf[payment_term__3]"]').val("Yearly");

					}
				});
			}
			
        	
        }); // end of jquery function
	} //end of function
	
	setTimeout(function(){ 
		searchAddOns();
		togglePaymentTerm()
	}, 1000);
});