<?php
get_header();
?>

    <div class="clearfix"></div>
    <div class="featureProjects property-archive">

    	<main class="property-catalog-wrap">
    		<section class="property-search-section">
    		    <article>
        			<h1 class="property-section-title">
        			    <?php echo pll__('Search for Luxury Properties in Dubai');?>
        			</h1>
        			
        			<div class="property-search-filters">
        			    
        			<?php
        			    //echo do_shortcode("[wd_asp id=5]");
        			    //echo do_shortcode('[wpdreams_ajaxsearchpro_results id=5 element="div"]');
        			    echo do_shortcode("[wd_asp elements='search,settings' ratio='100%,100%' id=6]");
        			    
        			    
        			?>
        		    </div>
        			
    			</article>
    		</section>
    		
    		<!--<ul class="toHighlight">
    		    <li>This Property to Highlight</li>
    		    <li>This to Highlight Property</li>
    		</ul>-->
    		
    		<section class="property-listing-catalog-section">
    		   <div class="property-container-section">
    		       
    		       
        			    
    		        <?php echo do_shortcode("[wd_asp elements='results' ratio='100%' id=6]") ;?>
    		   </div>
    		</section>

    	</main>
    </div>


<?php
get_footer();