<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framework
 */

//if ( (! is_front_page() ) or is_post_type_archive('agents') or is_page_template( 'template-contact.php' ) or in_category('54') or is_post_type_archive('estate_object') or is_singular()  ){
//    $pageHeader = 'pageHeader';
//}
$pageHeader = 'pageHeader';
$logoUrl = get_template_directory_uri()."/assets/images/mpd-black-logo.svg";
$templates = array(
    'template-about.php'
);
if ( is_page_template($templates) || is_front_page() || is_category()){
	$pageHeader = null;
    $logoUrl = get_template_directory_uri()."/assets/images/mpd-white-logo.svg";
}
$GLOBALS["headBtn"] = get_field('btn_in_header_url', 'option');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
    get_template_part( '/template-parts/meta' );
	wp_head();
	?>
    <meta name="google-site-verification" content="CBC-us1N8kJVIus7bf2jzDK78UM4thL5uDmXg2apSuo" />
    <meta name="yandex-verification" content="dd27bb89334475e9" />
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#00ade5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159948526-17"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-159948526-17');
    </script>
</head>

<body <?php body_class($pageHeader); ?>>
	<header id="stickyMenu" class="<?php echo $pageHeader ;?>">

        <div class="container-fluid">
            <div class="row">
                <div class="col-menu col-12 col-sm-6 col-md-12 mx-auto d-flex align-items-center flex-wrap">

	                <?php if (is_front_page()) : ?>
                        <div class="logoContainer">
                            <img src="<?=$logoUrl?>" alt="<?php echo get_bloginfo( "name" ); ?>">
                        </div>
	                <?php else :?>
                        <a class="logoContainer" href="<?php echo esc_url( home_url( '/' ) ) ?>">
                            <img src="<?=$logoUrl?>"  alt="<?php echo get_bloginfo( "name" ); ?>">
                        </a>
	                <?php endif; ?>
                    <div class="mobile-elastic"></div>
	                <?php get_template_part( '/template-parts/menu', 'main' );?>

	                <?php if ($GLOBALS["headBtn"]):?>
                        <div class="btnContainer">
                            <a href="<?=$GLOBALS["headBtn"]?>" class="sellPropertyBtn"><?php echo pll__('SELL YOUR PROPERTY');?></a>
                        </div>
	                <?php endif; ?>

                    <a href="javascript:void(0)" class="menuIcon menuOpen"><span class="menuOpen_line"></span><span class="menuOpen_line"></span><span class="menuOpen_line"></span></a>
                </div>
            </div>
        </div>

    </header>
