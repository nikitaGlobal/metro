<section class="offplan-related-content-main-wrap offplan-related">
    <div class="offplan-related-content-sub-wrap">
        <h3>Related Properties</h3>
    </div>
    <?php
        $locationID      = get_post_meta(get_the_id(), 'associated_location');
        $currentProperty = get_the_id();
        $args            = array(
            'post_type'      => 'properties',
            'posts_per_page' => 999,
            'post__not_in'   => array($currentProperty),
            'meta_query'     => array(
                array(
                    'key'     => 'associated_location',
                    'value'   => $locationID,
                    'compare' => 'IN',
                    //'type'    => 'NUMERIC'
                )
            )
        );
        $propertiesQuery = new WP_QUERY($args);
        if ($propertiesQuery->have_posts()) { ?>
            <div class="featureProjects">
            <div class="container" style="position: relative">
                <!--<div class="projectHeading left">
                    <h2>Some title</h2>
                    <div class="middleSlash"></div>
                </div>-->
                <div class="propertySlider owl-carousel">
                    <?php
                        while ($propertiesQuery->have_posts()) {
                            $propertiesQuery->the_post();
                            get_template_part('preview-related-property');
                        } ?> </div>
                <div class="btnWrp"><a href="<?= get_term_link('buy_properties',
                        'plan_types') ?>"
                                       class="showAllPrp"><?= get_sub_field('btn') ?></a>
                </div>
            </div>
            </div><?php
        }
        wp_reset_postdata();
        wp_reset_query();
    
    ?>
</section>