<?php
/**
 * The template for displaying all single posts
 *
 *
 * @package framework
 */
/*
Template Name: Listing Template HTML
Template Post Type: properties
*/

get_header();
?>

<section class="x-main-wrap featureProjects">
    
    <!-- INQUIRY MODAL -->
    <section class="inquiry-overlay"></section>
    <section class="inquiry-wrap">
        <span class="inquiry-modal-heading">Send us an Inquiry</span>
        <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?>
    </section>
    <!-- END OF INQUIRY MODAL-->

    <div class="x-banner-section">
        <div class="x-slider">

            <div class="owl-carousel">
              <?php echo do_shortcode( '[acf field="hero_banner_images"]' ); ?>
            </div>

        </div>
        <div class="x-unit-intro">
            <div class="x-unit-intro-content">
                <h1 class="x-unit-title"><?php echo do_shortcode( '[acf field="prop_title"]' ); ?></h1>
            </div>
        </div>
    </div>

    <div class="x-init-info-container">
        <div class="x-init-info">
            <div>
                <div class="x-init-info-entry x-init-property-type">
                    <span class="x-init-info-label">Property Type</span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="property_type"]' ); ?></span>
                </div>

                <div class="x-init-info-entry x-init-community">
                    <span class="x-init-info-label">Community</span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="community"]' ); ?></span>
                </div>
            </div>

            <div>
                <div class="x-init-info-entry x-init-area">
                    <span class="x-init-info-label">Area</span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="unit_builtup_area"]' ); ?> Sq.ft</span>
                </div>

                <div class="x-init-info-entry x-init-bedroom">
                    <span class="x-init-info-label">Bedroom</span>
                    <span class="x-init-info-content"><?php $iopo =  do_shortcode( '[acf field="bedrooms"]'); echo str_replace("ST","Studio", $iopo);?></span>
                </div>
            </div>

            <div>
                <div class="x-init-info-entry x-init-bathroom">
                    <span class="x-init-info-label">Bathroom</span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="no_of_bathroom"]' ); ?></span>
                </div>

                <div class="x-init-info-entry x-init-primary-view">
                    <span class="x-init-info-label">Property Name</span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="property_name"]' ); ?></span>
                </div>
            </div>

            
            <div>
                <span class="x-unit-price"><?php echo do_shortcode( '[acf field="price"]')  ; ?></span>
                <button class="x-inquire">INQUIRE NOW</button>
            </div>
        </div>
    </div>

    <div class="x-unit-details">
        <div class="x-desc">
        
            <h2 class="x-property-name"><?php echo do_shortcode( '[acf field="property_name"]' ); ?></h2>
            <p>
               <?php echo do_shortcode( '[acf field="property_writeup"]' ); ?>
            </p>
        </div>

        <div class="x-unit-gallery">

            <div class="owl-carousel">
              <?php echo do_shortcode( '[acf field="unit_gallery"]' ); ?>
            </div>
            
        </div>
    </div>

    <div class="x-indepth-details">
        <ul class="tab-options">

            <li>
                <a class="x-indepth-label" href="#">Project Details</a>
                <div class="x-unit-details-content">
                    <table class="x-unit-tbl-info">
                        <tbody>
                            <tr>
                                <td>Unit Reference</td>
                                <td><?php echo do_shortcode( '[acf field="unit_reference"]' ); ?></td>
                                <td>Parking Slots</td>
                                <td><?php echo do_shortcode( '[acf field="parking"]' ); ?></td>
                            </tr>
                            <tr>
                                <td>Property Name</td>
                                <td><?php echo do_shortcode( '[acf field="property_name"]' ); ?></td>
                                <td>Permit Number</td>
                                <td><?php echo do_shortcode( '[acf field="permit_number"]' ); ?></td>
                            </tr>
                            <tr>
                                <td>Emirate</td>
                                <td><?php echo do_shortcode( '[acf field="emirate"]' ); ?></td>
                                <td>Completion Status</td>
                                <td><?php echo do_shortcode( '[acf field="completion_status"]' ); ?></td>
                            </tr>
                            <tr>
                                <td>Latitude</td>
                                <td><?php echo do_shortcode( '[acf field="lat"]' ); ?></td>
                                <td>Longitude</td>
                                <td><?php echo do_shortcode( '[acf field="lng"]' ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </li>


            <li>
                <a class="x-indepth-label" href="#">Agent Information</a>
                <div class="x-unit-details-content">
                    <table class="x-unit-tbl-info">
                        <tbody>
                            <tr>
                                <td>Company Name</td>
                                <td><?php echo do_shortcode( '[acf field="company_name"]' ); ?></td>
                                <td>Listing Agent</td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent"]' ); ?></td>
                            </tr>
                            <tr>
                                <td>Listing Agent Email</td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email"]' ); ?></td>
                                <td>Listing Agent Phone</td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone"]' ); ?></td>
                            </tr>
                            <tr>
                                <td>Last Updated</td>
                                <td><?php echo do_shortcode( '[acf field="last_updated"]' ); ?></td>
                                <td>Listing Date</td>
                                <td><?php echo do_shortcode( '[acf field="listing_date"]' ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </li>
        </ul>
    </div>


    <div class="x-unit-facilities">
        <div class="x-unit-facilities-content">
            <h2>Features and Ameneties</h2>
            <span class="x-unit-facilities-subtitle">Everything you Need</span>

            <ul class="x-unit-facilities-listing">
                <?php echo do_shortcode( '[acf field="x-facilities"]' ); ?>
            </ul>
        </div>
    </div>

    <div class="x-location-landmark">
        <div class="x-location-gmaps">
            
             <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="650" id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo do_shortcode( '[acf field="lat"]' ); ?>,<?php echo do_shortcode( '[acf field="lng"]' ); ?>&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:650px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:650px;width:100%;}</style></div>
            
        </div>
        <div class="x-location-landmarks">
            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>
        </div>
    </div>

    <div class="x-contact-section">
        <div class="x-contact-section-content">
            <div class="x-contact-section-title">
                <span class="x-contact-section-subtitle">Ready for your new home?</span>
                <h2 class="x-contact-section-heading">Send us an Inquiry</h2>
            </div>

            <div class="x-contact-container">
                <div class="x-contact-basic">
                    <span class="x-contact-pre">[ CONTACT US ]</span>
                    <h3>Let's Get in Touch</h3>

                    <ul>
                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/envelope.svg"/>
                            </span>
                            <span class="x-contact-entry-label">Email</span>
                            <span class="x-contact-entry-listing">info@metropolitan.com</span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/smartphone.svg"/>
                            </span>
                            <span class="x-contact-entry-label">Call Us</span>
                            <span class="x-contact-entry-listing">+971 58 592 4856</span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/placeholder.svg"/>
                            </span>
                            <span class="x-contact-entry-label">Address</span>
                            <span class="x-contact-entry-label">92 Street Ibn Battuta, Gardens, Dubai</span>
                        </li>
                    </ul>
                </div>

                <div class="x-contact-form-container">
                    <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?>
                </div>
            </div>
        </div>
    </div>

</section>






<?php get_footer();
