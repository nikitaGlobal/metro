<?php /* Template Name: Join Family */
get_header();
?>

    <div class="bannerPM joinBanner">
        <div class="bannerText">
            <div class="container">
                <?php $header = get_field( 'header' ); ?>
                <?php if ($header['title']) : ?>
                    <h1 class="bannerHeading"><?=$header['title']?></h1>
                <?php endif; ?>
				<?php if ($header['desc']) : ?>
                   <p class="bannerSubHeading"><?=$header['desc']?></p>
				<?php endif; ?>
                <?php if ($header['btn']) : ?>
                <div class="valBtnCont" id="topSearch">
                    <button type="button" data-toggle="modal" data-target="#join" class="bannerValBtn"><?=$header['btn']?></button>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php if ( have_rows( 'flex_content' ) ) : ?>
	<?php while ( have_rows( 'flex_content' ) ) : the_row(); ?>

		<?php if (get_row_layout() === 'bIcons') : ?>
            <div class="featureProjects pmFeature">
                <div class="container">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedL" style="width: 130px;">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <?php if(get_sub_field( 'desc' )): ?>
                    <div class="pmBanB <?=get_sub_field( 'design' ) ?>">
                        <p><?php echo get_sub_field( 'desc' ) ?></p>
                    </div>
                    <?php endif; ?>
					<?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                        <div class="row advRow advRowpm">

							<?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="col-md-3">
                                    <div class="advTxt">
                                        <div class="advImg">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg">
                                            <div class="d-none"><?php echo $subarr['header'] ?></div>
                                        </div>
										<?php
										$temp_str = $subarr['header'];
										$spc_pos  = stripos( $temp_str, " " );
										if ( $spc_pos ) : ?>
                                            <h4 class="advHeadpm">
                                                <span><?php echo substr( $temp_str, 0, $spc_pos ) ?></span><?php echo substr( $temp_str, $spc_pos ) ?>
                                            </h4>
										<?php else : ?>
                                            <h4 class="advHeadpm"><?php echo $temp_str ?></h4>
										<?php endif; ?>
                                        <p class="advDesc"><?php echo $subarr['subheader'] ?></p>
                                    </div>
                                </div>
							<?php endforeach; ?>

                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

        <?php if ( get_row_layout() === 'text' ) : ?>
            <div class="featureProjects textJoin">
                <div class="container">
                    <div class="projectHeading left">
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="row abtRow">
                        <div class="col-md-6">
                            <div class="textBlock joinTextBlock noCenter">
                                <?php echo get_sub_field( 'content' ) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="joinVideo">
                                <?php $video = parse_url(get_sub_field('video')); ?>
                                <iframe width="100%" height="321" src="https://www.youtube.com/embed/<?=$video['path']?>?rel=0&modestbranding=1&showinfo=0" frameborder="0" allow="accelerometer; encrypted-media; gyroscope;" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php endif; ?>

        <?php if ( get_row_layout() === 'qa' ) : ?>
            <div class="featureProjects qaBlock">
                <div class="container">
                <?php if ( ! empty( get_sub_field( 'line' ) ) ) : ?>
                    <div class="row abtRow">
                    <?php foreach ( get_sub_field( 'line' ) as $subarr ) : ?>
                        <div class="col-md-6">
                            <div class="textBlock noCenter">
                                <p class="question"><?=$subarr['question']?></p>
                                <p class="answer"><?=$subarr['answer']?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="textBlock noCenter pd15">
                        <?=get_sub_field( 'text' )?>
                    </div>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ( get_row_layout() === 'awards' ) : ?>
        <div class="featureProjects fPawards">
            <div class="container">
                <div class="projectHeading left">
                    <h2><?php echo get_sub_field( 'header' ) ?></h2>
                    <div class="middleSlash"></div>
                </div>
                <div class="row awardRow">
                    <?php $originID = get_sub_field('origin')['0']; ?>
                    <?php if ( have_rows( 'flex_content', $originID ) ) :
                        while ( have_rows( 'flex_content',$originID ) ) : the_row();
                            if ( get_row_layout() === 'awards'):
                                if ( ! empty( get_sub_field( 'cards' ) ) ) :
                                    foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="col-md-3">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/awardribben.svg">
                                    <div class="awardText">
                                        <div class="awardLogo">
                                            <img src="<?php echo $subarr['award_logo']['url'] ?>" width="75" height="48">
                                        </div>
                                        <div class="awardImg">
                                            <img src="<?php echo $subarr['trophy_image']['url'] ?>" width="110" height="111">
                                        </div>
                                    </div>
                                    <div class="awardTextBottom">
                                        <p class="awardTextP"><?php echo $subarr['title'] ?></p>
                                    </div>
                                </div>
                                <?php endforeach;
                                endif;
                            endif;
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if ( get_row_layout() === 'vacancies' && get_sub_field( 'show_block' )) : ?>
            <?php
            $args = [
                'post_type' => 'vacancies',
                'posts_per_page' => -1
            ];
            $posts = new WP_Query( $args );
            if($posts->have_posts()):
            ?>
            <div class="featureProjects fPvacancies noPd">
                <div class="container">
                    <div class="projectHeading left headVacancies">
                        <h3><?php echo get_sub_field( 'header' ) ?><div class="middleSlash"></div></h3>
                    </div>
                    <div class="row vacancies">
                        <?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
                        <div class="col-md-4">
                            <a href="<?=get_post_permalink()?>" class="linkVacancy">
                                <p class="titleVacancy"><?=get_the_title()?></p>
                                <p class="locateOffice"><?=get_field('locate')?></p>
                                <p class="postedDate"><?php echo 'Posted '.esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' ago'; ?></p>
                            </a>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <?php
            endif;
            wp_reset_postdata();
        endif; ?>

        <?php if ( get_row_layout() === 'slider' ) : ?>
            <div class="featureProjects sliderFW">
                <div class="projectHeading">
                    <div class="middleSlash"></div>
                    <h2><?php echo get_sub_field( 'header' ) ?></h2>
                    <div class="middleSlash"></div>
                </div>
                <div class="gallerySlider owl-carousel">
                <?php
                $gallery = get_sub_field( 'gallery' );
                foreach ($gallery as $item): ?>
                    <div class="gallerySlider__item">
                        <img src="<?=$item['url']?>">
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

	<?php endwhile; ?>
<?php endif; ?>

    <div class="pmFooter joinForm">
        <div class="pmfooterDot">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>
        <div class="pmfooterDotL">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>

        <div class="pmFooterInner">
            <div class="projectHeading">
                <div class="middleSlash"></div>
                <h2><?php echo get_field('footer_header') ?></h2>
                <div class="middleSlash"></div>
            </div>
            <div class="footerForm">
	            <?php echo do_shortcode(get_field('form_shortcode')); ?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="join" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modalH"><?php echo get_field( 'footer_header' ) ?></p>
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">
                        <?php echo do_shortcode(get_field('form_shortcode')); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php get_footer();