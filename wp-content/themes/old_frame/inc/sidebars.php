<?php
/**
 * Register widgetized areas and widgets
 *
 * @package framework
 */
function framework_widgets_init() {

	register_sidebar( [
		'name'          => __( 'Footer 1', 'framework' ),
		'description'   => __( 'The footer widget with logo!', 'framework' ),
		'id'            => 'footer-1',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	] );

	register_sidebar( [
		'name'          => __( 'Footer 2', 'framework' ),
		'description'   => __( 'The footer widget area displayed after all content.', 'framework' ),
		'id'            => 'footer-2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p class="FooterMH">',
		'after_title'   => '</p>',
	] );

	register_sidebar( [
		'name'          => __( 'Footer 3', 'framework' ),
		'description'   => __( 'The second footer widget area, displayed below the Footer widget area.', 'framework' ),
		'id'            => 'footer-3',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p class="FooterMH">',
		'after_title'   => '</p>',
	] );

	register_sidebar( [
		'name'          => __( 'Footer 4', 'framework' ),
		'description'   => __( 'The second footer widget area, displayed below the Footer widget area.', 'framework' ),
		'id'            => 'footer-4',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p class="FooterMH">',
		'after_title'   => '</p>',
	] );

	register_sidebar( [
		'name'          => __( 'Footer 5', 'framework' ),
		'description'   => __( 'The second footer widget area, displayed below the Footer widget area.', 'framework' ),
		'id'            => 'footer-5',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	] );
}

add_action( 'widgets_init', 'framework_widgets_init' );
