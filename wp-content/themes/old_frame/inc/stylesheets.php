<?php
/**
 * Register and enqueue the CSS
 */
function fireworks_styles() {
	if ( is_singular()){
		wp_enqueue_style( 'rating', get_template_directory_uri() . '/assets/ratings/rating.css', array() );
	}

	if ( is_front_page() || is_tax() ){
		wp_enqueue_style( 'select2', get_template_directory_uri() . '/assets/css/select2.min.css', array() );
	}

	wp_enqueue_style( 'fireworks-style', get_stylesheet_uri() );
		wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css', array() );
	wp_enqueue_style( 'fontello', get_template_directory_uri() . '/assets/css/fontello.css', array() );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css', array() );
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array() );
	wp_enqueue_style( 'style-GPS1', get_template_directory_uri() . '/assets/css/style-GPS.css', array() );
    
	
    if(ICL_LANGUAGE_CODE!='ar') {
		wp_enqueue_style( 'style-site', get_template_directory_uri() . '/assets/css/style.css', array() );
		wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/assets/css/theme-style.css', array() );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array() );
    }
	
    if(ICL_LANGUAGE_CODE=='ar') {
		wp_enqueue_style( 'style-site', get_template_directory_uri() . '/assets/css/arabic/style_ar.css', array() );
		wp_enqueue_style( 'style-GPS', get_template_directory_uri() . '/assets/css/arabic/style-GPS_ar.css', array() );
		wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/assets/css/arabic/theme-style_css_ar.css', array() );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/arabic/bootstrap.mis_ar.css', array() );
    }

}

add_action( 'wp_enqueue_scripts', 'fireworks_styles' );
