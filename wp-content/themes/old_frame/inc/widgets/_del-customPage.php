<?php

class customPage extends WP_Widget {

	/**
	 * Sets up a new Custom page widget instance.
	 *
	 * @since 2.8.0
	 */
	function __construct() {
		// Запускаем родительский класс
		parent::__construct(
			'customPage_widget', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: my_widget
		__('Custom Page'),
			array('description' => 'We display arbitrary pages')
		);
	}


	/**
	 * Outputs the content for the current Custom page widget instance.
	 *
	 * @param array $args     Display arguments including .
	 * @param array $instance Settings for the current Custom categories widget instance.
	 */
	function widget( $args, $instance ){
		$pages = $instance["pages"];
		$str = explode( ',', $pages );

		echo '<ul class="footerMenu">';
        if ( 'en' !== pll_current_language() ){
	        $lang = pll_current_language() . '/';
        } else {
	        $lang = '';
        }

		foreach ( $str as $item ) {

			if ( get_post( $item ) ) {
				$post = get_post( $item );
				echo '<li class="page_item arhive int">';
				echo '<a href="/';
				echo $lang . $post->post_name ;
				echo '">'. $post->post_title;
				echo '</a></li>';
			} else {
				echo '<li class="page_item arhive str"><a href="/' . get_permalink($item) . '">' . ucfirst( $item ) . '</a></li>';
			}
		}

		echo "</ul>";
	}


	/**
	 * Handles updating settings for the current Custom page widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance["pages"] = htmlentities( $new_instance["pages"] );

		return $instance;
	}

	/**
	 * Outputs the settings form for the Categories widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$pages = "";

		// если instance не пустой, достанем значения
		if ( ! empty( $instance ) ) {
			$pages = $instance["pages"];
		}
		?>

		<?php

		$labelId   = $this->get_field_id( "pages" );
		$stringName = $this->get_field_name( "pages" );
		?>
        <p>
            <label for="<?php echo $labelId; ?>">Pages ids</label><br>
            <input id="<?php echo $labelId ?>" type="text" name="<?php echo $stringName; ?>" value="<?php echo $pages; ?>" style="width:100%;">
        <p/>

		<?php
	}

}

// Регистрация класса виджета
add_action( "widgets_init", function() {
	register_widget( "customPage" );
} );