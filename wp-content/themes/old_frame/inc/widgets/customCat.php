<?php

class customCat extends WP_Widget {

	/**
	 * Sets up a new Custom categories widget instance.
	 *
	 * @since 2.8.0
	 */
	function __construct() {
		// Запускаем родительский класс
		parent::__construct(
			'customCat_widget',
			// ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: my_widget
			'Custom Category',
			array( 'description' => __( 'A list of categories.' ) )
		);
	}

	/**
	 * Outputs the content for the current Custom categories widget instance.
	 *
	 * @param array $args Display arguments including .
	 * @param array $instance Settings for the current Custom categories widget instance.
	 */
	function widget( $args, $instance ) {
		$title     = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Categories' );
		$customCat = $instance["customCat"];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$query_emirate = get_terms( array(
			'taxonomy'   => 'emirate',
			'number'     => 5,
			'hide_empty' => true,
			'orderby'    => 'count',
			'order'      => 'DESC',
			'child_of'   => $customCat,
		) );

		if ( null !== $query_emirate ):
			$count = count( $query_emirate );
			echo '<ul class="footerMList">';
			for ( $i = 0; $i < $count; $i ++ ):
				echo '<li><a href="' . get_term_link( $query_emirate[ $i ] ) . '">' . $query_emirate[ $i ]->name . '</a></li>';
			endfor;
			echo '</ul>';
		else:
			echo 'There are no categories of objects';
		endif;

	}

	/**
	 * Handles updating settings for the current Custom categories widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 *
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = sanitize_text_field( $new_instance['title'] );
		$instance["customCat"] = htmlentities( $new_instance["customCat"] );

		return $instance;
	}

	/**
	 * Outputs the settings form for the Categories widget.
	 *
	 * @param array $instance Current settings.
	 */
	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );

//		$query_emirate = get_terms( array(
//			'taxonomy'   => 'emirate',
//			'hide_empty' => true,
//			'parent'     => 0,
//		) );
//
//		if ( null !== $query_emirate ):
//			$count = count( $query_emirate );
//			echo '<select name="sortby">';
//			for ( $i = 0; $i < $count; $i ++ ):
//				echo '<option value = "'. $query_emirate[ $i ]->name .'">' . $query_emirate[ $i ]->name . '</option>';
//			endfor;
//			echo '</select>';
//
//		else:
//			echo 'There are no categories of objects';
//		endif;

		?>

        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
        </p>

        <p><label for="<?php echo $this->get_field_id( 'customCat' ); ?>">Custom categories id</label><br>
            <input id="<?php echo $this->get_field_id( 'customCat' ); ?>" type="text" name="<?php echo $this->get_field_name( 'customCat' ); ?>" value="<?php echo esc_attr( $instance['customCat'] ); ?>" style="width:100%;">
        <p/>

		<?php
	}

}

// Register widget class
add_action( "widgets_init", function() {
	register_widget( "customCat" );
} );