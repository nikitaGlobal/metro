<?php
/**
 * The template for displaying all single posts
 *
 *
 * @package framework
 */
/*
Template Name: Listing Template 2 HTML
Template Post Type: properties, page
*/

get_header();

if (ICL_LANGUAGE_CODE=='ar'){
?>
<!--AR STYLING-->
<style>
    body{
    direction:rtl;
}
.y-price-wrap > div:first-child{
    text-align:right;
}

.y-init-price-label{
    text-align:right;
}

.y-left{
    text-align:right;
}

.y-label-icon{
    left:initial;
    right:0;
}

.y-property-init-det-wrap > div{
    padding-left:0;
    padding-right:60px;
}

.y-property-init-det-wrap > div:before {
    
    right: initial;
    left:25px;
}

.y-det-info-wrap{
    text-align:right;
}

.y-img-container1{
    left:initial;
    right:150px;
}

.y-img-container2{
    left:initial;
    right:-230px;;
}

.y-feats-amen{
    text-align:right;
}

.y-template-wrap .x-unit-facilities-listing li{
    padding-left:0;
    padding-right:45px;
}
.y-feats-amen .x-unit-facilities-subtitle{
    text-align:right;
}

.y-template-wrap .x-unit-facilities-listing li:before{
    left:initial;
    right:0;
}

.y-template-wrap .x-location-landmarks{
    text-align:right;
}

.y-template-wrap .x-contact-container{
 text-align:right;
}

.y-template-wrap .x-contact-basic li {
    position: relative;
    padding-right: 60px;
    padding-left:initial!important;
}

.y-template-wrap .x-contract-entry-icon {
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    width: 40px;
    height: 40px;
    background: #16a2e4;
    padding: 10px;
}


.y-template-wrap .x-contract-entry-icon {
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    width: 40px;
    height: 40px;
    background: #16a2e4;
    padding: 10px;
}

.y-template-wrap .x-contract-entry-icon {
    left:initial;
    right:0;
}


</style>
<!--AR STYLING END -->
<?php
//AR Page End
}
?>

<section class="x-main-wrap featureProjects">
    
    <!-- INQUIRY MODAL -->
    <section class="inquiry-overlay"></section>
    <section class="inquiry-wrap">
        <span class="inquiry-modal-heading"><?php echo do_shortcode( '[acf field="send_us_an_inquiry_label"]' ); ?></span>
        <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?>
    </section>
    <!-- END OF INQUIRY MODAL-->

    
    <main class="y-template-wrap">
        <section class="y-slider-wrap">
            <div class="y-slider">
                <div class="owl-carousel">
                    <?php echo do_shortcode( '[acf field="hero_banner_images"]' ); ?>
                </div>
            </div>
        </section>

        <section class="y-initial-info-wrap">
            <article>
                <div class="y-left">
                    <h1 class="y-property-title"><?php echo do_shortcode( '[acf field="prop_title"]' ); ?></h1>
                    <div class="y-property-init-det-wrap">
                        <div>
                            <img class="y-label-icon" src="/wp-content/themes/framework/assets/icons/property_type.svg"/>
                            <span class="y-init-det-label"><?php echo do_shortcode( '[acf field="property_type_label"]' ); ?></span>
                            <span class="y-init-det-entry"><?php echo do_shortcode( '[acf field="property_type"]' ); ?></span>
                        </div>
                        <div>
                            <img class="y-label-icon" src="/wp-content/themes/framework/assets/icons/area.svg"/>
                            <span class="y-init-det-label"><?php echo do_shortcode( '[acf field="area_label"]' ); ?></span>
                            <span class="y-init-det-entry"><?php echo do_shortcode( '[acf field="unit_builtup_area"]' ); ?> Sq.ft</span>
                        </div>
                        <div>
                            <img class="y-label-icon" src="/wp-content/themes/framework/assets/icons/bathroom.svg"/>
                            <span class="y-init-det-label"><?php echo do_shortcode( '[acf field="bathroom_label"]' ); ?></span>
                            <span class="y-init-det-entry"><?php echo do_shortcode( '[acf field="no_of_bathroom"]' ); ?></span>
                        </div>
                        <div>
                            <img class="y-label-icon" src="/wp-content/themes/framework/assets/icons/bedroom.svg"/>
                            <span class="y-init-det-label"><?php echo do_shortcode( '[acf field="bedroom_label"]' ); ?></span>
                            <span class="y-init-det-entry"><?php echo do_shortcode( '[acf field="bedrooms"]' ); ?></span>
                        </div>
                        <div>
                            <img class="y-label-icon" src="/wp-content/themes/framework/assets/icons/community.svg"/>
                            <span class="y-init-det-label"><?php echo do_shortcode( '[acf field="community_label"]' ); ?></span>
                            <span class="y-init-det-entry"><?php echo do_shortcode( '[acf field="community"]' ); ?></span>
                        </div>
                    </div>
                    <p class="y-property-writeup">
                        <?php echo do_shortcode( '[acf field="property_writeup"]' ); ?>
                    </p>
                    
                    
                <button class="y-inquire x-inquire"><?php echo do_shortcode( '[acf field="inquire_now_label"]' ); ?></button>
                
                </div>
                <div class="y-right">
                    <div class="y-inquiry-form-wrap">
                        <div class="y-price-wrap">
                            <div>
                                <span class="y-init-price-label"><?php echo do_shortcode( '[acf field="total_price_label"]' ); ?></span>
                                <span class="y-init-price"><?php echo number_format(do_shortcode( '[acf field="price"]')) ; ?> AED</span>
                            </div>
                            <div>
                                <button class="y-btn y-inquire-btn"><?php echo do_shortcode( '[acf field="inquire_now_label"]' ); ?></button>
                            </div>
                        </div>

                        <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?>

                    </div>
                </div>
            </article>
        </section>

        <section class="y-det-info-wrap">
            <article class="y-det-container">
                <div>
                    <button class="y-tab-btn y-proj-det-btn active"><?php echo do_shortcode( '[acf field="project_details_label"]' ); ?></button>
                    <button class="y-tab-btn y-proj-agn-btn"><?php echo do_shortcode( '[acf field="agent_information_label"]' ); ?></button>

                    <table class="y-proj-tbl">
                        <tbody class="y-proj-det-tbl">
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="unit_reference_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="unit_reference"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="parking_slots_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="parking"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="property_name_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="property_name"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="permit_number_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="permit_number"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="emirate_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="emirate"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="completition_status_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="completion_status"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="latitude_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="lat"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="longitude_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="lng"]' ); ?></td>
                            </tr>
                        </tbody>

                        <tbody class="y-agent-det-tbl">
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="company_name_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="company_name"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="last_updated_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="last_updated"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_date_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_date"]' ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>

        <section class="y-pictures-wrap">
            <article>
                <table>
                    <tr>
                        <td></td>
                        <td><div class="y-img-container1"><?php echo do_shortcode( '[acf field="showcase_image_1"]' ); ?><div></td>
                    </tr>
                </table>

                <table class="y-pictures-tbl-2">
                    <tr>
                        <td><div class="y-img-container2"><?php echo do_shortcode( '[acf field="showcase_image_2"]' ); ?></td>
                        <td class="y-feats-amen">
                            <h2><?php echo do_shortcode( '[acf field="features_and_amenities_label"]' ); ?></h2>
                            <span class="x-unit-facilities-subtitle"><?php echo do_shortcode( '[acf field="everything_you_need_label"]' ); ?></span>

                            
            <ul class="x-unit-facilities-listing">
                <?php echo do_shortcode( '[acf field="x-facilities"]' ); ?>
            </ul>
                        </td>
                    </tr>
                </table>
            </article>
        </section>


        <div class="x-unit-gallery">
            <div class="owl-carousel">
              <?php echo do_shortcode( '[acf field="unit_gallery"]' ); ?>
            </div>
        </div>

        
    <div class="x-location-landmark">
        <div class="x-location-gmaps">
            
             <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="650" id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo do_shortcode( '[acf field="lat"]' ); ?>,<?php echo do_shortcode( '[acf field="lng"]' ); ?>&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:650px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:650px;width:100%;}</style></div>
            
        </div>
        <!--
        <div class="x-location-landmarks">
            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>
        </div>-->
    </div>

    <div class="x-contact-section">
        <div class="x-contact-section-content">
            <div class="x-contact-section-title">
                <span class="x-contact-section-subtitle"><?php echo do_shortcode( '[acf field="ready_for_your_new_home_label"]' ); ?></span>
                <h2 class="x-contact-section-heading"><?php echo do_shortcode( '[acf field="send_us_an_inquiry_label"]' ); ?></h2>
            </div>

            <div class="x-contact-container">
                <div class="x-contact-basic">
                    <span class="x-contact-pre"> <?php echo do_shortcode( '[acf field="contact_us_label"]' ); ?> </span>
                    <!--<h3><?php echo do_shortcode( '[acf field="lets_get_in_touch_label"]' ); ?></h3>-->
                        <h3><?php if(ICL_LANGUAGE_CODE=='ar') { the_field('company_name_ar', 'option'); } else { the_field('company_name', 'option') ;} ?></h3>

                    <ul>
                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/envelope.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="email_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php the_field('email', 'option'); ?></span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/smartphone.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="call_us_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php the_field('phone', 'option'); ?></span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/placeholder.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="address_label"]' ); ?></span>
                            <span class="x-contact-entry-label"><?php if(ICL_LANGUAGE_CODE=='ar') { the_field('address_ar', 'option'); } else { the_field('address', 'option') ;} ?></span>
                        </li>
                    </ul>
                </div>

                <div class="x-contact-form-container">
                    <!-- <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?> -->
                    <?php if(ICL_LANGUAGE_CODE=='ar') { echo do_shortcode( '[contact-form-7 id="33076" title="Unit Inquiry Form AR"]' ); } else {echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]"]' );}?>
                </div>
            </div>
        </div>
    </div>


    </main>
</section>






<?php get_footer();
