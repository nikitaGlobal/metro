<?php get_header();

?>

<div class="featureProjects" style="padding-top: 0;">
    <div class="container">

		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<p id="breadcrumbs" class="pagesBrowse">', '</p>' );
		} ?>

        <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedL bgDottedLAgents" style="width: 130px;">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedR" style="width: 100px;">

        <div class="projectHeading">
            <div class="middleSlash"></div>
            <h2><?php echo pll__( 'Our Agents' ); ?></h2>
            <div class="middleSlash"></div>
        </div>

        <div id="topSearch" class="aboutBtm">
            <p><?php echo pll__( 'Meet and contact the agents working at Metropolitan Premium Properties' ); ?></p>
        </div>

        <div class="row propertiesRow" style="padding-top: 0px;">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-md-4">
                        <div class="agentImg">
							<?php $pic = get_field( 'photo' )['url'] ? get_field( 'photo' )['url'] : '/wp-content/themes/framework/assets/images/no-image.jpg'; ?>
                            <img src="<?php echo $pic; ?>">
                            <div class="agentContact">
                                <div class="agentContactInner">
                                    <p><?php the_field( 'full_name' ) ?></p>
                                    <p><span><?php echo pll__( 'Speaks' ); ?>:</span> <?php the_field( 'languages' ) ?></p>
                                </div>
								<?php if ( get_field( 'phone' ) ) : ?>
                                    <div class="agentContactInnerCon">
                                        <a href="tel:<?php the_field( 'phone' ) ?>">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/whatsapp.svg">
                                        </a>
                                    </div>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
			<?php endif; ?>
        </div>
    </div>
</div>

<?php if ( $wp_query->max_num_pages > 1 ):
	$query_count = 10; ?>

    <script>
      var current_page = 1,
        max_pages = '<?php echo $wp_query->max_num_pages; ?>';
    </script>

    <div class="viewAll agentsViewMore" data-count="<?php echo $query_count; ?>">
        <a href="#"><?php echo pll__( 'View more' ); ?> <img src="<?php echo get_template_directory_uri() ?>/assets/images/load.svg"></a>
    </div>
<?php endif; ?>

<?php get_footer();
