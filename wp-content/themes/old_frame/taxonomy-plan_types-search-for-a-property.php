<?php
get_header();

add_action('asp_post_parse_filters', 'asp_change_the_filters', 10, 2);
function asp_change_the_filters($search_id, $options) {
    if ( $search_id == 1 ) {
        // Remove a filter by label
        wd_asp()->front_filters->remove("property_type__4");
    }
}
?>



    <div class="clearfix"></div>
    <div class="featureProjects property-archive">

    	<main class="property-catalog-wrap">
    		<section class="property-search-section">
    		    <article>
        			<h1 class="property-section-title">
        			    <?php
    						echo pll__("Search for Luxury Properties in Dubai");
 				?>
        			</h1>
        			
        			<div class="property-search-filters">
        			    
        			<?php
        			    //echo do_shortcode("[wd_asp id=5]");
        			    //echo do_shortcode('[wpdreams_ajaxsearchpro_results id=5 element="div"]');
        			    echo do_shortcode("[wd_asp elements='search,settings' ratio='100%,100%' id=1]");
        			    
        			    
        			?>
        		    </div>
        			
    			</article>
    		</section>
    		
    		<!--<ul class="toHighlight">
    		    <li>This Property to Highlight</li>
    		    <li>This to Highlight Property</li>
    		</ul>-->
    		
    		<section class="property-listing-catalog-section">
    		    
    		    
    		   <div class="property-container-section">
        			    
    		        <?php echo do_shortcode("[wd_asp elements='results' ratio='100%' id=1]") ;?>
    		   </div>
    		</section>

    	</main>
    </div>


<?php
get_footer();