<?php /* Template Name: Primary Services */
get_header();
if ( get_field( 'bg' ) ) {
	$background = 'style="background-image: url('.get_field( 'bg' )['url'].');"';
	$head = get_field( 'head' );
}
?>
    <div class="headPage">
        <div class="headBG" <?=$background?>></div>

        <div class="headText">
            <div class="container">
                <div class="headText__wrp">
                    <img src="/wp-content/themes/framework/assets/images/vpat.svg" class="vpatPA">
                    <div class="headText__header">
                        <h2 class="headText__title"><?=$head['title']?></h2>
                        <div class="separator"></div>
                    </div>
                    <div class="headText__content">
                        <div class="headForm">
                                <div class="headForm-body">
                                    <p class="headForm-head"><?=$head['form']?></p>
                                    <div class="footerForm" style="padding:10px;position: relative;">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">
                                        <?php echo do_shortcode( get_field('form_shortcode') ); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="headText__view">
                            <?=$head['desc']?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php if ( have_rows( 'row' ) ) : ?>
    <?php while ( have_rows( 'row' ) ) : the_row(); ?>
    <section class="contentSection featureProjects">
    <?php if ( have_rows( 'content' ) ) : ?>
        <?php while ( have_rows( 'content' ) ) : the_row(); $row = get_row_layout();?>

            <?php if ($row === 'title_block') : ?>
                <div class="container">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedR" style="width: 100px;">

                    <div class="projectHeading <?php if(get_sub_field( 'style' )=='left'){echo ' left';}?>">
                        <?php if(get_sub_field( 'style' )!='left'){?><div class="middleSlash"></div><?php } ?>
                        <h2><?php echo get_sub_field( 'title' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($row === 'text_block') : ?>
                <div class="container">
                    <div class="textBlock pd30">
                        <?php echo get_sub_field( 'content' ) ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($row === 'ceo') : ?>
                <div class="ceoQuote" style="background-image: url('<?=get_sub_field( 'bg' )?>')">
                    <div class="container">
                        <div class="ceoQuote-content">
                            <div class="ceoQuote-text"><?php echo get_sub_field( 'text' ) ?></div>
                            <div class="ceoQuote-сap"><?php echo get_sub_field( 'сap' ) ?></div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($row === 'global_reach') : ?>
                <div class="globalReach">
                    <div class="container">
                        <div class="globalReachTitle">
                            <?php echo get_sub_field( 'title' ) ?>
                        </div>
                        <div class="globalReachText">
                            <?php echo get_sub_field( 'content' ) ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($row === 'domain') : ?>
                <div class="container catalogInfo">
                    <div class="row abtRow noPd">
                    <div class="col-md-6 first">
                        <div class="projectHeading left">
                            <h2><?php echo get_sub_field( 'title' ) ?></h2>
                            <div class="middleSlash"></div>
                        </div>
                        <div class="catalogInfo-content">
                            <?php echo get_sub_field( 'cnt' ) ?>
                        </div>
                    </div>
                    <div class="col-md-6 last">
                        <div class="catalogInfo-img">
                            <img src="<?=get_sub_field( 'img' )?>">
                        </div>
                    </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($row === 'btn') : ?>
                <div class="container">
                        <div class="valBtnCont" id="topSearch"><button type="button" data-toggle="modal" data-target="#consultationModal" class="bannerValBtn"><?=get_sub_field( 'name' )?></button></div>
                </div>
            <?php endif; ?>

            <?php if ( get_row_layout() === 'bwi') : ?>
                    <div class="container">
                        <?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                            <div class="row abtRow">
                                <?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                    <div class="<?=get_sub_field('style_cards');?>">
                                        <div>
                                            <div class="abtRowTCont">
                                                <div class="abtRowTL">
                                                    <img src="<?php echo $subarr['icon'] ?>" width="50">
                                                </div>
                                                <div class="abtRowTR">
                                                    <p><?php echo $subarr['header'] ?></p>
                                                </div>
                                            </div>
                                            <p class="abtRowBCont"><?php echo $subarr['description'] ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
            <?php endif; ?>

            <?php if ( get_row_layout() === 'property') : ?>
                <div class="container">
                    <?php if ( !empty( get_sub_field( 'item' ) ) ) : ?>
                        <div class="propertySlider owl-carousel">
                            <?php
                            $gallery = get_sub_field( 'item' );
                            foreach ($gallery as $item): $content = $item['content']; ?>
                                <div class="propertySlider__item property">
                                    <div class="propertyImg">
                                        <img src="<?=$item['img']['sizes']['large']?>">
                                        <span class="propertyType <?=$content['type']?>"><?=$content['type']?></span>
                                    </div>
                                    <div class="propertyHead">
                                        <div class="propertyHead-name"><?=$content['name']?></div>
                                        <div class="propertyHead-locate"><?=$content['loc']?></div>
                                        <ul class="propertyHead-icons">
                                            <li class="propertyHead-icons_item beds">Beds: <?=$content['beds']?></li>
                                            <li class="propertyHead-icons_item baths">Baths: <?=$content['baths']?></li>
                                            <li class="propertyHead-icons_item area">Sq Ft: <?=number_format($content['area'], 0, '.', ',')?></li>
                                        </ul>
                                        <div class="propertyHead-price">AED <?=number_format($content['price'], 0, '.', ',')?></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php /*if (get_row_layout() === 'two_cln') : ?>
                    <div class="container">
                        <div class="twoCln">
                            <?php if ( ! empty( get_sub_field( 'row' ) ) ) : $count = 1; ?>
                                <?php foreach (get_sub_field( 'row') as $subarr ) : $side = ( $count % 2 == 1 ) ? "R" : "L" ?>
                                    <div class="twoCln-row side<?=$side?>">
                                        <div class="twoCln-item title">
                                            <h2 class="twoCln-h2"><?php echo $subarr['title'] ?></h2>
                                        </div>
                                        <div class="twoCln-item content">
                                            <?php echo $subarr['desc'] ?>
                                        </div>
                                    </div>
                                    <?php $count ++; endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
            <?php endif;*/ ?>

            <?php if (get_row_layout() === 'two_cln') : ?>
            <div class="servicesWrp">
                    <div class="servicesBlock">
                        <?php if ( ! empty( get_sub_field( 'row' ) ) ) : $pointer = ""; $class = ' active';?>
                            <?php foreach (get_sub_field( 'row') as $subarr ): ?>
                                <div class="servicesItem<?=$class?>">
                                    <div class="container">
                                    <div class="servicesCln content">
                                        <div class="projectHeading left">
                                            <h2><?php echo $subarr['title'] ?></h2>
                                            <div class="middleSlash"></div>
                                        </div>
                                        <div class="servicesText">
                                            <?php echo $subarr['desc'] ?>
                                        </div>
                                        <div class="valBtnCont left">
                                            <button type="button" data-toggle="modal" data-target="#consultationModal" class="bannerValBtn"><?php echo $subarr['btn'] ?></button>
                                        </div>
                                    </div>
                                    <div class="servicesCln img">
                                        <img class="servicesImg" src="<?php echo $subarr['img'] ?>">
                                    </div>
                                    </div>
                                </div>
                            <?php
                                $pointer.='<li class="sPoiner-item'.$class.'"><span class="sPoiner-name">'. $subarr['title'] .'</span></li>';
                                $class=""; endforeach;
                            ?>
                        <?php endif; /*
                        <div class="servicesNav">
                            <a href="#prev" class="servicesNav-btn prev"></a>
                            <ul class="sPoiner"><?=$pointer?></ul>
                            <a href="#next" class="servicesNav-btn next"></a>
                        </div>
                         */ ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ( get_row_layout() === 'services') : ?>
                <div class="container">
                    <div class="row abtRow services">
                        <?php if ( !empty( get_sub_field( 'cnt1' ) ) ) : ?>
                        <div class="col-md-4">
                            <div class="servicePackages silver">
                                <h3 class="servicePackages-head"><?=get_sub_field( 'head1' )?></h3>
                                <?=get_sub_field( 'cnt1' )?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if ( !empty( get_sub_field( 'cnt2' ) ) ) : ?>
                        <div class="col-md-4">
                            <div class="servicePackages gold">
                                <h3 class="servicePackages-head"><?=get_sub_field( 'head2' )?></h3>
                                <?=get_sub_field( 'cnt2' )?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if ( !empty( get_sub_field( 'cnt3' ) ) ) : ?>
                        <div class="col-md-4">
                            <div class="servicePackages diamond">
                                <h3 class="servicePackages-head"><?=get_sub_field( 'head3' )?></h3>
                                <?=get_sub_field( 'cnt3' )?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if ( !empty( get_sub_field( 'btn' ) ) ) : ?>
                    <div class="valBtnCont">
                        <button type="button" data-toggle="modal" data-target="#consultationModal" class="bannerValBtn"><?=get_sub_field( 'btn' )?></button>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if ( get_row_layout() === 'full-width_block' ) : ?>
            <div class="full-width_block" style="background-image: url(<?=get_sub_field('bg')?>)">
                <div class="container">
                    <div class="col-md-8 fWbContent">
                        <img src="/wp-content/themes/framework/assets/images/vpat.svg" class="vpatLR">
                        <div class="fwHead projectHeading left">
                            <h2><?=get_sub_field('title' ) ?></h2>
                            <div class="middleSlash"></div>
                        </div>
                        <div class="fwDesc">
                            <?=get_sub_field('desc') ?>
                        </div>
                        <div class="valBtnCont left">
                            <button type="button" data-toggle="modal" data-target="#consultationModal" class="bannerValBtn"><?=get_sub_field('btn')?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ( get_row_layout() === 'grid' ) : ?>
                    <div class="container">
                        <?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                            <div class="row abtRow">
                                <?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                    <div class="col-md-4">
                                        <div class="BenefitsCont">
                                            <div class="abtRowTCont">
                                                <div class="abtRowTL">
                                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg" width="50">
                                                </div>
                                                <div class="abtRowTR">
                                                    <p><?php echo $subarr['text'] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
            <?php endif; ?>

            <?php if ( get_row_layout() === 'expert' ) : ?>
                <div class="container">
                    <div class="row abtRow expert">
                        <div class="expert_img">
                            <img src="<?=get_sub_field( 'photo' )?>" alt="<?=get_sub_field( 'name' )?>">
                        </div>
                        <div class="expert_info">
                            <div class="fwHead projectHeading left">
                                <h2><?=get_sub_field('name' ) ?></h2>
                                <div class="middleSlash"></div>
                                <div class="expert_caprion"><?=get_sub_field('сaption' ) ?></div>
                            </div>
                            <div class="expert_desc">
                                <?=get_sub_field('description')?>
                            </div>
                            <div class="expert_contacts">
                                <a href="tel:<?=get_sub_field('phone' ) ?>" class="expert_contacts-phone"><?=get_sub_field('phone' ) ?></a>
                                <a href="mailto:<?=get_sub_field('email' ) ?>" class="expert_contacts-email"><?=get_sub_field('email' ) ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        <?php endwhile; ?>
    <?php endif; ?>
    </section>
    <?php endwhile; ?>
<?php endif; ?>

    <div class="pmFooter">
        <div class="pmfooterDot">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>
        <div class="pmfooterDotL">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>

        <div class="pmFooterInner">
            <div class="projectHeading">
                <div class="middleSlash"></div>
                <h2><?php echo get_field( 'footer_header' ) ?></h2>
                <div class="middleSlash"></div>
            </div>
            <div class="footerForm">
	            <?php echo do_shortcode( get_field('form_shortcode') ); ?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="consultationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">

						<?php echo do_shortcode( get_field('form_shortcode') ); ?>

                    </div>
                </div>
            </div>

        </div>
    </div>

<?php get_footer();