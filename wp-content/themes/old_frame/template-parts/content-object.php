<div class="projectContainer">
	<div class="imageContainer">
		<a href="<?php the_permalink(); ?>">
			<img class="object-pic" src="<?php the_post_thumbnail_url(); ?>">
			<div class="projectImgText">
				<div class="projectImgTextHCont">
					<h3><?php the_title(); ?></h3>
					<div class="hslash"></div>
				</div>
				<div class="propertyDescContainer bounceInUp">
					<p class="propertyDesc"><?php echo get_the_excerpt(); ?></p>
					<div class="viewMoreContainer">
						<span class="viewMore"><?php echo pll__( 'View more' ); ?></span>
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/rightarrow.svg" class="viewMoreArrow">
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="propertyDetails">
		<div class="pd1">
			<div class="pd1left">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/type.svg">
                <span><?php echo get_the_terms( get_the_ID(), 'property_type' )[0]->name;?></span>
			</div>
			<div class="pd1Right">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/area.svg">
				<span><?php echo do_shortcode( '[convert_ft sq="'.get_field('unit_builtup_area').'"  ]' ); ?></span>
			</div>
		</div>
		<div class="pd1">
			<div class="pd1left">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/location.svg">
				<span><?php the_field('community')?></span>
			</div>
			<div class="pd1Right">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/price.svg">
				<span><?php echo strip_tags( do_shortcode('[wpcs_current_currency text="" flag=0]') ); echo do_shortcode( '[wpcs_price value='. get_field('price') .']' )?></span>
			</div>
		</div>
		<div class="pd2">
			<div class="pd1left">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/bed.svg">
				<span><?php the_field('bedrooms')?> Bedroom</span>
			</div>
			<div class="pd1Right"></div>
		</div>
	</div>
</div>