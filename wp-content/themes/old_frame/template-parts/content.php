<?php
/**
 * Template part for displaying posts and page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

$social_post = get_field( 'social_post', 'option' );
?>

<h1><?php the_title(); ?></h1>
<div class="postRead">
    <div class="postReadL">
		<?php if ( get_field( 'time_to_read' ) ) : ?>
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/book.svg" alt="book">
            <p>Time for reading: <span><?php the_field( 'time_to_read' ) ?></span></p>
		<?php endif; ?>
    </div>

    <div class="postReadR">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/eye.svg" alt="eye">
        <p><?php if ( function_exists( 'the_views' ) ) {
				the_views();
			} ?></p>
    </div>

</div>

<div class="postMain">
	<?php the_content();
  $images = get_attached_media( 'image', $post->ID ); 
  $video = parse_url(get_field('video', $post->I));
  ?>
    <?php if($images): ?>
    <div class="img-post">
		<?php foreach ( $images as $index ) : ?>
            <a href="<?php echo $index->guid; ?>" data-lightbox="oobject">
                <img class="img-post-item" src="<?php echo $index->guid; ?>" alt="img">
            </a>
		<?php endforeach; ?>
    </div>
    <?php endif; ?>
    <?php if($video['path']): ?>
    <div class="video-post">
		  <iframe width="100%" height="424" src="https://www.youtube.com/embed/<?=$video['path']?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope;" allowfullscreen></iframe>
    </div>
    <?php endif; ?>

    <div class="nextPrev">
		<?php previous_post_link( '%link',
			'<img src="/wp-content/themes/framework/assets/images/arrowleft.svg" alt="Previous"> ' . pll__( 'Previous article' ) ); ?>
		<?php next_post_link( '%link',
			pll__( 'Next article' ) . ' <img src="/wp-content/themes/framework/assets/images/arrowright.svg" alt="Next">' ); ?>
    </div>


    <div class="postRating">
        <div class="postReviews">
			<?php rating(); ?>
        </div>

		<?php if ( $social_post['show_block'] ): ?>
            <div class="postShare">
                <span><?php echo pll__( 'Share' ); ?>:</span>
                <div class="socialIcons">
					<?php foreach ( $social_post['item'] as $item ) : ?>
                        <a href="<?php echo $item['url']; ?>"><i class="icon-<?php echo $item['icon']; ?>"></i></a>
					<?php endforeach; ?>
                </div>
            </div>
		<?php endif; ?>

    </div>
</div>