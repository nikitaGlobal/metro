<div class="col-md-4 d-none order-0 order-sm-1 mb-5 mb-sm-0" style="position: relative;">
	<div class="mapControls">
		<div class="checkbox">
			<label><input type="checkbox" value=""><?php echo pll__( 'Search as I move the map' ); ?></label>
		</div>
		<a href="#"><?php echo pll__( 'Show Transit' ); ?></a>
	</div>
	<div class="mapSection">
		<div id="mapProperty" style="position: relative;width: 100%;height: 100%;"></div>
	</div>
</div>