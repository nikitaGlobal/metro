<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

?>

<section class="no-results not-found mx-auto">
	<header class="page-header">
		<h1 class="page-title"><?php echo pll__('Nothing Found'); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<p><?php echo pll__('It seems we can&rsquo;t find what you&rsquo;re looking for.'); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->
