<div class="paginationCont">
	<?php $args = array(
		'end_size'           => 5,
		'prev_text'          => '<img src="/wp-content/themes/framework/assets/images/next.png">',
		'next_text'          => '<img src="/wp-content/themes/framework/assets/images/next.png">',
		'screen_reader_text' => ' ',
	);

	the_posts_pagination( $args );
	?>
</div>