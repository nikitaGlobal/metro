<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framework
 */

if ( is_dynamic_sidebar() ): ?>
    <div class="col-md-3 footer1 with-logo">
		<?php if(is_active_sidebar( 'footer-1' )){ ?>
        <?php if ( is_front_page() ) : ?>
            <div class="logo-footer">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/mpd-black-logo.svg" alt="<?php echo get_bloginfo( "name" ); ?>">
            </div>
        <?php else :?>
            <a class="logo-footer" href="<?php echo esc_url( home_url( '/' ) ) ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/mpd-black-logo.svg" alt="<?php echo get_bloginfo( "name" ); ?>">
            </a>
        <?php endif; ?>
        <?php dynamic_sidebar( 'footer-1' );} ?>
    </div>

	<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
        <div class="col footer1">
			<?php dynamic_sidebar( 'footer-2' ); ?>
        </div>
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
        <div class="col footer1">
			<?php dynamic_sidebar( 'footer-3' ); ?>
        </div>
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
        <div class="col footer1">
			<?php dynamic_sidebar( 'footer-4' ); ?>
        </div>
	<?php endif; ?>

    <div class="col-md-3">
		<?php is_active_sidebar( 'footer-5' ) ? dynamic_sidebar( 'footer-5' ) : ''; ?>
    </div>
<?php endif; ?>