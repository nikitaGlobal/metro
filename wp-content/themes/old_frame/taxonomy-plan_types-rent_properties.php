<?php
get_header();

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}


$property_type = $_GET["property_type"];
$action = $_GET["action"];

 if ($action=='Buy') {
     if ($property_type == 'Villa') { $search_id = 11;}
     if ($property_type == 'Apartment') { $search_id = 12;}
     if ($property_type == 'Land Residential') { $search_id = 13;}
     if ($property_type == 'Office') { $search_id = 14;}
     if ($property_type == 'Penthouse') { $search_id = 15;}
     if ($property_type == 'Townhouse') { $search_id = 16;}
     //else $search_id = 10;
    }  
    else if ($action=='Rent') {
        if ($property_type == 'Villa') { $search_id = 18;}
        if ($property_type == 'Apartment') { $search_id = 19;}
        if ($property_type == 'Land Residential') { $search_id = 20;}
        if ($property_type == 'Office') { $search_id = 21;}
        if ($property_type == 'Penthouse') { $search_id = 22;}
        if ($property_type == 'Townhouse') { $search_id = 23;}
        if ($property_type == '') { $search_id = 1;}
    }
    else $search_id = 1;
 

?>



    <div class="clearfix"></div>
    <div class="featureProjects property-archive">

    	<main class="property-catalog-wrap">
    		<section class="property-search-section">
    		    <article>
        			<h1 class="property-section-title">
        			    <?php
    						echo pll__("Search for Luxury Properties in Dubai");
 				?>
        			</h1>
        			
        			<div class="property-search-filters">
        			    
        			<?php
        			    //echo do_shortcode("[wd_asp id=5]");
        			    //echo do_shortcode('[wpdreams_ajaxsearchpro_results id=5 element="div"]');
        			   echo do_shortcode('[wd_asp elements="search,settings" ratio="100%,100%" id='.$search_id.']');
        			?>
        		    </div>
        			
    			</article>
    		</section>
    		
    		<!--<ul class="toHighlight">
    		    <li>This Property to Highlight</li>
    		    <li>This to Highlight Property</li>
    		</ul>-->
    		
    		<section class="property-listing-catalog-section">
    		      <div class="col-md-4">
                            
<!--
            <a href="http://dev.metropolitan.realestate/plan_types/rent_properties/">
                 <p><?php// echo 'Current Search > Rent' ?></p>
                </a>
                 <?php 
                //if ($action <> '') { ?>
                 <a href="http://dev.metropolitan.realestate/plan_types/rent_properties/?action=<?php// echo $action?><?php// echo "&property_type=".$property_type ?>">
                <?php// echo $property_type ?>
            </a>
 -->                                 
            <?php// } ?>
                          
                            
                            
                        </div>
    		    
    		   <div class="property-container-section">
        			    
    		        <?php echo do_shortcode("[wd_asp elements='results' ratio='100%' id=".$search_id."]") ;?>
    		   </div>
    		</section>

    	</main>
    </div>


<?php
get_footer();