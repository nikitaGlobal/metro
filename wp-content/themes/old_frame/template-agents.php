<?php
    /* Template Name: Team */
    get_header();
?>

<div class="featureProjects" style="padding-top: 0;">
    <div class="container">
		<?php if (function_exists( 'yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p id="breadcrumbs" class="pagesBrowse">', '</p>' );
		} ?>

        <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedL bgDottedLAgents" style="width: 130px;">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedR" style="width: 100px;">

        <div class="projectHeading">
            <div class="middleSlash"></div>
            <h2><?php echo pll__( 'Our Agents' ); ?></h2>
            <div class="middleSlash"></div>
        </div>

        <div id="topSearch" class="aboutBtm">
            <p><?php         echo pll__( 'Meet and contact the agents working at Metropolitan Premium Properties' );             ?></p>
        </div>
        <?php
            $terms = get_terms([
                'taxonomy' => 'departments',
                'hide_empty' => true,
            ]);
            foreach( $terms as $term ):
                $id = $term->term_id;
                $catName = $term->name;
                $args = [
                    'post_type' => 'agents', 
                    'posts_per_page' => -1,
                    'tax_query' => [
                        [
                            'taxonomy' => 'departments', 
                            'field' => 'term_id', 
                            'terms' => $id, 
                            'include_children' => false,
                        ],
                    ],
                ];
                $posts_with_term = new WP_Query( $args );
        ?>
        <div class="agentsBlock">
            <div class="agentsTitle projectHeading left">
                <h2><?=$catName?></h2>
                <div class="middleSlash"></div>
            </div>
            <div class="row propertiesRow agentsRow" style="padding-top: 0px;">
				<?php while ( $posts_with_term->have_posts() ) : $posts_with_term->the_post(); ?>
                    <div class="col-md-4">
                        <div class="agentImg">
							<?php $pic = get_field( 'photo' )['sizes']['large']; if(!$pic){'/wp-content/themes/framework/assets/images/no-image.jpg';} ?>
                            <img src="<?php echo $pic; ?>">
                            <div class="agentContact">
                                <div class="agentContactInner">
                                    <p><?php the_field( 'full_name' ) ?></p>
                                    <p><span><?php  echo pll__( 'Speaks' ); ?>:</span> <?php the_field( 'languages' ) 
                                    ?></p>
                                </div>
								<?php if ( get_field( 'phone' ) ) : ?>
                                    <div class="agentContactInnerCon">
                                        <a href="https://wa.me/<?=preg_replace('~[^0-9]+~','',get_field('phone')); ?>" target="_blank">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/whatsapp.svg">
                                        </a>
                                    </div>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>
    <?php 
    wp_reset_postdata();
    endforeach;
    ?>
    </div>
</div>

<?php get_footer();
