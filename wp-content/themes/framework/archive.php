<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

get_header();
?>

    <div class="featureProjects estate-object" style="padding-top: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row propertiesRow" style="padding-top: 0px;">
						<?php if ( have_posts() ) :
							while ( have_posts() ) : the_post(); ?>
                                <div class="col-md-4">
	                                <?php get_template_part( 'template-parts/content', 'object' ); ?>
                                </div>
							<?php endwhile;
						endif; ?>

                    </div>
                </div>

	            <?php get_template_part( 'template-parts/content', 'mapbox' ); ?>

            </div>

        </div>
    </div>

<?php
get_template_part( 'template-parts/pagination' );
get_footer();