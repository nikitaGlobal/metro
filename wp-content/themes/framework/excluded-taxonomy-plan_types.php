<?php
get_header();
?>


    <div class="clearfix"></div>
    <div class="featureProjects property-archive">

    	<main class="property-catalog-wrap">
    		<section class="property-search-section">
    		    <article>
        			<h1 class="property-section-title">Properties for Sale</h1>
        			<?php echo do_shortcode( '[searchandfilter slug="listing-search-filters"]' ); ?>
        			
        			
    			</article>
    		</section>

    		<section class="property-listing-catalog-section">
    			<article class="property-listing-catalog-article">
                        
						<?php
						$propertyCurrentPage = get_query_var('paged');
						
						$args = array(
						    'post_type' => 'properties',
						    'post_status' => 'publish',
						    //'product_cat' => 'buy',
						    'post_name' => 'buy_properties_ar',
						    'posts_per_page' => 16,
						    'paged' => $propertyCurrentPage,
						);
						$args['search_filter_id'] = 32006;
						$arr_posts = new WP_Query( $args );
						 
						if ( $arr_posts->have_posts() ) :
						    while ( $arr_posts->have_posts() ) :
						        $arr_posts->the_post();
						        ?>

						        
						            
							
								<?php 
									$propertyId = get_the_ID();
						        	$propertyCommunity = get_field('community', $propertyId);
						        	$propertyType = get_field('property_type', $propertyId);
						        	$propertyBedRoom = get_field('bedrooms', $propertyId);
						        	$propertyArea = get_field('unit_builtup_area', $propertyId);
						        	$propertyPrice = get_field('price', $propertyId);
						        ?>                    

								<div class="listing-entry">
									<a id="post-<?php the_ID(); ?>" class="listing-img" href="<?php the_permalink(); ?>">
											<?php 
						                    //POST THUMBNAIL
												if ( has_post_thumbnail() ) :
												    the_post_thumbnail();
												endif;
											?>
									</a>

									<span class="listing-entry-title"><?php the_title(); ?></span>
									<span class="listing-entry-community"><?php echo $propertyCommunity; ?></span>
									<span class="listing-entry-dets"><?php echo $propertyType; ?> | <?php echo $propertyBedRoom; ?>BD | <?php echo round($propertyArea); ?> SQ FT</span>
									<span class="listing-entry-price">AED <?php echo number_format($propertyPrice); ?></span>
								</div>
						        
						        <?php
						    endwhile;
						    
						    echo "<div class='listing-pagination'>";
                            get_template_part( 'template-parts/pagination' );
                            echo "</div>";
						endif;

						?>


    			</article>
    		</section>

    	</main>
    </div>


<?php
get_footer();