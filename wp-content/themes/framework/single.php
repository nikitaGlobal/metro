<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package framework
 */

get_header();
?>

<!--Hello-->
    <div class="featureProjects" style="padding-top: 0px;">
        <div class="container">

            <div class="postdetails">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content' );
					endwhile;
				endif;
				?>
            </div>

        </div>
    </div>

<?php if ( get_field( 'read_more' )[0]['news'] ) : ?>
    <div class="featureProjects" style="padding-top: 5px !important;">
        <div class="container" style="border-top: solid 1px #D7D7D7; padding-top: 40px;">

			<?php get_template_part( 'template-parts/content', 'related' ); ?>

        </div>
    </div>
<?php endif; ?>

<?php get_footer();
