<?php
get_header();
$term      = get_queried_object();
$title_cat = get_field( 'title_category', $term );
$showCat = get_field( 'childCat', $term );
$bg = get_field( 'bg', $term );
if($bg==""){$bg='/wp-content/themes/framework/assets/images/banner-news.jpg';}
?>
    <div class="headPage">
        <div class="headBG cat" style="background-image: url(<?=$bg?>)"></div>
        <div class="headText">
            <div class="container">
                <div class="headText__wrp">
                    <img src="/wp-content/themes/framework/assets/images/vpat.svg" class="vpatPA">
                    <div class="headText__header">
                        <h2 class="headText__title"><?php echo $title_cat ? : get_queried_object()->name; ?></h2>
                        <div class="separator"></div>
                    </div>
                    <div class="headText__content">
                        <div class="headText__view">
                            <?php echo category_description(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="featureProjects">
        <div class="container">
            <div class="projectHeading mediaHead">
                <div class="title-wrap">
                    <h2 style="padding-left: 0px;"><?=get_field('subhead', $term)?></h2>
                    <div class="middleSlash"></div>
                </div>
                <?php if($showCat){ ?>
                <div class="navCat">
                    <ul class="navCatList">
                        <li class="navCatList-item">
                            <a
                                data-id="<?php if ($term->parent > 0) { echo $term->parent; } else { echo $term->term_id; } ?>"
                                <?php if($term->parent != 0): ?>
                                href="<?php if ($term->parent > 0) { echo get_term_link(get_term($term->parent, "category"), "category"); } else { echo get_term_link($term, "category"); } ?>"
                                <?php endif; ?>
                                class="navCatList-link <?php if ($term->parent == 0) { echo " active"; } ?>">
                                All <?php if ($term->parent > 0) { $termID = $term->parent; } else { $termID = $term->term_id; } $args = array('posts_per_page' => -1,'post_type' => 'post','cat' => $termID); $newquery = new WP_Query($args); echo "(".$newquery->post_count.")"; ?>
                            </a>
                        </li>
                        <?php if ($term->parent > 0) {$terms = get_terms("category", "hide_empty=0&pad_counts=1"); $terms = wp_list_filter($terms, array('parent'=>$term->parent));} else {$terms = get_terms("category", "hide_empty=0&pad_counts=1"); $terms = wp_list_filter($terms, array('parent'=>$term->term_id));} foreach ($terms as $item) { if($item->count!=0){?>
                        <li class="navCatList-item">
                            <a
                                data-id="<?php echo $item->term_id; ?>"
                                <?php if($term->term_id != $item->term_id): ?>
                                href="<?php echo get_term_link($item, "category"); ?>"
                                <?php endif; ?>
                                class="navCatList-link <?php if($term->term_id == $item->term_id){ echo " active"; } ?>">
                                <?php echo $item->name." (".$item->count.")"; ?>
                            </a>
                        </li>
                        <?php } } ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <div class="row propertiesRow">
                <?php if ( have_posts() ) :
                    while ( have_posts() ) : the_post(); ?>
                        <div class="col-md-6">
                            <?php if(in_category(array('Video','Видео','فيديو'))):
                                $video = parse_url(get_field('video'));
                            ?>
                                <div class="blogPost newsTemlate">
                                    <div class="postVideo">
                                    <iframe width="100%" height="321" src="https://www.youtube.com/embed/<?=$video['path']?>?rel=0&modestbranding=1&showinfo=0" frameborder="0" allow="accelerometer; encrypted-media; gyroscope;" allowfullscreen></iframe>
                                    </div>
                                    <div class="postContent">
                                        <p class="blogPostH"><?php the_title(); ?></p>
                                    </div>
                                </div>  
                            <?php else: ?>
                            <a href="<?php the_permalink(); ?>" class="blogPostL">
                                <div class="blogPost newsTemlate">
                                    <div class="postImg">
                                    <?php if ( get_the_post_thumbnail_url() ) : ?>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php get_the_title(); ?>">
                                    <?php else : ?>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-image.jpg'?>" alt="no images">
                                    <?php endif; ?>
                                        <div class="postDate"><?php echo get_the_date(); ?></div>
                                    </div>
                                    <div class="postContent">
                                        <p class="blogPostH"><?php the_title(); ?></p>
                                        <div class="blogPostP"><?php echo get_field('desc'); ?></div>
                                    </div>
                                </div>
                            </a>
                            <?php endif; ?>
                        </div>
                    <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>

<?php get_template_part('template-parts/pagination');?>

<?php get_footer();