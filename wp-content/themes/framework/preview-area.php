<?php
    $keys              = get_fields();
    $propertyCommunity = get_field('community', get_the_ID());
    $propertyName      = get_field('property_name', get_the_ID());
    $propertyEmirate   = get_field('of_emirates', get_the_ID());
    $propertyType      = get_field('of_property_type', get_the_ID());
    $propertyPlan      = get_field('of_property_plan', get_the_ID());
    $propertyBedRoom   = get_field('bedrooms', get_the_ID());
    $propertyBathRoom  = get_field('no_of_bathroom', get_the_ID());
    $propertyArea      = get_field('unit_builtup_area', get_the_ID());
    $propertyPrice     = number_format(get_field('price', get_the_ID()));
    $propertyDesc      = get_field('property_writeup', get_the_ID());
    $planType          = get_the_terms(get_the_ID(), 'plan_types');

?>
<div class="propertySlider__item property">
    <a href="<?= get_permalink(); ?>" class="propertyItem-link">
        <div class="propertyImg">
            <img src="<?= get_the_post_thumbnail_url(get_the_ID()) ?>">
            <div class="propertyStickers">
                <span class="propertySticker <?= $planType[0]->slug ?>"><?= $propertyPlan ?></span>
                <span class="propertySticker type"><?= $propertyType ?></span>
            </div>
        </div>
    </a>
    <div class="propertyHead">
        <a href="<?= get_permalink(); ?>" class="propertyItem-link title">
            <div class="propertyHead-name"><?= get_the_title(); ?></div>
        </a>
    </div>
</div>