<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

get_header();
?>
    <div class="featureProjects" style="padding-top: 0px;">
        <div class="container">

            <div class="postdetails">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content' );
					endwhile;
				endif;
				?>
            </div>

        </div>
    </div>

<?php
get_footer();
