<?php /* Template Name: About us */

get_header();

if ( get_field('homebg') ){
	$background = 'style="background-image: url('. get_field('homebg') .');"';
} else {
	$background = 'style="background-image: url(/wp-content/themes/framework/assets/images/about.jpg);"';
}
?>
    <div class="bannerAbout" <?php echo $background; ?> >
        <div class="container">
			<?php if ( ! empty( get_field( 'headlines' ) ) ) : ?>
				<?php foreach ( get_field( 'headlines' ) as $subarr ) : ?>
					<?php if ( $subarr['headline_style'] === 'aboutHeader' ) : ?>
                        <h1 class="aboutHeader"><?php echo $subarr['headline_text'] ?></h1>
					<?php else : ?>
                        <p class="<?php echo $subarr['headline_style'] ?>"><?php echo $subarr['headline_text'] ?></p>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
        </div>
    </div>

<?php if ( have_rows( 'flex_content' ) ) : ?>
	<?php while ( have_rows( 'flex_content' ) ) : the_row(); ?>

		<?php if ( get_row_layout() === 'about' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects aboutPage">
                <div class="container">

                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>

                    <div class="row aboutBlockCont">
						<?php if ( ! empty( get_sub_field( 'cards' ) ) ) : $count = 1; ?>
							<?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="col-md-4">
                                    <div class="aboutBlock">
                                        <div class="aboutBlockH">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.png">
                                            <span class="aboutBlockHL"><?php echo $count ?></span>
                                        </div>
                                        <div class="aboutBlockB">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/Oval.svg">
                                            <p class="aboutBlockBH"><?php echo $subarr['title'] ?></p>
                                            <p class="aboutBlockBP"><?php echo $subarr['card_text'] ?></p>
                                        </div>
                                    </div>
                                </div>
								<?php $count ++; endforeach; ?>
						<?php endif; ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>

		<?php if ( get_row_layout() === 'metropolitan' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects">
                <div class="container">

                    <div class="block2H">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/companyrec.svg" class="companyrec">
                        <h2><?php echo get_sub_field( 'header_1' ) ?></h2>
                        <div class="block2HS"></div>
                        <p><?php echo get_sub_field( 'header_2' ) ?></p>
                    </div>

                    <div class="block2D">
						<?php if ( ! empty( get_sub_field( 'companies' ) ) ) :
							$arr_count = count( get_sub_field( 'companies' ) );
							$counter = 1;
							?>
							<?php foreach ( get_sub_field( 'companies' ) as $subarr ) : ?>
                            <div class="companyProfile <?php echo $counter == $arr_count ? "lastComBloc" : "" ?>">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/companyp.png" class="companyBullet">
	                            <h2><?php echo $subarr['name'] ?></h2>
                                <div class="row">
                                    <div class="col-md-8">
	                                    <p class="companyPP"><?php echo $subarr['about_company'] ?></p>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="companyLogo <?php if ($subarr['company_link']!=""){echo 'link';}?>">
                                            <?php if ($subarr['company_link']!=""): ?>
                                            <a href="<?php echo $subarr['company_link'] ?>" target="_blank">
                                                <img src="<?php echo $subarr['company_logo']['url'] ?>">
                                            </a>
                                            <?php else: ?>
                                                <img src="<?php echo $subarr['company_logo']['url'] ?>">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<?php $counter ++; endforeach; ?>
						<?php endif; ?>
                    </div>

                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/companyrec.svg" class="companyrec2">
                </div>
            </div>
        <?php endif; ?>
        
        <?php if ( get_row_layout() === 'iconsBlock' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects landlords">
                <div class="container">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedR" style="width: 100px;">

                    <div class="projectHeading pmservices<?php if(get_sub_field( 'hs' )=='h3'){echo ' left';}?>">
                        <?php if(get_sub_field( 'hs' )!='h3'){?><div class="middleSlash"></div><?php } ?>
                        <<?=get_sub_field( 'hs' );?>><?php echo get_sub_field( 'header' ) ?></<?=get_sub_field( 'hs' );?>>
                        <div class="middleSlash"></div>
                    </div>

                    <?php if ( !empty( get_sub_field( 'desc' ))) : ?>
                    <div class="pmBanB fullWidth left"><?=get_sub_field( 'desc' )?></div>
                    <?php endif; ?>

					<?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                        <div class="row abtRow">
							<?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="<?=get_sub_field('style_cards');?>">
                                    <div>
                                    <?php if ( !empty($subarr['link'])) : ?>
                                    <a href="<?=$subarr['link']?>">
                                    <?php endif; ?>
                                    <div class="abtRowTCont">
                                        <div class="abtRowTL">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg" width="50">
                                        </div>
                                        <div class="abtRowTR">
                                            <p><?php echo $subarr['header'] ?></p>
                                        </div>
                                    </div>
                                    <p class="abtRowBCont"><?php echo $subarr['description'] ?></p>
                                    <?php if ( !empty($subarr['link'])) : ?>
                                    </a>
                                    <?php endif; ?>
                                    </div>
                                </div>
							<?php endforeach; ?>
                        </div>
					<?php endif; ?>

					<?php if ( get_sub_field( 'button_text' ) ) : ?>
                        <div class="discoverCont">
                            <a href="#" data-toggle="modal" data-target="#consultationModal"><?php echo get_sub_field( 'button_text' ); ?></a>
                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

        <?php if ( get_row_layout() === 'slider' ) : ?>
            <div class="featureProjects sliderFW">
                <div class="projectHeading">
                    <div class="middleSlash"></div>
                    <h2><?php echo get_sub_field( 'header' ) ?></h2>
                    <div class="middleSlash"></div>
                </div>
                <div class="gallerySlider grid owl-carousel">
                    <?php
                    $gallery = get_sub_field( 'gallery' );
                    foreach ($gallery as $item): ?>
                        <div class="gallerySlider__item">
                            <a href="<?=$item['url']?>" data-fancybox="images"><img src="<?=$item['url']?>"></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer();