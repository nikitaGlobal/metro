<?php
/**
 * The template for displaying all single posts
 *
 *
 * @package framework
 */
/*
Template Name: Sample Page Template
Template Post Type: properties
*/

get_header();
?>

<div class="property-single-container featureProjects">
    
    <!-- PAGE TEMPLATE INTACT -->
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
            //get_template_part( 'template-parts/content' );
            echo do_shortcode('[INSERT_ELEMENTOR id="28943"]');
        endwhile;
    endif;
    ?>
</div>
				


<?php get_footer();
