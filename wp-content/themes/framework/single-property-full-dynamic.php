<?php
/**
 * The template for displaying all single posts
 *
 *
 * @package framework
 */
/*
Template Name: Listing Template HTML Full Dynamic
Template Post Type: properties, page
*/

get_header();

if (strpos($_SERVER['REQUEST_URI'], "ar") !== false){
?>
<!--AR STYLING-->
<style>
    body{
    direction:rtl;
}

.x-unit-title{
    text-align:right;
}

.x-init-info-entry{
    text-align:right;
    padding-left: 0px!important;
    padding-right: 60px;
}

.x-init-info-entry:before{
    left:initial!important;
    right:0;
}

.x-desc,
.x-indepth-label{
    text-align:right;
}

.x-unit-facilities-listing{
    direction:rtl;
    text-align:right;
}

.x-unit-facilities-listing li:before{
    left:initial!important;
    right:0;
}

.x-unit-facilities-listing li{
    padding-right:40px;
    padding-left:initial!important;
}

.x-location-landmarks{
    text-align:right;
}

.x-contact-container{
 text-align:right;
}

.x-contact-basic li {
    position: relative;
    padding-right: 60px;
    padding-left:initial!important;
}

.x-contract-entry-icon {
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    width: 40px;
    height: 40px;
    background: #16a2e4;
    padding: 10px;
}

.x-contract-entry-icon {
    left:initial;
    right:0;
}

.x-unit-tbl-info{
    text-align:right;
}


</style>
<!--AR STYLING END -->
<?php
//AR Page End
}
?>

<section class="x-main-wrap featureProjects">
    
    <!-- INQUIRY MODAL -->
    <section class="inquiry-overlay"></section>
    <section class="inquiry-wrap">
        <span class="inquiry-modal-heading"><?php echo do_shortcode( '[acf field="send_us_an_inquiry_label"]' ); ?></span>
        <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?>
    </section>
    <!-- END OF INQUIRY MODAL-->

    <div class="x-banner-section">
        <div class="x-slider">

            <div class="owl-carousel">
              <?php echo do_shortcode( '[acf field="hero_banner_images"]' ); ?>
            </div>

        </div>
        <div class="x-unit-intro">
            <div class="x-unit-intro-content">
                <h1 class="x-unit-title"><?php echo do_shortcode( '[acf field="prop_title"]' ); ?></h1>
            </div>
        </div>
    </div>

    <div class="x-init-info-container">
        <div class="x-init-info">
            <div>
                <div class="x-init-info-entry x-init-property-type">
                    <span class="x-init-info-label"><?php echo do_shortcode( '[acf field="property_type_label"]' ); ?></span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="property_type"]' ); ?></span>
                </div>

                <div class="x-init-info-entry x-init-community">
                    <span class="x-init-info-label"><?php echo do_shortcode( '[acf field="community_label"]' ); ?></span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="community"]' ); ?></span>
                </div>
            </div>

            <div>
                <div class="x-init-info-entry x-init-area">
                    <span class="x-init-info-label"><?php echo do_shortcode( '[acf field="area_label"]' ); ?></span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="unit_builtup_area"]' ); ?> Sq.ft</span>
                </div>

                <div class="x-init-info-entry x-init-bedroom">
                    <span class="x-init-info-label"><?php echo do_shortcode( '[acf field="bedroom_label"]' ); ?></span>
                    <span class="x-init-info-content"><?php $iopo =  do_shortcode( '[acf field="bedrooms"]'); echo str_replace("ST","Studio", $iopo);?></span>
                </div>
            </div>

            <div>
                <div class="x-init-info-entry x-init-bathroom">
                    <span class="x-init-info-label"><?php echo do_shortcode( '[acf field="bathroom_label"]' ); ?></span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="no_of_bathroom"]' ); ?></span>
                </div>

                <div class="x-init-info-entry x-init-primary-view">
                    <span class="x-init-info-label"><?php echo do_shortcode( '[acf field="property_name_label"]' ); ?></span>
                    <span class="x-init-info-content"><?php echo do_shortcode( '[acf field="property_name"]' ); ?></span>
                </div>
            </div>

            
            <div>
                <span class="x-unit-price"><?php echo number_format(do_shortcode( '[acf field="price"]')) ; ?></span>
                <button class="x-inquire"><?php echo do_shortcode( '[acf field="inquire_now_label"]' ); ?></button>
            </div>
        </div>
    </div>

    <div class="x-unit-details">
        <div class="x-desc">
        
            <h2 class="x-property-name"><?php echo do_shortcode( '[acf field="property_name"]' ); ?></h2>
            <p>
               <?php echo do_shortcode( '[acf field="property_writeup"]' ); ?>
            </p>
        </div>

        <div class="x-unit-gallery">

            <div class="owl-carousel">
              <?php echo do_shortcode( '[acf field="unit_gallery"]' ); ?>
            </div>
            
        </div>
    </div>

    <div class="x-indepth-details">
        <ul class="tab-options">

            <li>
                <a class="x-indepth-label" href="#"><?php echo do_shortcode( '[acf field="project_details_label"]' ); ?></a>
                <div class="x-unit-details-content">
                    <table class="x-unit-tbl-info">
                        <tbody>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="unit_reference_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="unit_reference"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="parking_slots_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="parking"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="property_name_detail_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="property_name"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="permit_number_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="permit_number"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="emirate_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="emirate"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="completition_status_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="completion_status"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="latitude_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="lat"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="longitude_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="lng"]' ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </li>


            <li>
                <a class="x-indepth-label" href="#"><?php echo do_shortcode( '[acf field="agent_information_label"]' ); ?></a>
                <div class="x-unit-details-content">
                    <table class="x-unit-tbl-info">
                        <tbody>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="company_name_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="company_name"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="last_updated_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="last_updated"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_date_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_date"]' ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </li>
        </ul>
    </div>


    <div class="x-unit-facilities">
        <div class="x-unit-facilities-content">
            <h2><?php echo do_shortcode( '[acf field="features_and_amenities_label"]' ); ?></h2>
            <span class="x-unit-facilities-subtitle"><?php echo do_shortcode( '[acf field="everything_you_need_label"]' ); ?></span>

            <ul class="x-unit-facilities-listing">
                <?php 
                if (ICL_LANGUAGE_CODE=='ar'){
                $facilities_or = do_shortcode( '[acf field="x-facilities"]' );
                
                $facility_en = array("24 hours Maintenance", "Balcony", "Bank/ATM Facility","Basement parking","Basketball Court","BBQ area","Beach Access","Broadband ready","Built in wardrobes","Bus services","Business Center","Carpets","Central air conditioning","Central heating","Children's nursery","Children's play area","Clubhouse","Communal gardens","Community View","Concierge service","Covered parking","Cycling tracks","Driver's Room","Fitness Center","Fully fitted kitchen","Fully furnished","Gazebo and outdoor entertaining area","Golf club and clubhouse","Gymnasium","Intercom","Jacuzzi","Kitchen white goods","Landscaped Garden","Laundry Service","Laundry/washing room","Maid's room","Marble floors","Marina Berth","Metro station"," Mosque","On high floor","On low floor","On mid floor","Part furnished","Pets allowed","Polo club and clubhouse"," Private garage","Private garden","Private swimming pool","<li>Public park</li>","Public parking","Public transport","Recreational Facilities","Restaurants","Satellite/Cable TV","Sauna","School","Shared swimming pool","Shopping mall","Shops","Solid wood floors","Sports academies","Squash courts","Steam room","Storage room","Study","Tennis courts","Upgraded interior","Valet Service","View of gardens","View of golf course","View of parkland","View of sea/water","Walking Trails");
                $facility_ar = array("صيانة 24 ساعة", "شرفة", "/بنك / صراف آلي","وقوف السيارات في الطابق السفلي","ملعب كرة السلة","منطقة للشواء","الوصول إلى الشاطئ","جاهز للإنترنت","خزائن مدمجة","خدمات الحافلات","مركز أعمال","سجاد","تكييف الهواء المركزي","تدفئة مركزية","حضانة الأطفال","منطقة لعب الاطفال","كلوب هاوس","الحدائق العامة","اطلالة على المجمع","خدمة الكونسيرج","مواقف مغطاة للسيارات","مسارات ركوب الدراجات","غرفة السائق","مركز اللياقة البدنية","مطبخ مجهز بالكامل","مفروشة بالكامل","جازيبو ومنطقة ترفيهية في الهواء الطلق","نادي الجولف","صالة رياضية","انتركم","جاكوزي","المطبخ بسلع بيضاء ","حديقة معدة","خدمة غسيل الملابس","غرفة الغسيل","غرفة للخادمة","أرضيات من الرخام","مرسى مارينا","محطة مترو"," مسجد","في الطابق العلوي","في الطابق المنخفض","في الطابق الأوسط","مؤثثة جزئيا","مسموح بدخول الحيوانات الأليفة","نادي بولو","مرأب خاص","حديقة خاصة","مسبح خاص","<li>حديقه عامه</li>","مواقف السيارات العامة","النقل العام","المرافق الترفيهية","مطاعم","الأقمار الصناعية / تلفزيون الكابل","ساونا","مدرسة","مسبح مشترك","مركز تسوق","محلات","الأرضيات الخشبية الصلبة","الأكاديميات الرياضية","ملاعب اسكواش","غرفة البخار","غرفة تخزين","غرفة الدراسة","ملاعب التنس","تصميم داخلي مطور","خدمة صف السيارات","إطلالة على حدائق","إطلالة على ملعب الجولف","إطلالة على المنتزه","إطلالة على البحر / الماء","مسارات المشي");

                echo str_replace($facility_en,$facility_ar,$facilities_or); 
                }
                else {
                echo do_shortcode( '[acf field="x-facilities"]' ); 
                }
                ?>
            </ul>
        </div>
    </div>

    <div class="x-location-landmark">
        <div class="x-location-gmaps">
            
             <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="650" id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo do_shortcode( '[acf field="lat"]' ); ?>,<?php echo do_shortcode( '[acf field="lng"]' ); ?>&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:650px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:650px;width:100%;}</style></div>
            
        </div>
        <!--
        <div class="x-location-landmarks">
            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>
        </div>-->
    </div>

    <div class="x-contact-section">
        <div class="x-contact-section-content">
            <div class="x-contact-section-title">
                <span class="x-contact-section-subtitle"><?php echo do_shortcode( '[acf field="ready_for_your_new_home_label"]' ); ?></span>
                <h2 class="x-contact-section-heading"><?php echo do_shortcode( '[acf field="send_us_an_inquiry_label"]' ); ?></h2>
            </div>

            <div class="x-contact-container">
                <div class="x-contact-basic">
                    <span class="x-contact-pre"><?php echo do_shortcode( '[acf field="contact_us_label"]' ); ?></span>
                        <!--<h3><?php echo do_shortcode( '[acf field="lets_get_in_touch_label"]' ); ?></h3>-->
                        <h3><?php if(ICL_LANGUAGE_CODE=='ar') { the_field('company_name_ar', 'option'); } else { the_field('company_name', 'option') ;} ?></h3>

                    <ul>
                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/envelope.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="email_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php the_field('email', 'option') ?></span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/smartphone.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="call_us_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php the_field('phone', 'option'); ?></span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/placeholder.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="address_label"]' ); ?></span>
                            <span class="x-contact-entry-label"><?php if(ICL_LANGUAGE_CODE=='ar') { the_field('address_ar', 'option'); } else { the_field('address', 'option') ;} ?></span>
                        </li>
                    </ul>
                </div>

              <div class="x-contact-form-container">
                    <!--   <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?> -->
                    <?php if(ICL_LANGUAGE_CODE=='ar') { echo do_shortcode( '[contact-form-7 id="33076" title="Unit Inquiry Form AR"]' ); } else {echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]"]' );}?>
                </div>
            </div>
        </div>
    </div>

</section>






<?php get_footer();
