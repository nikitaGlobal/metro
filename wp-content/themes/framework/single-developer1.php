<?php
    get_header();
?>


    <!-- PAGE TEMPLATE INTACT -->
<?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            ?>

            <section class="p2-f-screen"
                     style="background-image:<?php
                         $image = get_field('cover_image');
                         if ($image) {
                             echo 'url(' . $image . ')';
                         } else {
                             echo 'none';
                         }
                     ?>!important;">
                <div class="container d-flex h-100 align-items-end">
                    <div class="p2-f-screen__text col-12">
                        <div class="d-flex align-items-lg-center align-items-start justify-content-start flex-column flex-lg-row">
                            <div class="d-flex align-items-start align-items-lg-center flex-column flex-lg-row">
                                <img class="mw-100 mr-5"
                                     src="<?php the_post_thumbnail_url('devlogo'); ?>"
                                     alt="" style="background-color: #fff;">
                                <h1 class="offplan-property-title">
                                    <?php the_title(); ?>
                                </h1>
                            </div>
                            <div class="ml-0 ml-lg-auto  mr-auto mr-lg-5">
                                <button>
                                    KNOW MORE
                                </button>
                            </div>
                        </div>
                        
                        <?php if (have_rows('payment_plan')) { ?>
                            <div class="offplan-special-section"
                                 style="margin-top:50px;">
                                <?php while (have_rows('payment_plan')) {
                                    the_row(); ?>
                                    <div class="offplan-special-ent">
                                        <div>
				        <span class="offplan-special-heading"><?php
                                echo the_sub_field('big_numbers');
                            ?></span>
                                            <span class="offplan-special-text"><?php ?><?php
                                                    echo the_sub_field('description')
                                                ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <section class="p2-adout">

                <div class="container">
                    <div class="p2-adout__wrapper flex-column flex-lg-row d-flex align-items-start justify-content-between">

                        <div class="p2-adout__text col-12 col-lg-6">
                            <h2 style="font-weight:700;">
                                <?php the_title(); ?>
                            </h2>
                            <?php the_content(); ?>
                        </div>
                        <div class="col-12 col-lg-5 p2-adout__video p-0">
                            <?php echo the_field('video'); ?>
                        </div>
                    </div>
                </div>
            </section>
            <section class="offplan-related-content-main-wrap">
                <div class="offplan-related-content-sub-wrap">
                    <h3>Areas with Properties from <?php the_title(); ?></h3>
                </div>
                <?php $developerID   = get_the_id();
                    $args            = array(
                        'post_type'      => 'properties',
                        'posts_per_page' => 999,
                        //     'post__not_in'   => array($currentProperty),
                        'meta_query'     => array(
                            array(
                                'key'     => 'of_property_developer',
                                'value'   => $developerID,
                                'compare' => 'IN',
                                //'type'    => 'NUMERIC'
                            )
                        )
                    );
                    $propertiesQuery = new WP_QUERY($args);
                    if ($propertiesQuery->have_posts()) { ?>
                        <div class="featureProjects">
                        <div class="container" style="position: relative">
                            <div class="propertySlider owl-carousel">
                                <?php
                                    while ($propertiesQuery->have_posts()) {
                                        $propertiesQuery->the_post();
                                        get_template_part('preview-related-property');
                                    } ?>
                            </div>
                        </div>
                        </div><?php
                    }
                    wp_reset_postdata();
                    wp_reset_query();
                
                ?>
            </section>
            <?php
            $args1              = $args;
            $args1['tax_query'] = array(
                array(
                    'taxonomy' => 'plan_types',
                    'field'    => 'slug',
                    'terms'    => array('off_plan_properties')
                )
            );
            $areas              = get_transient('areas' . $developerID);
            $types              = get_transient('types' . $developerID);
            if ( ! $areas || ! $types) {
                $query = new WP_Query($args);
                while ($query->have_posts()) {
                    $query->the_post();
                    $terms = wp_get_post_terms(get_the_id(), 'property_categ');
                    foreach ($terms as $term) {
                        $types[$term->slug] = $term->slug;
                        $b1                 = 1;
                    }
                    $areaid = get_field('of_associated_area')->ID;
                    if ($areaid > 0) {
                        $areas[$areaid] = get_field('of_associated_area')->post_title;
                    }
                    $keys = get_fields();
                    $b    = 1;
                }
                set_transient('areas' . $developerID, $areas, 600);
                set_transient('types' . $developerID, $types, 600);
                wp_reset_postdata();
                wp_reset_query();
            }
            $tpl = locate_template('single-developer-areas.php');
            include($tpl);
            ?>
            <section class="offplan-related-content-main-wrap">
            <div class="offplan-related-content-sub-wrap">
                <h3>Off-plan projects from <?php the_title(); ?></h3>
            </div>
            <?php
            
            
            $types = array();
            $types = get_transient('types' . $developerID);
            if ( ! $types) {
                $propertiesQuery = new WP_QUERY($args);
                if ($propertiesQuery->have_posts()) {
                    
                    while ($propertiesQuery->have_posts()) {
                        $propertiesQuery->the_post();
                        $terms = wp_get_post_terms(get_the_id(), 'property_categ');
                        foreach ($terms as $term) {
                            $types[$term->slug] = $term->slug;
                            $b1                 = 1;
                        }
                    }
                    set_transient('types' . $developerID, $types, 600);
                    wp_reset_postdata();
                    wp_reset_query();
                }
            }
            foreach ($types as $type) {
                //$types[$type] = $type;
                ?>
                <div class="featureProjects">
                <div class="container" style="position: relative">
                    <div class="propertySlider owl-carousel">
                        <?php
                            while ($propertiesQuery->have_posts()) {
                                $propertiesQuery->the_post();
                                get_template_part('preview-related-property');
                            } ?> </div>
                </div>
                </div><?php
                
            }
        }
    }
?>
    </section>
<?php
    get_footer();