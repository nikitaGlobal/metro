<?php

/**

* The template for displaying all single posts

*

*

* @package framework

*/

/*

Template Name: Off-Plan Template SEAN

Template Post Type: properties

*/



get_header();



if (ICL_LANGUAGE_CODE=='ar'){

    ?>





    <?php

//AR Page End

}

?>



<section class="offplan-template featureProjects">
    <main class="offplan-template-wrap">
        <section class="offplan-banner-main-wrap">
            <div class="offplan-banner">

                <?php
                    $rows = get_field('of_property_pictures'); // get all the rows
                    /*FIRST IMAGE*/
                    $first_row = $rows[0]; // get the first row
                    $first_row_image = $first_row['of_property_picture' ]; // get the sub field value 
                    $first_image = $first_row_image['url'];
                    /*FIRST IMAGE*/
                ?>

                <img src="<?php echo $first_image; ?>" />
            </div>

            <!-- <a data-fancybox="gallery" href="downloaded/bg.jpg"><img src="small_1.jpg"></a> -->

    <div class="offplan-dets">

        <h1 class="offplan-property-title"><?php the_title(); ?></h1>
        <p class="offplan-property-desc">
            <?php echo get_field('of_headline_description'); ?>
        </p>

        <div class="offplan-sides">
            <div>
                <a href="#" data-toggle="modal" data-target="#consultationModal" class="offplan-call-btn">Get Offer</a>
                <a class="offplan-inquiry-btn" href="tel:+971586488888">Download Brochure</a>
            </div>
        </div>

        <a data-fancybox="offplan-gallery" class="offplan-view-fullscreen" href="/uploads/2020/05/yas_banner_01.jpg">
            <img class="offplan-gallery-icon" src="/wp-content/themes/framework/assets/icons/expand.svg">    
            View Gallery in Fullscreen
        </a>

        <?php
            $rows = get_field('of_property_pictures');
            $skip=true;

            foreach ($rows as $row) {
                if ($skip) {
                    $skip=false;
                    continue;
                }
                ?><a style="display:none;" data-fancybox="offplan-gallery" class="offplan-view-fullscreen" href="<?php echo $row['of_property_picture']['url']; ?>"></a><?php
            }
        ?>
        <?php if( have_rows('property_features') ) { ?>
            <div class="offplan-special-section">
                <?php
                while ( have_rows('property_features') ) {
                    the_row();
                    ?>
                    <div class="offplan-special-ent">
                        <div>
                            <span class="offplan-special-heading"><?php
                            echo the_sub_field('big_numbers');
                            ?></span>
                            <span class="offplan-special-text"><?php ?><?php
                            echo the_sub_field('description')
                            ?></span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div><!--offplan dets-->
</section>

<section class="offplan-desc-main-wrap">
    <div class="offplan-desc-sub-wrap">
        <div class="offplan-desc">
            <h2><?php echo get_field('of_property_subtitle');?></h2>
            <p>
                <?php echo get_field('of_property_description'); ?>
            </p>
        </div>

        <div class="offplan-desc-img">
            <div class="offplan-display-img">
                <?php
                    /*SECOND IMAGE*/
                    $second_row = $rows[1]; // get the first row
                    $second_row_image = $second_row['of_property_picture' ]; // get the sub field value 
                    $second_image = $second_row_image['url'];
                    /*SECOND IMAGE*/
                ?>
                <img class="offplan-display-picture" src="<?php echo $second_image; ?>" />
                <img class="offplan-dotted" src="<?php echo get_template_directory_uri(); ?>/assets/images/dotted_atoms.svg"/>
            </div>
        </div>
    </div>

    <div class="offplan-video-wrap">
        <?php echo get_field('video');?>
    </div>
</section>

<section class="offplan-facilities-main-wrap">
    <div class="offplan-facilities-sub-wrap">
        <div class="offplan-facility-display-img">
            <?php
            /*THIRD IMAGE*/
            $third_row = $rows[2]; // get the second row
            $third_row_image = $third_row['of_property_picture' ]; // get the sub field value 
            $third_image = $third_row_image['url']; //get the image array url
            /*THIRD IMAGE*/
            ?>
            <img src="<?php echo $third_image; ?>" />
        </div>

        <div class="offplan-facilities">
            <h3>Property Facilities</h3>
            <ul>
                <?php //echo get_field('x-facilities');?>
                <?php
                $post_objects = get_field('asoc_fac');
                if( $post_objects ): ?>
                    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                        <?php setup_postdata($post); ?>
                        <li>
                            <div class="offplan-facilities-icon-con"><img src=" <?php the_post_thumbnail_url(); ?> "></div>
                            <?php the_title(); ?>
                        </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>

<section class="offplan-floorplan-main-wrap">
    <div class="offplan-floorplan-sub-wrap">
        <div>
            <h3>
                Floor Plan
                <img class="offplan-floorplan-dotted" src="<?php echo get_template_directory_uri(); ?>/assets/images/dotted_atoms.svg"/>
            </h3>
        </div>

        <?php 
            $plans=get_field('floor_plan');

            if ($plans && is_array($plans) && !empty($plans)) {
            ?>
            <div>    
                <div class="offplan-floorplan-details-wrap">
                    <div class="offplan-floorplan-tabs">
                        <ul>
                            <?php
                            $active=true;
                            foreach ($plans as $key=>$plan){
                                ?><li trigger="plantab<?php echo $key;?>" <?php
                                if ($active){?> class="active"<?php 
                                    $active=false; 
                            }
                            ?>><a  href="javascript:void(0);"><?php echo $plan['floor_plan_tab'];?></a></li><?php

                        } if (2==1) {?>
                            <?php if(get_field('floor_plan')): ?>
                                <?php while(has_sub_field('floor_plan')): ?>
                                    <li><?php the_sub_field('floor_plan_tab');?></li>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        <?php } ?>
                    </ul>
                </div>

                <?php
                $active=true;
                foreach ($plans as $key=>$plan) {?>
                    <div class="offplan-floorplan-entry<?php
                    if ($active){?> active<?php 
                        $active=false; 
                    }
                    ?>" id="plantab<?php echo $key;?>">

                    <div class="offplan-floorplan-details">
                        <h4><?php echo $plan['floor_plan_tab'];?></h4>
                        <span class="offplan-total-area-label"><?php echo $plan['floor_plan_sub_title'];?></span>
                        <span class="offplan-total-area-value"><?php echo $plan['area_in_square_ft'];?></span>
                        <span class="offplan-total-area-value"><?php echo $plan['area_in_square_meters'];?></span>
                        <a href="" class="offplan-floorplan-dl-btn"><?php echo $plan['download_button_label'];?></a>
                    </div>

                    <div>
                        <img class="downladed" src="<?php echo $plan['floor_plan_image']['url']; ?>"/>  
                    </div>

                </div>

            <?php } ?>

            <?php if(22==11 && get_field('floor_plan')): ?>
                <?php while(has_sub_field('floor_plan')): ?>
                    <?php $image = get_sub_field('floor_plan_image'); ?>

                    <div class="offplan-floorplan-entry">
                        <div class="offplan-floorplan-details">
                            <h4><?php echo $plan['floor_plan_tab'];?></h4>
                            <span class="offplan-total-area-label"><?php echo $plan['floor_plan_sub_title'];?></span>
                            <span class="offplan-total-area-value"><?php the_sub_field('area_in_square_ft');?></span>
                            <span class="offplan-total-area-value"><?php the_sub_field('area_in_square_meters');?></span>
                            <a href="" class="offplan-floorplan-dl-btn"><?php the_sub_field('download_button_label');?></a>
                        </div>

                        <div>
                            <img class="downladed" src="<?php echo $image['url']; ?>"/>  
                        </div>
                    </div>



                <?php endwhile; ?>
            <?php endif; ?>
        <?php } ?>                        
    </div>
</div>
</div>
</section>

<div class="x-unit-details">
    <div class="x-unit-gallery">
        <div class="owl-carousel">
            <?php echo do_shortcode( '[acf field="unit_gallery"]' ); ?>
        </div>
    </div>
</div>

<?php if (have_rows('payment_plans')) {?>
    <section class="offplan-payment-plan-main-wrap">
        <div class="offplan-payment-plan-sub-wrap">
            <h3>Payment Plan</h3>
            <div class="offplan-payment-plans">
                <?php while (have_rows('payment_plans')) {
                    the_row();
                    ?>
                    <div class="offplan-payment-entry">
                        <div class="offplan-payment-box">
                            <span class="offplan-installment"><?php the_sub_field('type_of'); ?></span>
                            <span class="offplan-percentage"><?php the_sub_field('percent'); ?></span>
                            <span class="offplan-installment-info"><?php the_sub_field('note'); ?></span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php $developer=get_field('associated_developer');
if ($developer){
    ?>
    <section class="offplan-developer-main-wrap">
        <div class="offplan-developer-sub-wrap">
            <div><?php $bgImage= wp_get_attachment_image_src( get_post_thumbnail_id( $developer ), 'original' );
            if (isset($bgImage[0])){?>
                <img class="ofp-dv-logo" src="<?php
                echo $bgImage[0];
                ?>"><?php } ?></div>
                <div>
                    <h4 class="ofp-dv-title"><?php echo $developer->post_title;?></h4>
                    <p class="ofp-dv-desv">
                        <?php echo $developer->post_content;?>
                    </p>

                    <a class="ofp-dv-link-btn" href="<?php echo get_post_permalink($developer);?>">
                        Read More
                    </a>
                </div>
            </div>
        </section>
    <?php } ?>

    <section style="display:none;" class="offplan-map-section">
        <div>
            <div id="googleMap"></div>
            <script type="text/javascript">
                function initMapPage() {
                    var mapOptions = {
                        zoom: 15,

                        center: new google.maps.LatLng(<?=get_field("lat")?>, <?=get_field("lng")?>),

                        styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#02b2ee"},{"visibility":"on"}]}]

                    };



                    var mapElement = document.getElementById('googleMap');

                    var map = new google.maps.Map(mapElement, mapOptions);

                    var marker = new google.maps.Marker({

                        position: new google.maps.LatLng(<?=get_field("lat")?>, <?=get_field("lng")?>),

                        map: map,

                        title: "<?=get_field("prop_title") ?>"

                    });

                }
            </script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRTYB_ke75HeFjs_B5uhGEVUobs1KUmDo&callback=initMapPage"></script>
        </div>
    </section>



    <section class="offplan-contact-main-wrap">
        <div class="offplan-contact-sub-wrap">
            <div class="offplan-contact-form">
                <h4>Get Special Offer</h4>
                <?php
                    
                    $keys=get_fields();
                    $b1=1;
                ?>
                <span>We will call you back within one hour and your addtional information on email</span>
                <div class="offplan-contact-form footerForm">
                    <?php echo do_shortcode('[cf7form cf7key="off-plan-inquiry-form-en"]')?>
                </div>
            </div>

            <div>
                <img src="<?php echo $rows[3]['image']['url'];?>">
            </div>
        </div>
    </section>



    <?php $location=get_field('associated_location');?>

    <section class="offplan-location-wrap" style="background:url('<?php

        $bgImage= wp_get_attachment_image_src( get_post_thumbnail_id( $location ), 'original' );

        echo $bgImage[0];

        ?>');">



        ?>

        <div class="offplan-location-sub-wrap">

            <h3><?php echo $location->post_title;?></h3>

            <p>

                <?php echo $location->post_content;?>

            </p>

            <a href="<?php echo get_post_permalink($location);?>">Learn More</a>

        </div>

    </section>

    <?php

    get_template_part('single-property-related');

    get_template_part('single-property-samedeveloper');

    ?>



</main>

</section>

<!-- Modal -->
    <div id="consultationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modalH"><?php echo get_field( 'footer_header' ) ?></p>
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">

                        <?php echo do_shortcode('[contact-form-7 id="30467"]'); ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
<?php get_footer();

