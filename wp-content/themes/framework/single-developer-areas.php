<?php
    $argsAreas       = array(
        'post_type'      => 'area',
        'posts_per_page' => 999,
        'posts__in'      => array_keys($areas)
    );
    $propertiesQuery = new WP_QUERY($argsAreas);
?>
<section class="offplan-related-content-main-wrap">
    <div class="offplan-related-content-sub-wrap">
        <h3>Areas with Properties from <?php the_title(); ?></h3>
    </div>
    <div class="featureProjects">
        <div class="container" style="position: relative">
            <div class="propertySlider owl-carousel">
                <?php
           
                    while ($propertiesQuery->have_posts()) {
                        $propertiesQuery->the_post();
                        get_template_part('preview-area');
                    } ?>
            </div>
        </div>
    </div>
</section>
<?php
    wp_reset_postdata();
    wp_reset_query();
    
    return;
