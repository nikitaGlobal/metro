<?php
/**
 * The front page our site
 *
 * @package framework
 */

get_header();

$background         = '';
$homebg             = get_field( 'homebg' );
$videobg            = get_field( 'videobg' );
$search_sorting     = get_field( 'search_sorting', 'option' );
//$min_bedroom_option = $search_sorting['min_bedroom'];
//$max_bedroom_option = $search_sorting['max_bedroom'];
//$price              = $search_sorting['price'];

if ( $homebg['images'] && $homebg['show_block'] ) {
	$background = 'style="background-image: url(' . $homebg['images']['url'] . ');"';
} elseif ( ! $background && ! $videobg['show_block'] && $videobg['video_url'] ) {
	$background = 'style="background-image: url(/wp-content/themes/framework/assets/images/banner.png);"';
}
/*
foreach ( $price as $item ):
	$optionPrice .= '<option value="' . str_replace( ' ', '',
			$item['item_price'] ) . '">' . $item['item_price'] . '</option>';
endforeach;

for ( $i = 1; $i <= $max_bedroom_option; $i ++ ):
	$optionBedroom .= '<option value="' . $i . '">' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
endfor;
*/
?>

    <div class="banner " <?php echo $background; ?> >

		<?php if ( ! $homebg['show_block'] && $videobg['show_block'] && $videobg['video_url'] ) : ?>
            <div class="home-video-content">
                <video id="home_video" preload="metadata" playsinline muted loop style="position:absolute; width:100%; height:100vh; top:0;">
                    <source src="<?php echo $videobg['video_mp4_url']; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                    <source src="<?php echo $videobg['video_url']; ?>" type='video/webm; codecs="vp8, vorbis"'>
                </video>
            </div>
		<?php endif; ?>


        <div class="bannerText">
            <h1 class="bannerHeading"><?php the_field( 'title' ); ?></h1>
            <p class="bannerSubHeading"><?php the_field( 'subtitle' ) ?></p>
        </div>

    </div>


<?php /*$query = new WP_Query( array(
	'post_type'      => 'estate_object',
	'posts_per_page' => 6,
) );

if ( $query->have_posts() ): ?>
    <div class="featureProjects">
        <div class="container">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedL">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedR">

            <div class="projectHeading">
                <div class="middleSlash"></div>
                <h2><?php the_field( 'object_section_title' ) ?></h2>
                <div class="middleSlash"></div>
            </div>

            <div class="row propertiesRow">
                <div id="object-ajax-home" class="col-md-12 projectTopBtn">
					<?php if ( $query_emirate ):
						$count = count( $query_emirate );
						for ( $i = 0; $i < $count; $i ++ ):
							if ( 0 === $i ) {
								echo '<a class="active" data-ajax-id="' . $query_emirate[ $i ]->term_id . '">' . $query_emirate[ $i ]->name . '</a>';
							} else {
								echo '<a data-ajax-id="' . $query_emirate[ $i ]->term_id . '">' . $query_emirate[ $i ]->name . '</a>';
							}
						endfor;
					endif; ?>
                </div>

				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="col-md-4">
						<?php get_template_part( 'template-parts/content', 'object' ); ?>
                    </div>
				<?php endwhile;
				wp_reset_query(); ?>
            </div>

			<?php if ( $query->max_num_pages > 1 ): ?>
                <script>
                  var current_page = 1,
                    max_pages = '<?php echo $query->max_num_pages; ?>';
                </script>

                <div class="viewAll objectViewMore" data-count="<?php echo $query->query['posts_per_page']; ?>">
                    <a><?php echo pll__( 'View more' ); ?>
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/load.svg"></a>
                </div>
			<?php endif; ?>

        </div>
    </div>
<?php endif; */?>

<?php if ( have_rows( 'flex_content' ) ) :
	while ( have_rows( 'flex_content' ) ) :
		the_row();

		if ( get_row_layout() === 'advantages' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects">
                <div class="container">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'title' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="row advRow mt-2">

						<?php if ( ! empty( get_sub_field( 'advantages' ) ) ) :
							foreach ( get_sub_field( 'advantages' ) as $subarr ) : ?>
                                <div class="col-md-3">
                                    <div class="advTxt">
                                        <div class="advImg">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg">
                                        </div>
                                        <h4 class="advHead"><?php echo $subarr['title'] ?></h4>
                                        <p class="advDesc"><?php echo $subarr['description'] ?></p>
                                    </div>
                                </div>
							<?php endforeach;
						endif; ?>

                    </div>
                </div>
            </div>
		<?php endif;

        if ( get_row_layout() === 'offplan' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects fPoffplan">
                <div class="container">
                    <div class="projectHeading left">
                        <h2><?php echo get_sub_field( 'title' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="row abtRow offplan">
                        <?php if ( ! empty( get_sub_field( 'offplans' ) ) ) :
                            foreach ( get_sub_field( 'offplans' ) as $value ) : ?>
                                <div class="col-md-4">
                                    <div class="offplan-cell">
                                        <a class="offplan-link" href="<?php echo $value['link'] ?>" title="<?php echo $value['title'] ?>" target="_blank">
                                            <div class="offplan-image">
                                                <img src="<?php echo $value['img']['sizes']['medium_large'] ?>" alt="<?php echo $value['title'] ?>">
                                                <div class="offplan-flex">
                                                    <div class="offplan-desc">
                                                        <p class="ofpH2 white"><?php echo $value['title'] ?></p>
                                                        <p class="desc"><?php echo $value['description'] ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="offplan-head">
                                                <h2 class="ofpH2"><?php echo $value['title'] ?></h2>
                                                <p class="ofploc"><?php echo $value['loc'] ?></p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach;
                        endif; ?>

                    </div>
                </div>
            </div>
        <?php endif;

		if ( get_row_layout() === 'about' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects fPabout">
                <div class="container">
                    <div class="projectHeading left">
                        <h2><?php echo get_sub_field( 'title' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="aboutBtm">
                        <p><?php echo get_sub_field( 'subtitle' ) ?></p>
                    </div>
                    <div class="row abtRow about">

						<?php if ( ! empty( get_sub_field( 'cards' ) ) ) :
							foreach ( get_sub_field( 'cards' ) as $value ) : ?>
                                <div class="col-md-6">
                                    <div class="abtRow-cell <?php echo $value['bg_image'] ?>">
                                        <div class="abtRow-cell-content">
                                            <h3 class="abtH1"><?php echo $value['title'] ?></h3>
                                            <h4 class="abtH2"><?php echo $value['subtitle'] ?></h4>
                                            <p class="abtDesc"><?php echo $value['desc'] ?></p>
                                        </div>
                                    </div>
                                </div>
							<?php endforeach;
						endif; ?>

                    </div>
                </div>
            </div>
        <?php endif;
        if ( get_row_layout() === 'full-width_block' && get_sub_field( 'show_block' ) ) : ?>
            <div class="full-width_block" style="background-image: url(<?=get_sub_field('bg')?>)">
                <div class="container">
                    <div class="col-md-8 fWbContent">
                        <img src="/wp-content/themes/framework/assets/images/vpat.svg" class="vpatLR">
                        <div class="fwHead projectHeading left">
                            <h2><?=get_sub_field('title' ) ?></h2>
                            <div class="middleSlash"></div>
                        </div>
                        <div class="fwDesc">
                            <p><?=get_sub_field('desc') ?></p>
                        </div>
                        <div class="valBtnCont">
                            <button type="button" data-toggle="modal" data-target="#fwBtnModal" class="bannerValBtn"><?=get_sub_field('btn')?></button>
                        </div>
                    </div>
                </div>
            </div>
        <?php 
        $GLOBALS['fwForm'] = get_sub_field('form');
        endif;

		if ( get_row_layout() === 'awards' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects fPawards">
                <div class="container">
                    <div class="projectHeading left">
                        <h2><?php echo get_sub_field( 'title' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="row awardRow">

						<?php if ( ! empty( get_sub_field( 'cards' ) ) ) :
							foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="col-md-3">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/awardribben.svg">
                                    <div class="awardText">
                                        <div class="awardLogo">
                                            <img src="<?php echo $subarr['award_logo']['url'] ?>" width="75" height="48">
                                        </div>
                                        <div class="awardImg">
                                            <img src="<?php echo $subarr['trophy_image']['url'] ?>" width="110" height="111">
                                        </div>
                                    </div>
                                    <div class="awardTextBottom">
                                        <p class="awardTextP"><?php echo $subarr['title'] ?></p>
                                    </div>
                                </div>
							<?php endforeach;
						endif ?>

                    </div>
                </div>
            </div>
		<?php endif;

		if ( get_row_layout() === 'ceo_message' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects fPceo">
                <div class="container">
                    <div class="ownerMsg">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/coma.svg" class="comaImg">
                        <div class="ownerContainer">
                            <div class="ownerImg">
                                <img src="<?php echo get_sub_field( 'ceo_photo' )['url'] ?>" width="100" data-width="100">
                            </div>
                            <div class="ownerText">
								<?php echo substr( get_sub_field( 'message' ), 0, 280 ); ?>...
                            </div>
                        </div>
                        <div class="ownerContainer2">
                            <div class="ownerslash"></div>
                            <h5><?php echo get_sub_field( 'full_name' ); ?></h5>
                        </div>
                        <div class="ownerContainer3">
                            <p><?php echo get_sub_field( 'position' ) ?></p>
                        </div>
                    </div>
                </div>
            </div>
		<?php endif;

        if ( get_row_layout() === 'news' && get_sub_field( 'show_block' ) ) :
            $cats = get_sub_field( 'cat' );
            $args = array(
                'numberposts' => 3,
                'orderby'     => 'date',
                'order'       => 'DESC',
                'category'    => $cats,
                'suppress_filters' => true,
            );
            $posts = get_posts($args);
            ?>
            <div class="featureProjects news">
                <div class="container">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'title' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="row abtRow newsRow">
                        <?php if ( !empty($posts) ) :
                            foreach ( $posts as $item ) : ?>
                                <div class="col-md-4 newsCol">
                                    <a class="news-link" href="<?php  the_permalink($item->ID) ?>" title="<?php echo $item->post_title ?>">
                                    <div class="news-cell">
                                            <div class="news-image">
                                                <?php echo get_the_post_thumbnail($item->ID) ?>
                                                <div class="news-date"><?php echo date("d.m.Y", strtotime($item->post_date)) ?></div>
                                            </div>
                                            <div class="news-head">
                                                <h2 class="newsH2"><?php echo $item->post_title ?></h2>
                                                <p class="newsDesc"><?php echo get_the_excerpt($item->ID) ?></p>
                                            </div>
                                    </div>
                                    </a>
                                </div>
                            <?php endforeach;
                        endif; ?>

                    </div>
                </div>
            </div>

        <?php wp_reset_postdata(); endif;

		if ( get_row_layout() === 'callback' && get_sub_field( 'show_block' ) ) : ?>
            <div class="contactFtContainer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="contactFtL">
                                <h3 class="contactFtLh"><?php echo get_sub_field( 'tb_title' ) ?></h3>
                                <ul>
									<?php $tb_content = get_sub_field( 'tb_content' );
									foreach ( $tb_content as $item ) : ?>
                                        <li><p><?php echo $item['item']; ?></p></li>
									<?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="contactFtRT">
                                <h3 class="contactFtRTH"><?php echo get_sub_field( 'title' ) ?></h3>
                                <div class="middleSlash"></div>
                            </div>
                            <div class="contactFtRT2">
                                <p><?php echo get_sub_field( 'phone' ) ?></p>
                                <p class="cp2"><?php echo get_sub_field( 'time_work' ) ?></p>
                                <p class="cp2"><?php echo get_sub_field( 'time_work_2' ) ?></p>
                            </div>
                            <div class="contactFtRT3">
                                <a href="<?php echo get_sub_field( 'block_url_1' ) ?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/call2.svg">
									<?php echo get_sub_field( 'block_url_name_1' ) ?>
                                </a>

                                <?php $modal_cont = get_sub_field( 'block_url_2' );?>
                                <a data-toggle="modal" data-target="#frontPageModal">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/msg.svg">
									<?php echo get_sub_field( 'block_url_name_2' ); ?>
                                </a>

                                <a href="<?php echo get_sub_field( 'block_url_3' ) ?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/chat.svg">
									<?php echo get_sub_field( 'block_url_name_3' ) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php endif;

	endwhile;
endif;
?>
<?php if(isset($modal_cont)): ?>
    <!-- Modal -->
    <div id="frontPageModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modalH"><?php echo get_field( 'footer_header' ) ?></p>
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">
						<?php echo do_shortcode(  $modal_cont  ); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>
<?php get_footer();