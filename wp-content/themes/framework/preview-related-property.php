<?php
    $keys=get_fields();
    $propertyCommunity = get_field('community', get_the_ID());
    $propertyName      = get_field('property_name', get_the_ID());
    $propertyEmirate   = get_field('of_emirates', get_the_ID());
    $propertyType      = get_field('of_property_type', get_the_ID());
    $propertyPlan      = get_field('of_property_plan', get_the_ID());
    $propertyBedRoom   = get_field('bedrooms', get_the_ID());
    $propertyBathRoom  = get_field('no_of_bathroom', get_the_ID());
    $propertyArea      = get_field('unit_builtup_area', get_the_ID());
    $propertyPrice     = number_format(get_field('price', get_the_ID()));
    $propertyDesc      = get_field('property_writeup', get_the_ID());
    $planType          = get_the_terms(get_the_ID(), 'plan_types');
    
?>
<div class="propertySlider__item property">
    <a href="<?= get_permalink(); ?>" class="propertyItem-link">
        <div class="propertyImg">
            <img src="<?= get_the_post_thumbnail_url(get_the_ID()) ?>">
            <div class="propertyStickers">
                <span class="propertySticker <?= $planType[0]->slug ?>"><?= $propertyPlan ?></span>
                <span class="propertySticker type"><?= $propertyType ?></span>
            </div>
        </div>
    </a>
    <div class="propertyHead">
        <a href="<?= get_permalink(); ?>" class="propertyItem-link title">
            <div class="propertyHead-name"><?= get_the_title(); ?></div>
        </a>
        <div class="propertyHead-locate"><?= $propertyCommunity ?></div>
        <ul class="propertyHead-icons">
            <li class="propertyHead-icons_item beds"><?= pll__('Beds') ?>
                : <?= $propertyBedRoom ?></li>
            <li class="propertyHead-icons_item baths"><?= pll__('Baths') ?>
                : <?= $propertyBathRoom ?></li>
            <li class="propertyHead-icons_item area"><?= pll__('Sq Ft') ?>
                : <?= $propertyArea ?></li>
        </ul>
        <div class="propertyHead-price">
            AED <?= $propertyPrice ?>
            <a class="listing-whatsapp"
               href="https://api.whatsapp.com/send?phone=<?php the_field('phone',
                   'option'); ?>&amp;text=Hi,%20I%20am%20interterested%20in%20this%20property%20%20%20%20%20<?php echo get_permalink(); ?>"><span
                        class="whapp">WhatsApp</span><span class="i-whatsapp"></span>
            </a>
        </div>
    </div>
</div>