(function ( $, undefined ) {
  $( function () {
    //Global var
    var lng = $('html').attr('lang'),
        rtl = false;
    if (lng === 'ar') {
      rtl = true;
    }
    var img_src = jQuery('#stickyMenu .logoContainer img').attr('src');

    img_src = img_src.substring(img_src.lastIndexOf('/') + 1, img_src.length);

    window.onscroll = function () {
      myFunction();
    };

// Get the header

// Get the offset position of the navbar
    var sticky = document.querySelector('.featureProjects').offsetTop;

// Add the sticky class to the header when you reach its scroll position.
// Remove "sticky" when you leave the scroll position

    function myFunction() {

      if (window.pageYOffset > sticky) {
        $('#stickyMenu').addClass('scroll');
        $('#stickyMenu .logoContainer img').attr('src',
            '/wp-content/themes/framework/assets/images/MCRE.svg');
      } else {
        $('#stickyMenu').removeClass('scroll');
        $('#stickyMenu .logoContainer img').attr('src',
            '/wp-content/themes/framework/assets/images/' + img_src);
      }
    }

    if (navigator.userAgent.match(/(iPad|iPhone|iPod|Android|Silk)/gi) && $('.full-width_block').length) {
      $('.full-width_block').addClass('mobile');
    }
    //Start open/closed menu
    $('.menuOpen').on('click', function () {
      //$( '.mobileMenu' ).show();
      //$( this ).parent().parent().attr( 'id', '' );
      //$( this ).parent().parent().addClass( 'mobileMenu' );
      if ($(this).hasClass('opened')) {
        $(document).find('.mobileMenu').attr('id', 'stickyMenu');
        $(document).find('.mobileMenu').removeClass('mobileMenu');
        document.body.style.overflow = 'visible';
        if (!$(document).find('#stickyMenu').hasClass('scroll')) {
          $('#stickyMenu .logoContainer img').attr('src',
              '/wp-content/themes/framework/assets/images/' + img_src);
        }
      } else {
        $(document).find('#stickyMenu').addClass('mobileMenu');
        $(document).find('#stickyMenu').attr('id', '');
        document.body.style.overflow = 'hidden';
        $('.mobileMenu .logoContainer img').attr('src',
            '/wp-content/themes/framework/assets/images/MCRE.svg');
      }
      $(this).toggleClass('opened');
    });
    //End open/closed menu

    //Start bild search form home
    var searchAction = $('#topSearch form');
    $('.searchCat').on('click', function () {

      let cat = $(this).data('id'),
          slug = $(this).data('slug'),
          data_site = {
            'action': 'ajax_load_community',
            'term_id': cat,
          };

      $(document).find('.searchCat').removeClass('active');
      $(this).addClass('active');
      slug ? searchAction.attr('action', '/emirate/' + slug) : '';
      $('#searchType').val($(this).data('id'));

      $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function (data) {
          $('#topSearch .locationContainer').html(data);
          $('#loc_select').select2({placeholder: 'Location'});
        },
        error: function () {
          console.log('Ajax error !!');
        },
      });

    });

    //Set id in search category
    if ($('.home #topSearch').length) {
      $('#searchType').val($('a.searchCat.active').data('id'));
    }

//---------------------------------------------------------------------------

    //Start add form currency class
    $('select.currency-switcher').addClass('form-control');
    //End add form currency class

    //Start ajax load post agent
    $('.agentsViewMore').on('click', function (e) {
      e.preventDefault();
      var img = $(this).children().find('img');
      var count = $(this).data('count');
      var data_site = {
        'action': 'ajax_load_agents',
        'current_page': current_page,
        'count': count,
      };

      img.addClass('active');

      $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function (data) {
          if (data) {
            current_page++;
            img.removeClass('active');
            $('.row.propertiesRow').append(data);
            if (current_page === Number(max_pages)) {
              $('.viewAll').remove();
            }
          } else {
            $('.viewAll').remove();
          }
        },
        error: function () {
          console.log('Ajax error !!');
          img.removeClass('animation');
        },
      });
    });
    //END ajax load post agent

    //Start ajax load post object home
    $('.objectViewMore').on('click', function (e) {
      e.preventDefault();
      var img = $(this).children().find('img'),
          count = $(this).data('count'),
          data_site = {
            'action': 'ajax_load_object',
            'current_page': current_page,
            'count': count,
          };

      img.addClass('active');

      $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function (data) {
          if (data) {
            current_page++;
            img.removeClass('active');
            $('.row.propertiesRow').append(data);
            if (current_page === Number(max_pages)) {
              $('.viewAll').remove();
            }
          } else {
            $('.viewAll').remove();
          }
        },
        error: function () {
          console.log('Ajax error !!');
          img.removeClass('animation');
        },
      });

    });
    //END ajax load post object home

    //Start deep menu hover
    $('.menu-item-has-children').hover(function () {
      $(this).toggleClass('active');
      $(this).children('.sub-menu').toggleClass('active');
    });//End deep menu hover

    $('.mobileMenu .menu-item-has-children').on('click touchstart', function (e) {
      e.preventDefault();
      $(this).children('.sub-menu').toggleClass('active');
    });

    //Start animation for form label
    $('.form-control').on('blur', function () {
      if (!$(this).val()) {
        $(this).parent().children('label').removeClass('filled');
      }
    });

    $('.form-control').on('focus', function () {
      $(this).parent().children('label').removeClass('filled').addClass('filled');
    });
    //End animation for form label

    //Start archive object map
    $('.showmap').on('click', function () {
      var
          object = $('.estate-object .propertiesRow').parent(),
          block_map = $('.estate-object .mapControls').parent(),
          showmap = $('.showmap');

      // this_text.text() === 'SHOW MAP'
      //   ? this_text.addClass( 'active' )
      //   : this_text.text( 'SHOW MAP' );

      object.toggleClass('col-md-12 col-md-8');
      block_map.toggleClass('d-none');
      showmap.toggleClass('active');

    });
    //End archive object map

    //Start video background home
    var playerEl = document.getElementById('home_video');
    if (playerEl) {
      function vidRescale() {
        var w = window.innerWidth,
            //h = window.innerHeight;
            h = document.querySelector('.banner').clientHeight;

        if (w / h > 16 / 9) {
          playerEl.style.width = w + 'px';
          playerEl.style.height = (w / 16 * 9) + 'px';
          //playerEl.style.left = '0px';
          //banner.style.height = (w / 16 * 9) + 'px';
        } else {
          playerEl.style.width = (h / 9 * 16) + 'px';
          playerEl.style.height = h + 'px';
          playerEl.style.left = (-(playerEl.outerWidth - w) / 2) + 'px';
        }

        playerEl.style.display = 'block';

      }

      function playVideo() {
        playerEl.play();

        vidRescale();
      }

      window.addEventListener('load', playVideo);
      window.addEventListener('resize', vidRescale);
    }
    //End video background home
    /*
    if($('#loc_select').length){
    $( '#loc_select' ).select2( {
        placeholder: 'Location',
      },
    ); //Initialization select2.js
    }*/

    //Start ajax home "Featured Projects"
    $('#object-ajax-home a').on('click touchstart', function () {

      $('#object-ajax-home a').removeClass('active');
      $(this).addClass('active');

      let term_id = $(this).data('ajax-id'),
          data_site = {
            'action': 'object_ajax_home',
            'term_id': term_id,
          };

      $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: data_site,
        success: function (data) {
          $('.propertiesRow .col-md-4').remove();
          $('.propertiesRow').append(data);
        },
        error: function () {
          console.log('Ajax error !!');
        },
      });
    });
    //End ajax home "Featured Projects"

    //Start ajax object sort
    $('.pageInputCont .pageSelectSort option').on('click touchstart', function () {

      Cookies.set('sorting', $(this).val(), {expires: 0.001, path: ''});//Доробити

      if (typeof community != 'undefined' || typeof propertyType !=
          'undefined' || typeof minBedroom != 'undefined' ||
          typeof maxBedroom != 'undefined' || typeof minPrice != 'undefined' ||
          typeof maxPrice != 'undefined') {
        var data_site = {
          'action': 'ajax_object_sort',
          'orderby': $(this).val(),
          'community': community,
          'propertyType': propertyType,
          'minBedroom': minBedroom,
          'maxBedroom': maxBedroom,
          'minPrice': minPrice,
          'maxPrice': maxPrice,
          'paged': paged,
        };

        $.ajax({
          type: 'POST',
          url: '/wp-admin/admin-ajax.php',
          data: data_site,
          success: function (data) {
            $('#object-post .col-md-6').remove();
            $('#object-post').append(data);
          },
          error: function () {
            console.log('Ajax error !!');
          },
        });
      }

    });
    //End ajax object sort

    if (Cookies.get('sorting')) {
      var sort = Cookies.get('sorting'),
          option = $('select.pageSelectSort option');

      option.map(function (indx, element) {
        //var _this = element.attributes.value.nodeValue;
        var _this = $(element);

        _this.prop({'selected': false});

        if (_this.val() === sort) {
          _this.prop({'selected': true});
        }

      });
    }

    $('.contactFtRT3 a[href="jivo_api.open();"]').on('click touchstart', function (e) {
      e.preventDefault();
      jivo_api.open();
    });

    //Start conversation feet for meters
    function areaSelect() {
      var active = Cookies.get('convert-ft'),
          _self = $('.areaSelect .areaSelect-btn');

      if (active) {
        _self.map(function (indx, element) {
          if (active === $(element).data('convert')) {
            _self.removeClass('active');
            $(element).addClass('active');
          }
        });
      }

      _self.on('click touchstart', function () {
        var _this = $(this);

        _self.removeClass('active');
        _this.addClass('active');
        Cookies.set('convert-ft', _this.data('convert'), {expires: 1});
        location.reload();
      });
    }

    //Langauge swicher
    $('.lang-item.current-lang > a').on('click', function (e) {
      e.preventDefault();
      $('.langSwicher').toggleClass('show');
    });
    //End conversation feet for meters
    areaSelect();

    //CF7 add information for hidden input on Service pages
    if ($('input[name="pageTitle"]').length) {
      $('input[name="pageTitle"]').val(document.title);
      $('input[name="siteUrl"]').val(document.URL);
    }
    if ($('.gallerySlider:not(.grid)').length) {
      $('.gallerySlider:not(.grid)').owlCarousel({
        loop: true,
        items: 1,
        nav: true,
        rtl: rtl
      });
    }
    if ($('.gallerySlider.grid').length) {
      $('.gallerySlider.grid').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        items: 3,
        rtl: rtl,
        responsive: {
          0: {
            items: 1
          },
          768: {
            items: 2
          },
          1024: {
            items: 3
          }
        }
      });
    }
    if ($('.propertySlider').length) {
      $('.propertySlider').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        items: 3,
        rtl: rtl,
        margin: 20,
        responsive: {
          0: {
            items: 1,
            margin: 0
          },
          768: {
            items: 2,
            margin: 10
          },
          1024: {
            items: 3,
            margin: 20
          }
        }
      });
    }
    if ($('.newsCaroseul').length) {
      $('.newsCaroseul').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        items: 2,
        margin: 20,
        rtl: rtl,
        responsive: {
          0: {
            items: 1,
          },
          768: {
            items: 2,
          }
        }
      });
    }
    //Add CV file form
    $('input[name="cv"]').change(function () {
      var filename = $(this).val().replace(/.*\\/, "");
      $(this).closest('div').find(".file_name").text(filename);
    });
    //CF7 + Analitics
    document.addEventListener('wpcf7mailsent', function (event) {
      gtag('event', 'formlead', {'event_category': 'knopka', 'event_action': 'formlead'});
      ym(61700275, 'reachGoal', 'formlead');
      return true;
    }, false);

    $('.sPoiner-item').on('click', function () {
      var indx = $(this).index();
      slideNow(indx);
    });
    $('.servicesNav-btn').on('click', function (event) {
      event.preventDefault();
      var indx = $('.sPoiner-item.active').index(),
          side = $(this).attr('href'),
          length = $('.sPoiner-item').length,
          newId = '';
      if (side === "#prev") {
        if (indx === 0) {
          newId = length - 1;
        } else {
          newId = indx - 1;
        }
      }
      if (side === "#next") {
        if (indx === (length - 1)) {
          newId = 0;
        } else {
          newId = indx + 1;
        }
      }
      slideNow(newId);
    });

    function slideNow(indx) {
      $('.servicesItem, .sPoiner-item').removeClass('active');
      setTimeout(function () {
        $('.sPoiner-item').eq(indx).addClass('active');
      }, 200);
      setTimeout(function () {
        $('.servicesItem').eq(indx).addClass('active');
      }, 400)
    }

    //ADDED BY GPS TEAM 03/05/2020
    $(document).ready(function () {

      /* For Template 2 Only */

      $(".y-proj-det-btn").click(function () {
        $(".y-tab-btn").removeClass("active");
        $(this).addClass("active")
        $(".y-agent-det-tbl").hide();
        $(".y-proj-det-tbl").fadeIn();
      });


      $(".y-proj-agn-btn").click(function () {
        $(".y-tab-btn").removeClass("active");
        $(this).addClass("active");
        $(".y-proj-det-tbl").hide();
        $(".y-agent-det-tbl").fadeIn();
      });

      $(".y-inquire-btn").click(function () {
        $(".y-inquiry-form-wrap .unit-contact-form .wpcf7-submit").click();
        return false;

      });
      /**/

      //Inquiry Modal Popup Trigger
      $(".x-inquire").click(function () {
        $(".inquiry-overlay, .inquiry-wrap").fadeIn();
      });


      $(".inquiry-overlay").click(function () {
        $(".inquiry-overlay, .inquiry-wrap").fadeOut();
      });

      //we have errors on pages where there is no carousel, added verification
      if ($('.owl-carousel').length) {


        if (window.location.href.indexOf("/ar/") > -1) {

          //Template 2 Carousel
          $('.y-slider .owl-carousel').owlCarousel({
            rtl: true,
            loop: true,
            responsiveClass: true,
            responsive: {
              0: {
                items: 1,
                nav: true
              },
              993: {
                items: 3,
                nav: false
              }
            }
          });

          $('.x-slider .owl-carousel').owlCarousel({
            rtl: true,
            loop: true,
            responsive: {
              0: {
                items: 1
              }
            }
          });

          $('.x-unit-gallery .owl-carousel').owlCarousel({
            rtl: true,
            loop: true,
            margin: 10,
            responsive: {
              0: {
                items: 2
              },
              1000: {
                items: 3
              }

            }
          });
        } else {

          //Template 2 Carousel
          $('.y-slider .owl-carousel').owlCarousel({
            loop: true,
            responsiveClass: true,
            responsive: {
              0: {
                items: 1,
                nav: true
              },
              993: {
                items: 3,
                nav: false
              }
            }
          });

          $('.x-slider .owl-carousel').owlCarousel({
            loop: true,
            responsive: {
              0: {
                items: 1
              }
            }
          });

          $('.x-unit-gallery .owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsive: {
              0: {
                items: 2
              },
              1000: {
                items: 3
              }

            }
          });
        }
      }/*end if*/
    });
  });
})( jQuery );