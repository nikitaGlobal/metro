<?php
/**
 * The template for displaying Vacancy
 */

get_header();
?>
    <div class="featureProjects" style="padding-top: 0px;">
        <div class="container">

            <div class="postdetails">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', 'vacancy' );
					endwhile;
				endif;
				?>
            </div>

        </div>
    </div>
    <div class="pmFooter joinForm" style="margin-top: 30px">
        <div class="pmfooterDot">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>
        <div class="pmfooterDotL">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>

        <div class="pmFooterInner">
            <div class="projectHeading">
                <div class="middleSlash"></div>
                <h2>Sign Up to Join<br>The Metropolitan Family</h2>
                <div class="middleSlash"></div>
            </div>
            <div class="footerForm">
                <?php echo do_shortcode('[contact-form-7 id="30158"]'); ?>
            </div>
        </div>
    </div>

<?php get_footer();
