<?php /* Template Name: Contacts */
get_header();

?>
<div class="featureProjects" style="padding-top: 0px;">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p id="breadcrumbs" class="pagesBrowse">','</p>' );
		}?>

		<div class="locationMap">
			<div class="mapouter">
				<div class="gmap_canvas">
					<?php the_field('iframe_map')?>
				</div>
			</div>
			<img class="contactUsBg" src="<?php echo get_template_directory_uri() ?>/assets/images/contact-us-bg.png" alt="dotted ">
		</div>

		<div class="row">
			<div class="col-md-6">
				<p class="contactH"><?php echo get_field('custom_name')?></p>
				<p class="contactSH"><?php echo pll__( 'ADDRESS' );?>:</p>
				<img class="contactIcon contactIcon1" src="<?php echo get_template_directory_uri() ?>/assets/images/location-pin.svg" alt="location">
				<p class="contactAdd"><?php echo get_field( 'address', 'option' );?></p>
				<p class="contactSH"><?php echo pll__( 'PHONE' );?>:</p>
				<img class="contactIcon" src="<?php echo get_template_directory_uri() ?>/assets/images/call.svg" alt="call" style="width: 20px;">
				<p class="contactAdd"><?php echo get_field( 'phone', 'option' );?></p>
				<p class="contactSH"><?php echo pll__( 'EMAIL' );?>:</p>
				<img class="contactIcon" src="<?php echo get_template_directory_uri() ?>/assets/images/email.svg" alt="email" style="width: 20px;">
				<p class="contactAdd"><?php echo get_field( 'email', 'option' );?></p>
				<p class="contactSH"><?php echo pll__( 'POST BOX' );?>:</p>
				<img class="contactIcon" src="<?php echo get_template_directory_uri() ?>/assets/images/postbox.svg" alt="postbox" style="width: 20px;">
				<p class="contactAdd"><?php echo get_field( 'post_box', 'option' );?></p>
			</div>

			<div class="col-md-6">
				<p class="contactH"><?php echo pll__( 'Drop Us a Message' );?></p>
				<div class="contactForm">
					<?php echo do_shortcode( get_field('form_shortcode') ); ?>
				</div>
			</div>

		</div>
	</div>
</div>
<?php if ( have_rows( 'flex_content' ) ) : ?>
    <?php while ( have_rows( 'flex_content' ) ) : the_row(); ?>
        <?php if ( get_row_layout() === 'slider' ) : ?>
            <div class="featureProjects sliderFW">
                <div class="projectHeading">
                    <div class="middleSlash"></div>
                    <h2><?php echo get_sub_field( 'header' ) ?></h2>
                    <div class="middleSlash"></div>
                </div>
                <div class="gallerySlider owl-carousel">
                    <?php
                    $gallery = get_sub_field( 'gallery' );
                    foreach ($gallery as $item): ?>
                        <div class="gallerySlider__item">
                            <img src="<?=$item['url']?>">
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer();
