<?php
/**
 * The template for displaying all single posts
 *
 *
 * @package framework
 */
/*
Template Name: Listing Template 2 HTML
Template Post Type: properties, page
*/

get_header();

if (ICL_LANGUAGE_CODE=='ar'){
?>
<!--AR STYLING-->
<style>
    body{
    direction:rtl;
}
.y-price-wrap > div:first-child{
    text-align:right;
}

.y-init-price-label{
    text-align:right;
}

.y-left{
    text-align:right;
}

.y-label-icon{
    left:initial;
    right:0;
}

.y-property-init-det-wrap > div{
    padding-left:0;
    padding-right:60px;
}

.y-property-init-det-wrap > div:before {
    
    right: initial;
    left:25px;
}

.y-det-info-wrap{
    text-align:right;
}

.y-img-container1{
    left:initial;
    right:150px;
}

.y-img-container2{
    left:initial;
    right:-230px;;
}

.y-feats-amen{
    text-align:right;
}

.y-template-wrap .x-unit-facilities-listing li{
    padding-left:0;
    padding-right:45px;
}
.y-feats-amen .x-unit-facilities-subtitle{
    text-align:right;
}

.y-template-wrap .x-unit-facilities-listing li:before{
    left:initial;
    right:0;
}

.y-template-wrap .x-location-landmarks{
    text-align:right;
}

.y-template-wrap .x-contact-container{
 text-align:right;
}

.y-template-wrap .x-contact-basic li {
    position: relative;
    padding-right: 60px;
    padding-left:initial!important;
}

.y-template-wrap .x-contract-entry-icon {
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    width: 40px;
    height: 40px;
    background: #16a2e4;
    padding: 10px;
}


.y-template-wrap .x-contract-entry-icon {
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    width: 40px;
    height: 40px;
    background: #16a2e4;
    padding: 10px;
}

.y-template-wrap .x-contract-entry-icon {
    left:initial;
    right:0;
}


</style>
<!--AR STYLING END -->
<?php
//AR Page End
}
?>
<?php
    $propType = get_the_terms(get_the_ID(), 'property_categ');
    $planType =get_the_terms(get_the_ID(), 'plan_types');
    $genrImg = $propType[0]->slug.'-'.get_field('property_name').'-'.get_field('unit_reference');
    $nameImgs = str_replace('&', '', $genrImg);
    $nameImgs = str_replace('  ', ' ', $nameImgs);
    $nameImgs = str_replace(' ', '-', $nameImgs);
    $attImgs = get_image_id($nameImgs);

    $i = 1; $gallery = array();
    $altImg = "Image - ".get_field('property_name').", ".get_field('community').", ".get_field('emirate')." | Project - ".get_field('property_type');
    foreach($attImgs as $image){
        $gallery[$i] = $image[0];
        $i++;
    }
?>
<section class="x-main-wrap featureProjects">

    <!-- INQUIRY MODAL -->
    <section class="inquiry-overlay"></section>
    <section class="inquiry-wrap">
        <span class="inquiry-modal-heading"><?php echo do_shortcode( '[acf field="send_us_an_inquiry_label"]' ); ?></span>
        <?php if(ICL_LANGUAGE_CODE=='ar') { echo do_shortcode( '[contact-form-7 id="48839" title="Unit Inquiry Form AR"]' ); } else {echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' );}?>    </section>
    <!-- END OF INQUIRY MODAL-->

    
    <main class="y-template-wrap">
        <section class="y-slider-wrap">
            <div class="y-slider">
                <div class="owl-carousel">
                    <?php foreach($gallery as $image){ ?>
                    <div>
                        <a href="<?=$image?>" data-fancybox="images" class="y-slider-link">
                            <img src="<?=$image?>" alt="<?=$altImg?>">
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="y-initial-info-wrap">
            <article>
                <div class="y-left">
                    <div class="y-property-stickers">
                        <span class="y-sticker plan <?=$planType[0]->slug?>"><?=get_field( 'action')?></span>
                        <span class="y-sticker type"><?=get_field( 'property_type')?></span>
                    </div>
                    <h1 class="y-property-title"><?=get_field("prop_title") ?></h1>
                    <div class="y-property-location">
                        <img width="24" class="y-location-icon" src="/wp-content/themes/framework/assets/icons/community.svg"/>
                        <span class="y-location-text"><?=get_field('community')?></span>
                    </div>
                    <div class="y-property-init-det-wrap">
                        <?php if(get_field("unit_builtup_area")): ?>
                        <div>
                            <div class="y-property-init-det-wrap-img">
                                <img class="y-label-icon" src="/wp-content/themes/framework/assets/images/ruler.svg"/>
                            </div>
                            <div class="y-property-init-det-wrap-img-text">
                                <span class="y-init-det-label"><?=get_field("area_label")?></span>
                                <span class="y-init-det-entry"><?=get_field("unit_builtup_area")?> <?=pll__('Sq Ft')?></span>
                            </div>
                        </div>
                        <?php endif;?>
                        <?php if(get_field("no_of_bathroom")): ?>
                        <div>
                            <div class="y-property-init-det-wrap-img">
                                <img class="y-label-icon" src="/wp-content/themes/framework/assets/images/bathroom.svg"/>
                            </div>
                            <div class="y-property-init-det-wrap-img-text">
                                <span class="y-init-det-label"><?=get_field("bathroom_label")?></span>
                                <span class="y-init-det-entry"><?=get_field("no_of_bathroom")?></span>
                            </div>
                        </div>
                        <?php endif;?>
                        <?php if(get_field("bedrooms")): ?>
                        <div>
                            <div class="y-property-init-det-wrap-img">
                                <img class="y-label-icon" src="/wp-content/themes/framework/assets/images/mattress.svg"/>
                            </div>
                            <div class="y-property-init-det-wrap-img-text">
                                <span class="y-init-det-label"><?=get_field("bedroom_label")?></span>
                                <span class="y-init-det-entry"><?=get_field("bedrooms")?></span>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                    <p class="y-property-writeup">
                        <?=get_field("property_writeup")?>
                    </p>
                    
                    
                <button class="y-inquire x-inquire"><?php echo do_shortcode( '[acf field="inquire_now_label"]' ); ?></button>
                
                </div>
                <div class="y-right">
                    <div class="y-inquiry-form-wrap">
                        <div class="y-price-wrap">
                            <div>
                                <span class="y-init-price-label"><?php echo do_shortcode( '[acf field="total_price_label"]' ); ?></span>
                                <span class="y-init-price"><?php echo number_format(do_shortcode( '[acf field="price"]')) ; ?> AED</span>
                            </div>
                            <?php /* ?>
                            <div>
                                <button class="y-btn y-inquire-btn"><?php echo do_shortcode( '[acf field="inquire_now_label"]' ); ?></button>
                            </div>
 <?php */ ?>
                        </div>

                        <?php if(ICL_LANGUAGE_CODE=='ar') { echo do_shortcode( '[contact-form-7 id="48839" title="Unit Inquiry Form AR"]' ); } else {echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' );}?>

                    </div>
                </div>
            </article>
        </section>

        <section class="y-det-info-wrap">
            <article class="y-det-container">
                <div>
                    <button class="y-tab-btn y-proj-det-btn active"><?php echo do_shortcode( '[acf field="project_details_label"]' ); ?></button>
                    <button class="y-tab-btn y-proj-agn-btn"><?php echo do_shortcode( '[acf field="agent_information_label"]' ); ?></button>

                    <table class="y-proj-tbl">
                        <tbody class="y-proj-det-tbl">
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="unit_reference_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="unit_reference"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="parking_slots_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="parking"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="property_name_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="property_name"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="permit_number_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="permit_number"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="emirate_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="emirate"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="completition_status_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="completion_status"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="latitude_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="lat"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="longitude_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="lng"]' ); ?></td>
                            </tr>
                        </tbody>

                        <tbody class="y-agent-det-tbl">
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="company_name_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="company_name"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_email"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_agent_phone"]' ); ?></td>
                            </tr>

                            <tr>
                                <td><?php echo do_shortcode( '[acf field="last_updated_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="last_updated"]' ); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo do_shortcode( '[acf field="listing_date_label"]' ); ?></td>
                                <td><?php echo do_shortcode( '[acf field="listing_date"]' ); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>

        <section class="y-pictures-wrap">
            <article>
                <table>
                    <tr>
                        <td></td>
                        <td><div class="y-img-container1"><img src="<?=$gallery[1];?>" alt="<?=$altImg?>"><div></td>
                    </tr>
                </table>

                <table class="y-pictures-tbl-2">
                    <tr>
                        <td><div class="y-img-container2"><img src="<?=$gallery[2];?>" alt="<?=$altImg?>"></td>
                        <td class="y-feats-amen">
                            <h2><?php echo do_shortcode( '[acf field="features_and_amenities_label"]' ); ?></h2>
                            <span class="x-unit-facilities-subtitle"><?php echo do_shortcode( '[acf field="everything_you_need_label"]' ); ?></span>

                            
                        <ul class="x-unit-facilities-listing">
                            <?php
                            if (ICL_LANGUAGE_CODE=='ar'){
                                $facilities_or = do_shortcode( '[acf field="x-facilities"]' );

                                $facility_en = array("24 hours Maintenance", "Balcony", "Bank/ATM Facility","Basement parking","Basketball Court","BBQ area","Beach Access","Broadband ready","Built in wardrobes","Bus services","Business Center","Carpets","Central air conditioning","Central heating","Children's nursery","Children's play area","Clubhouse","Communal gardens","Community View","Concierge service","Covered parking","Cycling tracks","Driver's Room","Fitness Center","Fully fitted kitchen","Fully furnished","Gazebo and outdoor entertaining area","Golf club and clubhouse","Gymnasium","Intercom","Jacuzzi","Kitchen white goods","Landscaped Garden","Laundry Service","Laundry/washing room","Maid's room","Marble floors","Marina Berth","Metro station"," Mosque","On high floor","On low floor","On mid floor","Part furnished","Pets allowed","Polo club and clubhouse"," Private garage","Private garden","Private swimming pool","<li>Public park</li>","Public parking","Public transport","Recreational Facilities","Restaurants","Satellite/Cable TV","Sauna","School","Shared swimming pool","Shopping mall","Shops","Solid wood floors","Sports academies","Squash courts","Steam room","Storage room","Study","Tennis courts","Upgraded interior","Valet Service","View of gardens","View of golf course","View of parkland","View of sea/water","Walking Trails");
                                $facility_ar = array("صيانة 24 ساعة", "شرفة", "/بنك / صراف آلي","وقوف السيارات في الطابق السفلي","ملعب كرة السلة","منطقة للشواء","الوصول إلى الشاطئ","جاهز للإنترنت","خزائن مدمجة","خدمات الحافلات","مركز أعمال","سجاد","تكييف الهواء المركزي","تدفئة مركزية","حضانة الأطفال","منطقة لعب الاطفال","كلوب هاوس","الحدائق العامة","اطلالة على المجمع","خدمة الكونسيرج","مواقف مغطاة للسيارات","مسارات ركوب الدراجات","غرفة السائق","مركز اللياقة البدنية","مطبخ مجهز بالكامل","مفروشة بالكامل","جازيبو ومنطقة ترفيهية في الهواء الطلق","نادي الجولف","صالة رياضية","انتركم","جاكوزي","المطبخ بسلع بيضاء ","حديقة معدة","خدمة غسيل الملابس","غرفة الغسيل","غرفة للخادمة","أرضيات من الرخام","مرسى مارينا","محطة مترو"," مسجد","في الطابق العلوي","في الطابق المنخفض","في الطابق الأوسط","مؤثثة جزئيا","مسموح بدخول الحيوانات الأليفة","نادي بولو","مرأب خاص","حديقة خاصة","مسبح خاص","<li>حديقه عامه</li>","مواقف السيارات العامة","النقل العام","المرافق الترفيهية","مطاعم","الأقمار الصناعية / تلفزيون الكابل","ساونا","مدرسة","مسبح مشترك","مركز تسوق","محلات","الأرضيات الخشبية الصلبة","الأكاديميات الرياضية","ملاعب اسكواش","غرفة البخار","غرفة تخزين","غرفة الدراسة","ملاعب التنس","تصميم داخلي مطور","خدمة صف السيارات","إطلالة على حدائق","إطلالة على ملعب الجولف","إطلالة على المنتزه","إطلالة على البحر / الماء","مسارات المشي");

                                echo str_replace($facility_en,$facility_ar,$facilities_or);
                            }
                            else {
                                echo do_shortcode( '[acf field="x-facilities"]' );
                            }
                            ?>
                        </ul>
                        </td>
                    </tr>
                </table>
            </article>
        </section>

        <?php /* ?>
        <div class="x-unit-gallery">
            <div class="owl-carousel">
              <?php echo do_shortcode( '[acf field="unit_gallery"]' ); ?>
            </div>
        </div>
        <?php */ ?>
        
    <div class="map">
        <div>
            <div id="googleMap"></div>
            <script type="text/javascript">
                function initMapPage() {
                    var mapOptions = {
                        zoom: 15,
                        center: new google.maps.LatLng(<?=get_field("lat")?>, <?=get_field("lng")?>),
                        styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#02b2ee"},{"visibility":"on"}]}]
                    };

                    var mapElement = document.getElementById('googleMap');
                    var map = new google.maps.Map(mapElement, mapOptions);
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?=get_field("lat")?>, <?=get_field("lng")?>),
                        map: map,
                        title: "<?=get_field("prop_title") ?>"
                    });
                }
            </script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRTYB_ke75HeFjs_B5uhGEVUobs1KUmDo&callback=initMapPage"></script>
        </div>
        <?php /* ?>
        <!--
        <div class="x-location-landmarks">
            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>

            <div class="x-landmark-entry">
                <h3>Dubai Mall</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>
        </div>-->
        <?php */ ?>
    </div>
    <div class="x-contact-section">
        <div class="x-contact-section-content">

            <div class="x-contact-container">
                <div class="x-contact-basic">
                    <span class="x-contact-pre"> <?php echo do_shortcode( '[acf field="contact_us_label"]' ); ?> </span>
                        <?php /* ?><!--<h3><?php echo do_shortcode( '[acf field="lets_get_in_touch_label"]' ); ?></h3>--><?php */ ?>
                        <h3><?=get_field('company_name')?></h3>

                    <ul>
                        <li>
                            <span class="x-contract-entry-icon email">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/envelope.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="email_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php the_field('email', 'option'); ?></span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/phone.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="call_us_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php the_field('phone', 'option'); ?></span>
                        </li>


                        <li>
                            <span class="x-contract-entry-icon">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/placeholder.svg"/>
                            </span>
                            <span class="x-contact-entry-label"><?php echo do_shortcode( '[acf field="address_label"]' ); ?></span>
                            <span class="x-contact-entry-listing"><?php if(ICL_LANGUAGE_CODE=='ar') { the_field('address_ar', 'option'); } else { the_field('address', 'option') ;} ?></span>
                        </li>
                    </ul>
                </div>

                <div class="x-contact-form-container">
                    <div class="x-contact-section-title">
                        <span class="x-contact-section-subtitle"><?php echo do_shortcode( '[acf field="ready_for_your_new_home_label"]' ); ?></span>
                        <h2 class="x-contact-section-heading"><?php echo do_shortcode( '[acf field="send_us_an_inquiry_label"]' ); ?></h2>
                    </div>
                    <?php /* <!-- <?php echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' ); ?> --> */ ?>
                    <?php if(ICL_LANGUAGE_CODE=='ar') { echo do_shortcode( '[contact-form-7 id="48839" title="Unit Inquiry Form AR"]' ); } else {echo do_shortcode( '[contact-form-7 id="28387" title="Unit Inquiry Form"]' );}?>
                </div>
            </div>
        </div>
    </div>


    </main>
</section>






<?php get_footer();
