<?php
    $args1              = $args;
    $args1['tax_query'] = array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'plan_types',
            'field'    => 'slug',
            'terms'    => array('off_plan_properties')
        )
    );

?>
<section class="offplan-related-content-main-wrap">
    <div class="offplan-related-content-sub-wrap">
        <h3>Off-plan projects from <?php the_title(); ?></h3>
        <div class="p1-readyProject__wrapperBtn col-8 col-md-7 col-lg-5 col-xl-3 ml-auto mr-auto d-flex align-items-center justify-content-between btnSelect">
            <?php
                $begin = true;
                foreach ($types as $k => $type) {
                    
                    ?>
                    <button data-target="<?php echo $type; ?>" type="button"
                            class="typeSelect typeSelect<?php
                                if ($begin) {
                                    ?> btnActive<?php }
                            ?>"><?php echo ucfirst($type); ?></button><?php
                    if ($type != end($types)) {
                    
                    }
                    $begin = false;
                }
            ?>
        </div>
    </div>
    <div class="featureProjects">
        <div class="container" style="position: relative">
            <?php
                $begin = true;
                foreach ($types as $k => $type) {
                    $args2                = $args1;
                    $args2['tax_query'][] = array(
                        'taxonomy' => 'property_categ',
                        'field'    => 'id',
                        'terms'    => $k
                    );
                    $qu                   = new WP_QUERY($args2);
                    if ($qu->have_posts()) {
                        ?>
                        <div
                                data-target="<?php echo $type; ?>"
                                <?php if ( ! $begin){ ?>style="display:none;"<?php }
                            $begin = false;
                        ?>
                                class="propertySlider owl-carousel propertySlider-<?php echo $type; ?>">
                            <?php
                                
                                while ($qu->have_posts()) {
                                    $qu->the_post();
                                    get_template_part('preview-related-property');
                                } ?>
                        </div>
                        <?php
                    }
                    wp_reset_postdata();
                    wp_reset_query();
                } ?>
        </div>
    </div>
</section>
