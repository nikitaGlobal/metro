<?php /* Template Name: About Services */
get_header();
$background = "";
if ( get_field( 'bg' ) ) {
	$background = 'style="background-image: url(' . get_field( 'bg' ) . ');"';
}
?>
    <div class="headPage">
        <div class="headBG" <?=$background?>></div>
        <div class="headText">
            <div class="container">
                <div class="headText__wrp">
                    <img src="/wp-content/themes/framework/assets/images/vpat.svg" class="vpatPA">
                    <div class="headText__header">
                        <h2 class="headText__title"><?php echo get_field('title') ?></h2>
                        <div class="separator"></div>
                    </div>
                    <div class="headText__content">
                        <div class="headForm">
                            <div class="headForm-body">
                                <p class="headForm-head"><?php echo pll__( 'Leave a Request' );?></p>
                                <div class="footerForm" style="padding:10px;position: relative;">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">
                                    <?php echo do_shortcode( get_field('form_shortcode') ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="headText__view">
                            <?php echo get_field('content') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ( have_rows( 'flex_content' ) ) : ?>
	<?php while ( have_rows( 'flex_content' ) ) : the_row(); ?>

		<?php if ( get_row_layout() === 'who' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects pmFeature who">
                <div class="container">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedL" style="width: 130px;">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <?php if(get_sub_field( 'desc' )): ?>
                    <div class="pmBanB <?=get_sub_field( 'design' ) ?>">
                        <p><?php echo get_sub_field( 'desc' ) ?></p>
                    </div>
                    <?php endif;
                    $cards = get_sub_field( 'cards' );
                    ?>

					<?php if ( ! empty( $cards ) ) : ?>
                        <div class="row advRow advRowpm">
                            <?php
                                if(count($cards)>4){$col=4;}else{$col=3;}
                            ?>
							<?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="col-md-<?=$col?>">
                                    <div class="advTxt">
                                        <div class="advImg">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg">
                                            <div class="d-none"><?php echo $subarr['header'] ?></div>
                                        </div>
										<?php
										$temp_str = $subarr['header'];
										$spc_pos  = stripos( $temp_str, " " );
										if ( $spc_pos ) : ?>
                                            <h4 class="advHeadpm">
                                                <span><?php echo substr( $temp_str, 0, $spc_pos ) ?></span><?php echo substr( $temp_str, $spc_pos ) ?>
                                            </h4>
										<?php else : ?>
                                            <h4 class="advHeadpm"><?php echo $temp_str ?></h4>
										<?php endif; ?>
                                        <p class="advDesc"><?php echo $subarr['subheader'] ?></p>
                                    </div>
                                </div>
							<?php endforeach; ?>

                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

		<?php if ( get_row_layout() === 'for_landlords' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects landlords">
                <div class="container">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/dotted1.png" class="bgDottedR" style="width: 100px;">

                    <div class="projectHeading pmservices<?php if(get_sub_field( 'hs' )=='h3'){echo ' left';}?>">
                        <?php if(get_sub_field( 'hs' )!='h3'){?><div class="middleSlash"></div><?php } ?>
                        <<?=get_sub_field( 'hs' );?>><?php echo get_sub_field( 'header' ) ?></<?=get_sub_field( 'hs' );?>>
                        <div class="middleSlash"></div>
                    </div>

					<?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                        <div class="row abtRow">
							<?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="<?=get_sub_field('style_cards');?>">
                                    <div>
                                    <div class="abtRowTCont">
                                        <div class="abtRowTL">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg" width="50">
                                        </div>
                                        <div class="abtRowTR">
                                            <p><?php echo $subarr['header'] ?></p>
                                        </div>
                                    </div>
                                    <p class="abtRowBCont"><?php echo $subarr['description'] ?></p>
                                    </div>
                                </div>
							<?php endforeach; ?>
                        </div>
					<?php endif; ?>

					<?php if ( get_sub_field( 'button_text' ) ) : ?>
                        <div class="discoverCont">
                            <a href="#" data-toggle="modal" data-target="#consultationModal"><?php echo get_sub_field( 'button_text' ); ?></a>
                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

		<?php if ( get_row_layout() === 'benefits' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects BenefitsBlock">
                <div class="container">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <?php if ( !empty( get_sub_field( 'desc' ))) : ?>
                    <div class="pmBanB fullWidth left"><?=get_sub_field( 'desc' )?></div>
                    <?php endif; ?>
					<?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                        <div class="row abtRow">
							<?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                                <div class="col-md-4">
                                    <div class="BenefitsCont">
                                        <div class="abtRowTCont">
                                            <div class="abtRowTL">
                                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $subarr['icon'] ?>.svg" width="50">
                                            </div>
                                            <div class="abtRowTR">
                                                <p><?php echo $subarr['text'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php endforeach; ?>
                        </div>
					<?php endif; ?>

					<?php if ( get_sub_field( 'button_text' ) ) : ?>
                        <div class="discoverCont">
                            <a href="#" data-toggle="modal" data-target="#consultationModal"><?php echo get_sub_field( 'button_text' ); ?></a>
                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

		<?php if ( get_row_layout() === 'how_it_work' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects">
                <div class="container">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <div class="verticalSep">
						<?php if ( ! empty( get_sub_field( 'steps' ) ) ) : $count = 1; ?>
							<?php foreach ( get_sub_field( 'steps' ) as $subarr ) : $side = ( $count % 2 == 1 ) ? "R" : "L" ?>
                                <div class="verticalSep<?php echo $side ?>Cont">

                                    <div class="verticalSep<?php echo $side ?>">
										<?php if ( $side === "R" ) : ?>
                                            <div class="stepVR stepVR2">
                                                <span><?php echo pll__( 'Step' );?> <span class="blue"><?php echo $count ?></span></span><img src="<?php echo get_template_directory_uri() ?>/assets/images/Ellipse.svg">
                                            </div>
                                            <p class="verticalSepP1"><?php echo $subarr['header'] ?></p>
                                            <p class="verticalSepP2"><?php echo $subarr['subheader'] ?></p>
										<?php else : ?>
                                            <div class="stepVL">
                                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/Ellipse.svg"><span><?php echo pll__( 'Step' );?> <span class="blue"><?php echo $count ?></span></span>
                                            </div>
                                            <div class="stepVR stepVR3">
                                                <span><?php echo pll__( 'Step' );?> <span class="blue"><?php echo $count ?></span></span><img src="<?php echo get_template_directory_uri() ?>/assets/images/Ellipse.svg">
                                            </div>
                                            <p class="verticalSepP1"><?php echo $subarr['header'] ?></p>
                                            <p class="verticalSepP2"><?php echo $subarr['subheader'] ?></p>
										<?php endif; ?>
                                    </div>
                                </div>
								<?php $count ++; endforeach; ?>
						<?php endif; ?>
                    </div>
					<?php if ( get_sub_field( 'button_text' ) ): ?>
                        <div class="discoverCont">
                            <a href="#" data-toggle="modal" data-target="#consultationModal"><?php echo get_sub_field( 'button_text' ) ?></a>
                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

		<?php if ( get_row_layout() === 'reviews' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects">
                <div class="container">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>

                    <div class="pmBanB">
						<?php echo get_sub_field( 'desc' ) ?>
                    </div>

					<?php if ( ! empty( get_sub_field( 'reviews' ) ) ) : ?>
						<?php foreach ( get_sub_field( 'reviews' ) as $subarr ) : ?>
                            <div class="reviewBlock">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/coma.svg" class="reviewComa">
                                <div class="reviewCont">
                                    <div class="reviewL">
                                        <img src="<?php echo $subarr['picture']['url'] ?>">
                                    </div>
                                    <div class="reviewR">
                                        <p><?php echo $subarr['review_text'] ?></p>
                                    </div>
                                </div>
                                <div class="reviewSign">
                                    <div class="reviewSignSlash"></div>
                                    <p><?php echo $subarr['full_name'] ?></p>
                                </div>
                            </div>
						<?php endforeach; ?>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; ?>

		<?php if ( get_row_layout() === 'prices' && get_sub_field( 'show_block' ) ) : ?>
            <div class="featureProjects" style="padding-bottom: 50px;">
                <div class="container">
                    <div class="projectHeading">
                        <div class="middleSlash"></div>
                        <h2><?php echo get_sub_field( 'header' ) ?></h2>
                        <div class="middleSlash"></div>
                    </div>
                    <table class="table table-bordered pmTbl">
                        <tbody>
                        <tr>
                            <td class="td-col-1 td-header"></td>
                            <td class="td-col-2 td-header"><span><?php echo get_sub_field( 'type_1' ) ?></span></td>
                            <td class="td-col-3 td-header"><span><?php echo get_sub_field( 'type_2' ) ?></span></td>
                            <td class="td-col-4 td-header"><span><?php echo get_sub_field( 'type_3' ) ?></span></td>
                        </tr>
						<?php if ( ! empty( get_sub_field( 'services' ) ) ) : $test = get_sub_field( 'services' ); ?>
							<?php foreach ( get_sub_field( 'services' ) as $subarr ) : ?>
                                <tr class="<?php echo $subarr['service_name'] ? '' : 'bb-0'; ?>">
                                    <td class="td-col-1 td-left"><?php echo $subarr['service_name'] ?></td>
									<?php for ( $i = 0; $i < 3; $i ++ ) : $output_text = '' ?>
										<?php if ( $subarr['cells'][ $i ]['provided'] === 'custom' ) :
											$output_text = '<span class="mobiletdCol">' . $subarr['cells'][ $i ]['custom_text'] . '</span>';
                                        elseif ( $subarr['cells'][ $i ]['provided'] === 'yes' ) :
											$output_text = '<img src="' . get_template_directory_uri() . '/assets/images/plus.svg" class="plusIcon">';
										else :
											$output_text = '<img src="' . get_template_directory_uri() . '/assets/images/minus.svg" class="plusIcon">';
										endif; ?>
                                        <td class="td-col-<?php echo $i + 2;
										echo $i === 2 ? ' td-right' : ''; ?>"><?php echo $output_text ?></td>
									<?php endfor; ?>
                                </tr>
							<?php endforeach; ?>
						<?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif; ?>
        <?php if ( get_row_layout() === 'text_block' ) : ?>
            <div class="featureProjects noPd" style="padding-bottom: 50px; pading-top: 0;">
                <div class="container">
                <div class="textBlock">
						<?php echo get_sub_field( 'text' ) ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>

	<?php endwhile; ?>
<?php endif; ?>

    <div class="pmFooter">
        <div class="pmfooterDot">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>
        <div class="pmfooterDotL">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/pmfdot.png">
        </div>

        <div class="pmFooterInner">
            <div class="projectHeading">
                <div class="middleSlash"></div>
                <h2><?php echo get_field( 'footer_header' ) ?></h2>
                <div class="middleSlash"></div>
            </div>
            <div class="footerForm">
	            <?php echo do_shortcode( get_field('form_shortcode') ); ?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="consultationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modalH"><?php echo get_field( 'footer_header' ) ?></p>
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">

						<?php echo do_shortcode( get_field('form_shortcode') ); ?>

                    </div>
                </div>
            </div>

        </div>
    </div>

<?php get_footer();