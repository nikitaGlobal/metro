<?php
/* Prevent direct access */
defined('ABSPATH') or die("You can't access this file directly.");

/**
 * This is the default template for one isotopic result
 *
 * !!!IMPORTANT!!!
 * Do not make changes directly to this file! To have permanent changes copy this
 * file to your theme directory under the "asp" folder like so:
 *    wp-content/themes/your-theme-name/asp/isotopic.php
 *
 * It's also a good idea to use the actions to insert content instead of modifications.
 *
 * You can use any WordPress function here.
 * Variables to mention:
 *      Object() $r - holding the result details
 *      Array[]  $s_options - holding the search options
 *
 * DO NOT OUTPUT ANYTHING BEFORE OR AFTER THE <div class='item'>..</div> element
 *
 * You can leave empty lines for better visibility, they are cleared before output.
 *
 * MORE INFO: https://wp-dreams.com/knowledge-base/result-templating/
 *
 * @since: 4.0
 */
?>

<div class="listing-entry">
    

    <?php
        $propertyCommunity = get_field('community', $r->id);
        $propertyName = get_field('property_name', $r->id);
        $propertyEmirate = get_field('emirate', $r->id);
        $propertyType = get_field('property_type', $r->id);
        $propertyPlan = get_field('action', $r->id);
        $propertyBedRoom = get_field('bedrooms', $r->id);
        $propertyBathRoom = get_field('no_of_bathroom', $r->id);
        $propertyArea = get_field('unit_builtup_area', $r->id);
        $propertyPrice = number_format(get_field('price', $r->id));
        $propertyDesc = get_field('property_writeup', $r->id);
        $planType =get_the_terms($r->id, 'plan_types');
       
    ?>
    <a id="<?php echo $r->id;?>" class="listing-img" href="<?php echo $r->link;?>" target="_blank">
            <div class="property-status-tag"><?php echo do_shortcode('[acf field="additional_remarks" post_id="' . $r->id . '"]'); ?></div>
            <img src="<?php echo $r->image; ?>"/>
            <div class="propertyStickers">
                <span class="propertySticker <?=$planType[0]->slug?>"><?=$propertyPlan?></span>
                <span class="propertySticker type"><?=$propertyType?></span>
            </div>
            <div class="property-summary-desc">
                <p class="property-desc-title"><?php echo $propertyName ; ?>, <?php echo $propertyCommunity ; ?>, <?php echo $propertyEmirate ; ?></p>
                <p class="property-desc-writeup">
                    <?php
                        $position=120; // Define how many character you want to display.
                        $message= strip_tags($propertyDesc);
                        $set = substr($message, 0, $position);
                        echo $set;
                        echo "...";
                    ?>
                </p>
            </div>
    </a>
    
    <div class="listing-detail-container">
        <a class="listing-title-link" href="<?php echo $r->link;?>" target="_blank"><span class="listing-entry-title"><?php echo $r->title; ?></span></a>
        <span class="listing-entry-community"><?php echo $propertyName ; ?>, <?php echo $propertyCommunity ; ?>, <?php echo $propertyEmirate ; ?></span>
        
        <ul class="propertyHead-icons">
            <li class="propertyHead-icons_item beds"><?=pll__('Beds')?>: <?php echo $propertyBedRoom; ?></li>
            <li class="propertyHead-icons_item baths"><?=pll__('Baths')?>: <?php echo $propertyBathRoom; ?></li>
            <li class="propertyHead-icons_item area"><?=pll__('Sq Ft')?>: <?php echo round($propertyArea); ?></li>
        </ul>
        
        <span class="listing-entry-dets"></span>
        <span class="listing-entry-price">AED <?php echo $propertyPrice; ?>
        
        <a class="listing-whatsapp" href="https://api.whatsapp.com/send?phone=<?php the_field('phone', 'option'); ?>&amp;text=Hi,%20I%20am%20interterested%20in%20this%20property%20%20%20%20%20<?php echo $r->link;?>"><span class="whapp">WhatsApp</span><img src="/wp-content/themes/framework/assets/images/icon-whatsapp.svg"></a>
        
        </span>
 
    </div>
</div>