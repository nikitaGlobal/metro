<?php
$search_sorting     = get_field( 'search_sorting', 'option' );
$min_bedroom_option = $search_sorting['min_bedroom'];
$max_bedroom_option = $search_sorting['max_bedroom'];
$price              = $search_sorting['price'];
global $minBedroom, $maxBedroom, $minPrice, $maxPrice, $community, $propertyType, $searchType;
//var_dump( $community );
?>
<div class="pageContainer" id="topSearch">
    <div class="container">
        <div class="pageSearch">
            <form class="d-flex flex-wrap justify-content-between align-items-center">
				<?php
				if ( ! $searchType ):
					$tax_name = basename( $_SERVER['REQUEST_URI'] );
					$term_id  = get_term_by( 'slug', $tax_name, 'emirate' )->term_id;
				else:
					$term_id = $searchType;
				endif;
				?>
                <input type="hidden" name="searchType" value="<?php echo $term_id; ?>" id="searchType">

                <div class="locationContainer mb-3">
					<?php $comm_terms = get_terms( array(
						'taxonomy'   => 'emirate',
						'hide_empty' => true,
						'child_of'   => $term_id,
					) );
					//debag($comm_terms);
					?>
                    <select id="loc_select" name="community[]" class="community-select form-control location" multiple="multiple">
						<?php if ( ! empty( $comm_terms ) ) :

							foreach ( $comm_terms as $key => $termobj ) :
								if ( in_array( $termobj->term_id, $community ) ):
									echo '<option selected value="' . $termobj->term_id . '">' . $termobj->name . '</option>';
								else:
									echo '<option value="' . $termobj->term_id . '">' . $termobj->name . '</option>';
								endif;
							endforeach;

						endif; ?>
                    </select>
                </div>

                <div class="selectContainer">
					<?php $pt_terms = get_terms( array(
						'taxonomy'   => 'property_type',
						'hide_empty' => false,
					) ); ?>
                    <select class="selectField form-control pageSelectField" name="PropertyType" id="pt_select">
						<?php if ( ! empty( $pt_terms ) ) :
							foreach ( $pt_terms as $termobj ) :
								if ( (int) $propertyType === $termobj->term_id ):
									echo '<option selected value="' . $termobj->term_id . '">' . $termobj->name . '</option>';
								else:
									echo '<option value="' . $termobj->term_id . '">' . $termobj->name . '</option>';
								endif;
							endforeach;
						endif; ?>
                    </select>
                </div>

                <div class="selectContainer">
                    <select class="form-control selectField" name="MinBedroom">
						<?php if ( $minBedroom ):
							echo '<option disabled value="0">' . pll__( 'Min. Bedroom' ) . '</option>';

							for ( $i = 1; $i <= $max_bedroom_option; $i ++ ):
								if ( $i == $minBedroom ):
									echo '<option selected value="' . $i . '">' . pll__( 'Min' ) . ' ' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
								else:
									echo '<option value="' . $i . '">' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
								endif;
							endfor;

						else:
							echo '<option selected disabled value="0">' . pll__( 'Min. Bedroom' ) . '</option>';

							for ( $i = 1; $i <= $max_bedroom_option; $i ++ ):
								echo '<option value="' . $i . '">' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
							endfor;
						endif; ?>
                    </select>
                </div>

                <div class="selectContainer">
                    <select class="form-control selectField" name="MaxBedroom">
						<?php if ( $maxBedroom ):
							echo '<option disabled value="0">' . pll__( 'Max. Bedroom' ) . '</option>';

							for ( $i = 1; $i <= $max_bedroom_option; $i ++ ):
								if ( $i == $maxBedroom ):
									echo '<option selected value="' . $i . '">' . pll__( 'Max' ) . ' ' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
								else:
									echo '<option value="' . $i . '">' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
								endif;
							endfor;

						else:
							echo '<option selected disabled value="0">' . pll__( 'Max. Bedroom' ) . '</option>';

							for ( $i = 1; $i <= $max_bedroom_option; $i ++ ):
								echo '<option value="' . $i . '">' . $i . ' ' . pll__( 'Bedroom' ) . '</option>';
							endfor;
						endif; ?>
                    </select>
                </div>

                <div class="selectContainer">
                    <select class="form-control selectField" name="MinPrice">
						<?php if ( $minPrice ):
							echo '<option disabled value="0">' . pll__( 'Min. Price' ) . '</option>';
							foreach ( $price as $item ):
								$trim_min = str_replace( ' ', '', $item['item_price'] );

								if ( $minPrice == $trim_min ):
									echo '<option selected value="' . $trim_min . '">' . $item['item_price'] . '</option>';
								else:
									echo '<option value="' . $trim_min . '">' . $item['item_price'] . '</option>';
								endif;
							endforeach;
						else:
							echo '<option selected disabled value="0">' . pll__( 'Min. Price' ) . '</option>';
							foreach ( $price as $item ):
								echo '<option value="' . str_replace( ' ', '',
										$item['item_price'] ) . '">' . $item['item_price'] . '</option>';
							endforeach;
						endif; ?>
                    </select>
                </div>

                <div class="selectContainer">
                    <select class="form-control selectField" name="MaxPrice">
						<?php if ( $maxPrice ):
							echo '<option disabled value="0">' . pll__( 'Max. Price' ) . '</option>';
							foreach ( $price as $item ):
								$trim_max = str_replace( ' ', '', $item['item_price'] );

								if ( $maxPrice == $trim_max ):
									echo '<option selected value="' . $trim_max . '">' . $item['item_price'] . '</option>';
								else:
									echo '<option value="' . $trim_max . '">' . $item['item_price'] . '</option>';
								endif;
							endforeach;
						else:
							echo '<option selected disabled value="0">' . pll__( 'Max. Price' ) . '</option>';
							foreach ( $price as $item ):
								echo '<option value="' . str_replace( ' ', '',
										$item['item_price'] ) . '">' . $item['item_price'] . '</option>';
							endforeach;
						endif; ?>
                    </select>
                </div>

                <div class="selectContainerBtn">
                    <button type="submit" class="searchBtn"><?php echo pll__( 'SEARCH' ); ?></button>
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="pageSearchB">
            <span><?php echo pll__( 'Sort by' ); ?></span>
            <div class="pageInputCont">
                <select class="form-control pageSelectSort" name="sortby">
                    <option value="DATE"><?php echo pll__( 'Date added' ); ?></option>
                    <option value="ASC"><?php echo pll__( 'A - Z' ); ?></option>
                    <option value="DESC"><?php echo pll__( 'Z - A' ); ?></option>
                    <option value="views-less"><?php echo pll__( 'Views from less to more' ); ?></option>
                    <option value="views-more"><?php echo pll__( 'Views from more to less' ); ?></option>
                    <option value="price-less"><?php echo pll__( 'Price from less to more' ); ?></option>
                    <option value="price-more"><?php echo pll__( 'Price from more to less' ); ?></option>
                </select>
            </div>
            <button type="button" class="showmap active">
                <span><?php echo pll__( 'SHOW MAP' ); ?></span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/map.svg">
            </button>

            <button type="button" class="showmap">
                <span><?php echo pll__( 'HIDE MAP' ); ?></span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/map.svg">
            </button>
        </div>
    </div>

</div>