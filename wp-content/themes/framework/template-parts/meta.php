<?php
/**
 * The template used to load meta elements in header*.php
 *
 * @package framework
 */
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
