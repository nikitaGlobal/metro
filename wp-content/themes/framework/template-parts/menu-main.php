<?php
/**
 * The template used to load the Main Menu in header*.php
 *
 * @package framework
 */
?>

<!-- Main menu -->
<?php $arr_m = array(
	'theme_location' => 'menu-1',
	'container_class' => 'menusContainer',
    'echo' => '0',
);
echo wp_nav_menu( $arr_m ); ?>

<div class="langContainer">
    <?php /*
    <a class="contactContainer" href="tel:<?php the_field('phone', 'option'); ?>">
	    <?php  if ( is_front_page() ) : ?>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone.svg" style="width: 15px;">
        <?php else : ?>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/call.svg" style="width: 15px;">
        <?php endif; ?>
        <span><?php the_field('phone', 'option'); ?></span>
    </a>
*/ ?>
    <a class="whatContainer" href="https://api.whatsapp.com/send?phone=<?php the_field('phone', 'option'); ?>&text=Hi,%20I%20am%20Interested%20in%20Metropolitan%20Properties.%20Kindly%20send%20me%20more%20information%20about%20Pricing,%20Projects%20and%20Availability.%20Thank%20you!">
        <img width="16" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-whatsapp.svg" alt="whatsapp">
        <span><?php the_field('phone', 'option'); ?></span>
    </a>

    <div class="langEl"><ul class="langSwicher"><?php pll_the_languages(array('show_flags' => 1));?></ul></div>

</div>

<!-- End Main menu -->