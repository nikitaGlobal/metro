<?php
/**
 * Template part for displaying posts and page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

$social_post = get_field( 'social_post', 'option' );
?>

<h1><?php the_title(); ?></h1>
<div class="postRead vacancy">
    <div class="postReadL">
        <p class="officeLocation"><?php echo get_field('locate') ?></p>
    </div>

    <div class="postReadR">
        <p class="postedTime"><?php echo 'Posted '.esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' ago'; ?></p>
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/eye.svg" alt="eye">
        <p><?php if ( function_exists( 'the_views' ) ) {
				the_views();
			} ?></p>
    </div>
</div>

<div class="postMain">
	<?php the_content(); ?>
    <div class="postRating">
		<?php if ( $social_post['show_block'] ): ?>
            <div class="postShare">
                <span><?php echo pll__( 'Share' ); ?>:</span>
                <div class="socialIcons">
					<?php foreach ( $social_post['item'] as $item ) : ?>
                        <a href="<?php echo $item['url']; ?>"><i class="icon-<?php echo $item['icon']; ?>"></i></a>
					<?php endforeach; ?>
                </div>
            </div>
		<?php endif; ?>

    </div>
</div>