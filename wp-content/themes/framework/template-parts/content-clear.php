<?php
/**
 * Template part for displaying posts and page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

$social_post = get_field( 'social_post', 'option' );
?>

<h1><?php the_title(); ?></h1>
<div class="postMain clear">
	<?php the_content();
  $images = get_attached_media( 'image', $post->ID ); 
  $video = parse_url(get_field('video', $post->I));
  ?>
    <?php if($images): ?>
    <div class="img-post">
		<?php foreach ( $images as $index ) : ?>
            <a href="<?php echo $index->guid; ?>" data-lightbox="oobject">
                <img class="img-post-item" src="<?php echo $index->guid; ?>" alt="img">
            </a>
		<?php endforeach; ?>
    </div>
    <?php endif; ?>
    <?php if($video['path']): ?>
    <div class="video-post">
		  <iframe width="100%" height="424" src="https://www.youtube.com/embed/<?=$video['path']?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope;" allowfullscreen></iframe>
    </div>
    <?php endif; ?>

</div>