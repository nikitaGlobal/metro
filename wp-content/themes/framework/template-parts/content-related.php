<?php
$read_more = get_field( 'read_more' );
?>
<div class="projectHeading" style="text-align: left;">
    <h2 style="padding-left: 0;"><?php echo pll__( 'Read More' ); ?></h2>
    <div class="middleSlash"></div>
</div>

<div class="row propertiesRow">
    <?php foreach ( $read_more as $item ) : ?>
    <div class="col-md-6">
        <?php if(in_category(array('Video','Видео'), $item['news']->ID)): 
            $video = parse_url(get_field('video',$item['news']->ID));
         ?>
        <div class="blogPost newsTemlate">
            <div class="postVideo">
                <iframe width="100%" height="321" src="https://www.youtube.com/embed/<?=$video['path']?>"
                    frameborder="0" allow="accelerometer; encrypted-media; gyroscope;" allowfullscreen></iframe>
            </div>
            <div class="postContent">
                <p class="blogPostH"><?php echo $item['news']->post_title; ?></p>
            </div>
        </div>
        <?php else: ?>
        <a href="<?php the_permalink($item['news']->ID); ?>" class="blogPostL">
            <div class="blogPost newsTemlate">
                <div class="postImg">
                    <?php if ( get_the_post_thumbnail_url($item['news']->ID) ) : ?>
                    <img src="<?php the_post_thumbnail_url($item['news']->ID); ?>" alt="<?php echo $item['news']->post_title; ?>">
                    <?php else : ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-image.jpg'?>"
                        alt="no images">
                    <?php endif; ?>
                    <div class="postDate"><?php echo date( 'd.m.Y',strtotime( $item['news']->post_date )); ?></div>
                </div>
                <div class="postContent">
                    <p class="blogPostH"><?php echo $item['news']->post_title; ?></p>
                    <div class="blogPostP"><?php echo get_the_excerpt($item['news']->ID); ?></div>
                </div>
            </div>
        </a>
        <?php endif; ?>
    </div>
    <?php endforeach; ?>
</div>