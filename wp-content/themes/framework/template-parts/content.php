<?php
/**
 * Template part for displaying posts and page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package framework
 */

$social_post = get_field( 'social_post', 'option' );
?>

<h1><?php the_title(); ?></h1>
<div class="postRead">
    <div class="postReadL">
		<?php if ( get_field( 'time_to_read' ) ) : ?>
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/book.svg" alt="book">
            <p>Time for reading: <span><?php the_field( 'time_to_read' ) ?></span></p>
		<?php endif; ?>
    </div>

    <div class="postReadR">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/eye.svg" alt="eye">
        <p><?php if ( function_exists( 'the_views' ) ) {
				the_views();
			} ?></p>
    </div>

</div>

<div class="postMain">
	<?php
  //$images = get_attached_media( 'image', $post->ID );
  $video = parse_url(get_field('video', $post->I));
  ?>
    <?php /* if($images): ?>
    <div class="img-post">
		<?php foreach ( $images as $index ) : ?>
            <a href="<?php echo $index->guid; ?>" data-lightbox="oobject">
                <img class="img-post-item" src="<?php echo $index->guid; ?>" alt="img">
            </a>
		<?php endforeach; ?>
    </div>
    <?php endif; */ ?>
    <?php if($video['path']): ?>
    <div class="video-post">
		  <iframe width="100%" height="424" src="https://www.youtube.com/embed/<?=$video['path']?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope;" allowfullscreen></iframe>
    </div>
    <?php endif; ?>
    <?php if ( have_rows( 'content' ) ) : ?>
        <?php while ( have_rows( 'content' ) ) : the_row(); ?>

        <?php if ( get_row_layout() === 'bwi' ) : ?>
            <?php if ( ! empty( get_sub_field( 'cards' ) ) ) : ?>
                <div class="row rowPost">
                    <?php foreach ( get_sub_field( 'cards' ) as $subarr ) : ?>
                        <div class="bwiItem <?=get_sub_field('style');?>">
                                <div class="abtRowTCont">
                                    <div class="abtRowTL">
                                        <img src="<?=$subarr['icon'] ?>" width="50">
                                    </div>
                                    <div class="abtRowTR">
                                        <p><?php echo $subarr['header'] ?></p>
                                    </div>
                                </div>
                                <p class="abtRowBCont"><?php echo $subarr['desc'] ?></p>
                            </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ( get_row_layout() === 'editor' ) : ?>
            <div class="textBlock">
                <?php echo get_sub_field( 'text' ) ?>
            </div>
        <?php endif; ?>

         <?php if (get_row_layout() === 'title') : ?>
             <div class="projectHeading <?php if(get_sub_field( 'style' )=='left'){echo ' left';}?>">
                 <?php if(get_sub_field( 'style' )!='left'){?><div class="middleSlash"></div><?php } ?>
                 <h2><?php echo get_sub_field( 'title' ) ?></h2>
                 <div class="middleSlash"></div>
             </div>
         <?php endif; ?>

        <?php if (get_row_layout() === 'steps') : ?>
            <div class="verticalStep">
                <?php if ( ! empty( get_sub_field( 'item' ) ) ) : $count = get_sub_field( 'start' ); ?>
                    <?php foreach ( get_sub_field( 'item' ) as $subarr ) : ?>
                        <div class="stepItem">
                                <div class="stepNum">
                                    <span><?php echo pll__( 'Step' );?> <span class="blue"><?php echo $count ?></span></span>
                                </div>
                                <?php if($subarr['head']) { ?><p class="verticalSepP1"><?php echo $subarr['head'] ?></p><?php } ?>
                                <?php if($subarr['text']) { ?><p class="verticalSepP2"><?php echo $subarr['text'] ?></p><?php } ?>
                        </div>
                        <?php $count ++; endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>

    <div class="nextPrev">
		<?php previous_post_link( '%link',
			'<img src="/wp-content/themes/framework/assets/images/arrowleft.svg" alt="Previous"> ' . pll__( 'Previous article' ) ); ?>
		<?php next_post_link( '%link',
			pll__( 'Next article' ) . ' <img src="/wp-content/themes/framework/assets/images/arrowright.svg" alt="Next">' ); ?>
    </div>


    <div class="postRating">
        <div class="postReviews">
			<?php rating(); ?>
        </div>

		<?php if ( $social_post['show_block'] ): ?>
            <div class="postShare">
                <span><?php echo pll__( 'Share' ); ?>:</span>
                <div class="socialIcons">
					<?php foreach ( $social_post['item'] as $item ) : ?>
                        <a href="<?php echo $item['url']; ?>"><i class="icon-<?php echo $item['icon']; ?>"></i></a>
					<?php endforeach; ?>
                </div>
            </div>
		<?php endif; ?>

    </div>
</div>