<?php
/**
 * framework functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package framework
 */

if ( ! function_exists( 'frame_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function frame_setup() {

		// Register the navigation menus.
		locate_template( '/inc/menus.php', true );

		// Register sidebars
		locate_template( '/inc/sidebars.php', true );

		// Load scripts
		locate_template( '/inc/scripts.php', true );

		// Load the CSS
		locate_template( '/inc/stylesheets.php', true );

		// Clean up header output
		locate_template( '/inc/cleanup.php', true );

		// Register polylang strings translation
		locate_template( '/inc/pll-register-string.php', true );

		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on framework, use a find and replace
		 * to change 'frame' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'frame', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
        add_image_size( 'devlogo', 129, 129, true );
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
//		add_theme_support( 'custom-logo', array(
//			'height'      => 250,
//			'width'       => 250,
//			'flex-width'  => true,
//			'flex-height' => true,
//		) );
	}
endif;
add_action( 'after_setup_theme', 'frame_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function frame_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'frame_content_width', 640 );
}

add_action( 'after_setup_theme', 'frame_content_width', 0 );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

get_template_part( 'assets/ratings/rating' );
get_template_part( 'inc/widgets/social' );
get_template_part( 'inc/widgets/areaSelect' );
//get_template_part( 'inc/widgets/customPage' );
get_template_part( 'inc/widgets/customCat' );

function debag( $arr ) {
	echo '<b> DEBAG:</b><br><br> <pre>' . print_r( $arr, 1 ) . '</pre>';
}//Для дебага кода

add_filter( 'excerpt_more', function ( $more ) {
	return '...';
} );

add_filter( 'excerpt_length', function () {
	return 19;
} );

add_filter( 'nav_menu_link_attributes', 'add_class_menu_tag_a', 10, 4 );
function add_class_menu_tag_a( $atts, $item, $args, $depth ) {
    $atts += [ "class" => "menuItem"];

	if ( 1 === $depth ) {
		$atts['class'] .= ' depht';
	}
	return $atts;
}

add_filter( 'navigation_markup_template', 'my_pagination_template', 10, 2 );
function my_pagination_template( $template, $class ) {
	return '%3$s';
}

function wp_nav_lang() {
	$multilang = pll_the_languages( array( 'raw' => 1,'show_flags' => 1, ) );
	$html      = '';

	foreach ( $multilang as $key => $item ) {
		if ( $item['current_lang'] ) {
			$class = 'langItem active';
		} else {
			$class = 'langItem';
		}

		$html .= '<li class="lang-li"><a href="' . $item['url'] . '" class="' . $class . '">' . $item['name'] . '</a></li>';
	}

	echo $html;
}

/**
 * Registr custom posts
 */
add_action( 'init', 'my_registr_custom_post' );
function my_registr_custom_post() {
	register_post_type( 'estate_object', array(
		'labels'             => array(
			'name'               => 'Objects', // Основное название типа записи
			'singular_name'      => 'Object',  // отдельное название записи типа Book
			'add_new'            => 'Add Object',
			'add_new_item'       => 'Add Object',
			'edit_item'          => 'Edit Object',
			'new_item'           => 'New Object',
			'view_item'          => 'View Object',
			'search_items'       => 'Find Object',
			'not_found'          => 'Not Found Object',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Objects',

		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'show_in_rest'       => true,
		'menu_position'      => null,
		'menu_icon'          => 'dashicons-building',
		'supports'           => array( 'title', 'author', 'editor', 'thumbnail', 'comments' ),
	) );

	register_taxonomy( 'departments', [ 'departments_object' ], [
		'label'        => '',
		'labels'       => [
			'name'              => 'Departments',
			'singular_name'     => 'Department',
			'search_items'      => 'Search',
			'all_items'         => 'All',
			'view_item '        => 'View',
			'parent_item'       => 'Parent',
			'parent_item_colon' => 'Parent:',
			'edit_item'         => 'Edit',
			'update_item'       => 'Update',
			'add_new_item'      => 'Add New',
			'new_item_name'     => 'New Department',
			'menu_name'         => 'Departments',
		],
		'description'  => '',
		'public'       => true,
		'hierarchical' => true,
		'publicly_queryable' => false,
		'rewrite'           => false,
		'show_in_nav_menus'  => false,
		'capabilities'      => array(),
		'meta_box_cb'       => null,
		'show_admin_column' => false,
		'show_in_rest'      => null,
		'rest_base'         => null,
	]);
	register_post_type( 'agents', array(
		'labels'             => array(
			'name'               => 'Agents', // Основное название типа записи
			'singular_name'      => 'Agent',  // отдельное название записи типа Book
			'add_new'            => 'Add Agent',
			'add_new_item'       => 'Add Agent',
			'edit_item'          => 'Edit Agent',
			'new_item'           => 'New Agent',
			'view_item'          => 'View Agent',
			'search_items'       => 'Find Agent',
			'not_found'          => 'Not Found Agent',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Agents',

		),
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'show_in_nav_menus'  => false,
		'hierarchical'       => false,
		'show_in_rest'       => true,
		'menu_position'      => null,
		'taxonomies'		 => array('departments'),
		'menu_icon'          => 'dashicons-groups',
		'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', 'comments' ),
	));
    register_post_type( 'vacancies', array(
        'labels'             => array(
            'name'               => 'Vacancies', // Основное название типа записи
            'singular_name'      => 'Vacancy',  // отдельное название записи типа Book
            'add_new'            => 'Add Vacancy',
            'add_new_item'       => 'Add Vacancy',
            'edit_item'          => 'Edit Vacancy',
            'new_item'           => 'New Vacancy',
            'view_item'          => 'View Vacancy',
            'search_items'       => 'Find Vacancy',
            'not_found'          => 'Not Found Vacancy',
            'not_found_in_trash' => 'Not found in trash',
            'parent_item_colon'  => '',
            'menu_name'          => 'Vacancies',

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'show_in_rest'       => true,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-megaphone',
        'supports'           => array( 'title', 'editor','excerpt'),
    ));

    register_taxonomy( 'emirate', [ 'estate_object' ], [
		'label'        => '',
		'labels'       => [
			'name'              => 'Emirates',
			'singular_name'     => 'Emirate',
			'search_items'      => 'Search',
			'all_items'         => 'All',
			'view_item '        => 'View',
			'parent_item'       => 'Parent',
			'parent_item_colon' => 'Parent:',
			'edit_item'         => 'Edit',
			'update_item'       => 'Update',
			'add_new_item'      => 'Add New',
			'new_item_name'     => 'New Emirate Name',
			'menu_name'         => 'Emirate',
		],
		'description'  => '',
		'public'       => true,
		'hierarchical' => true,

		'rewrite'           => true,
		'capabilities'      => array(),
		'meta_box_cb'       => null,
		'show_admin_column' => false,
		'show_in_rest'      => null,
		'rest_base'         => null,
	] );

	register_taxonomy( 'property_type', [ 'estate_object' ], [
		'label'        => '',
		'labels'       => [
			'name'              => 'Property types',
			'singular_name'     => 'Property type',
			'search_items'      => 'Search',
			'all_items'         => 'All',
			'view_item '        => 'View',
			'parent_item'       => 'Parent',
			'parent_item_colon' => 'Parent:',
			'edit_item'         => 'Edit',
			'update_item'       => 'Update',
			'add_new_item'      => 'Add New',
			'new_item_name'     => 'New property type Name',
			'menu_name'         => 'Property type',
		],
		'description'  => '',
		'public'       => true,
		'hierarchical' => false,

		'rewrite'           => true,
		'capabilities'      => array(),
		'meta_box_cb'       => null,
		'show_admin_column' => false,
		'show_in_rest'      => null,
		'rest_base'         => null,
	] );
}

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false,
	) );
}


add_filter( 'wpcf7_form_elements', function ( $content ) {
	$content = preg_replace( '/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i',
		'\2', $content );

	return $content;
} );

//add_action('pmxi_after_post_import', 'my_after_post_import', 10, 1);
function my_after_post_import( $import_id ) {
	/*
	 * Берем айді поста з куки
	 * Берем дату поста і теперіфшню таду
	 * Якщо не збігаються то ставим посту драфт
	 */
}

//add_action('pmxi_saved_post', 'my_saved_post', 10, 1);
function my_saved_post( $post_id ) {
	//Пишем айді поста в куку
}

add_filter( 'wp_revisions_to_keep', 'my_revisions_to_keep' );
function my_revisions_to_keep( $revisions ) {
	return 1;
}

add_action( 'pmxi_after_xml_import', 'set_post_language_and_status', 10 );
function set_post_language_and_status() {

	$query = new WP_Query( array(
		'post_type'      => 'estate_object',
		'posts_per_page' => - 1,
	) );
	while ( $query->have_posts() ) : $query->the_post();
		if ( date( 'd' ) !== get_the_date( 'd', get_the_ID() ) ) {
			wp_update_post( array(
				'ID'          => get_the_ID(),
				'post_status' => 'draft',
			) );
		}

		pll_set_post_language( get_the_ID(), 'en' );
	endwhile;
	wp_reset_query();

}

if ( wp_doing_ajax() ) {
	add_action( 'wp_ajax_ajax_load_agents', 'ajax_load_agents' );
	add_action( 'wp_ajax_nopriv_ajax_load_agents', 'ajax_load_agents' );
}
function ajax_load_agents() {
	$count = $_POST['count'];
	$page  = $_POST['current_page'];


	$agent_posts = new WP_Query( array(
		'post_type'      => 'agents',
		'posts_per_page' => $count,
		'paged'          => $page + 1,
	) );

	while ( $agent_posts->have_posts() ) : $agent_posts->the_post(); ?>
        <div class="col-md-4">
            <div class="agentImg">
				<?php $pic = get_field( 'photo' )['url'] ? get_field( 'photo' )['url'] : '/wp-content/themes/framework/assets/images/no-image.jpg'; ?>
                <img src="<?php echo $pic; ?>">
                <div class="agentContact">
                    <div class="agentContactInner">
                        <p><?php the_field( 'full_name' ) ?></p>
                        <p><span>Speaks:</span> <?php the_field( 'languages' ) ?></p>
                    </div>
					<?php if ( get_field( 'phone' ) ) : ?>
                        <div class="agentContactInnerCon">
                            <a href="tel:<?php the_field( 'phone' ) ?>">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/Whatsapp.svg">
                            </a>
                        </div>
					<?php endif; ?>
                </div>
            </div>
        </div>
	<?php endwhile;
	wp_reset_query();

	exit();
}

if ( wp_doing_ajax() ) {
	add_action( 'wp_ajax_ajax_load_object', 'ajax_load_object' );
	add_action( 'wp_ajax_nopriv_ajax_load_object', 'ajax_load_object' );
}
function ajax_load_object() {
	$count = $_POST['count'];
	$page  = $_POST['current_page'];


	$agent_posts = new WP_Query( array(
		'post_type'      => 'estate_object',
		'posts_per_page' => $count,
		'paged'          => $page + 1,
//		'meta_query' => array(
//			array(
//				'key' => 'featured',
//				'value' => 1
//			)
//		)
	) );

	while ( $agent_posts->have_posts() ) : $agent_posts->the_post(); ?>
        <div class="col-md-4">
			<?php get_template_part( 'template-parts/content', 'object' ); ?>
        </div>
	<?php endwhile;
	wp_reset_query();

	exit();
}

if ( wp_doing_ajax() ) {
	add_action( 'wp_ajax_ajax_load_community', 'ajax_load_community' );
	add_action( 'wp_ajax_nopriv_ajax_load_community', 'ajax_load_community' );
}
function ajax_load_community() {
	$term_id = $_POST['term_id'];

	$comm_terms = get_terms( array(
		'taxonomy'   => 'emirate',
		'hide_empty' => true,
		'child_of'   => $term_id,
	) ); ?>

    <select id="loc_select" name="community[]" class="community-select form-control location" multiple="multiple">
		<?php if ( ! empty( $comm_terms ) ) : ?>
			<?php foreach ( $comm_terms as $termobj ) : ?>
                <option value="<?php echo $termobj->term_id; ?>"><?php echo $termobj->name; ?></option>
			<?php endforeach; ?>
		<?php endif; ?>
    </select>

	<?php
	exit();
}

if ( wp_doing_ajax() ) {
	add_action( 'wp_ajax_object_ajax_home', 'object_ajax_home' );
	add_action( 'wp_ajax_nopriv_object_ajax_home', 'object_ajax_home' );
}
function object_ajax_home() {
	$term_id = $_POST['term_id'];

	$query = new WP_Query( array(
		'post_type'      => 'estate_object',
		'posts_per_page' => 6,
		'order'          => 'ASC',
		'post_status'    => 'publish',
		'tax_query'      => [
			[
				'taxonomy' => 'emirate',
				'field'    => 'id',
				'terms'    => $term_id,
			],
		],
	) );

	while ( $query->have_posts() ) : $query->the_post(); ?>
        <div class="col-md-4">
			<?php get_template_part( 'template-parts/content', 'object' ); ?>
        </div>
	<?php endwhile;
	wp_reset_query();

	exit();
}

if ( wp_doing_ajax() ) {
	add_action( 'wp_ajax_ajax_object_sort', 'ajax_object_sort' );
	add_action( 'wp_ajax_nopriv_ajax_object_sort', 'ajax_object_sort' );
}
function ajax_object_sort() {
	$orderby      = $_POST['orderby'];
	$community    = $_POST['community'];
	$propertyType = $_POST['propertyType'];
	$minBedroom   = $_POST['minBedroom'];
	$maxBedroom   = $_POST['maxBedroom'];
	$minPrice     = $_POST['minPrice'];
	$maxPrice     = $_POST['maxPrice'];
	$paged        = $_POST['paged'];
	$arr          = array(
		'post_type' => 'estate_object',
	);

	if ( 'DATE' === $orderby ) {
		$arr['orderby'] = 'date';
		$arr['order']   = 'ASC';
		$arr['paged']   = $paged;

	} elseif ( 'ASC' === $orderby || 'DESC' === $orderby ) {
		$arr['orderby'] = 'date';
		$arr['order']   = $orderby;
		$arr['paged']   = $paged;

	} elseif ( 'views-less' === $orderby || 'price-less' === $orderby ) {
		$orderby = substr( $orderby, 0, 5);
		$arr['orderby']  = 'meta_value_num';
		$arr['order']    = 'ASC';
		$arr['meta_key'] = $orderby;
		$arr['paged']    = $paged;

	} elseif ( 'views-more' === $orderby || 'price-more' === $orderby ) {
		$orderby = substr( $orderby, 0, 5);
		$arr['orderby']  = 'meta_value_num';
		$arr['order']    = 'DESC';
		$arr['meta_key'] = $orderby;
		$arr['paged']    = $paged;
    }

	$arr['tax_query'] = array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'emirate',
			'field'    => 'id',
			'terms'    => $community,
		),
		array(
			'taxonomy' => 'property_type',
			'field'    => 'id',
			'terms'    => $propertyType,
		),
	);

	$arr['meta_query'] = array(
		'relation' => 'AND',
		array(
			'key'     => 'price',
			'value'   => array( $minPrice, $maxPrice ),
			'compare' => 'BETWEEN',
			'type'    => 'NUMERIC',
		),
		array(
			'key'     => 'bedrooms',
			'value'   => array( $minBedroom, $maxBedroom ),
			'compare' => 'BETWEEN',
			'type'    => 'NUMERIC',
		),
	);

	$query = new WP_Query( $arr );
	while ( $query->have_posts() ) : $query->the_post(); ?>
        <div class="col-md-6">
			<?php get_template_part( 'template-parts/content', 'object' ); ?>
        </div>
	<?php endwhile;
	wp_reset_query();
	exit();
}
function shortcode_convert_ft( $atts ) {
	$convert = $_COOKIE['convert-ft'];
	if ( 'M' === $convert ) {
		$meter = $atts['sq'] / 10.764;
		return (int) $meter . ' SQM';
	} else {
		return (int) $atts['sq'] . ' SQF';
	}
}
add_shortcode( 'convert_ft', 'shortcode_convert_ft' );

//Social Image Banner
function add_images( $object ) {
    $newImag = get_field('sImg');
    if($newImag){
        $object->add_image(array( 'url' => $newImag, 'height' => 600, 'width' => 1200 ));
    }
}
add_action( 'wpseo_add_opengraph_images', 'add_images' );

function get_link_by_slug($slug, $type = 'post'){
    $lang_slug = pll_current_language();
    $post = get_page_by_path($slug, OBJECT, $type);
    $id = ($lang_slug) ? pll_get_post($post->ID, $lang_slug) : $post->ID;
    return get_permalink($id);
}

function get_image_id($image_ttl) {
    global $wpdb;
    if ('' == $image_ttl) return false;
    $attachment = $wpdb->get_results("SELECT $wpdb->posts.guid FROM $wpdb->posts WHERE post_title LIKE '%{$image_ttl}%';", ARRAY_N);
    return $attachment;

}
add_action('wp_enqueue_scripts', 'ngWpEnqueueScripts', 9999);
function ngWpEnqueueScripts()
{
    $path=get_stylesheet_directory_uri().'/assets/css/ng/';
    $styles=array('/custom.css','/app/modules.css','/style.css');
    $scripts=array('/assets/js/custom.js');
    foreach ($styles as $style) {
        $script=$path . $style;
        wp_register_style('ng'.crc32($script), $script);
        wp_enqueue_style('ng'.crc32($script));
    }
    
}