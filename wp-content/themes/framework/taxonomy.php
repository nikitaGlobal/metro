<?php
get_header();

if ( ! empty( $_GET ) ):
	$searchType   = $_GET['searchType'];
	$community    = $_GET['community'];
	$propertyType = $_GET['PropertyType'];
	$minBedroom   = $_GET['MinBedroom'];
	$maxBedroom   = $_GET['MaxBedroom'];
	$minPrice     = $_GET['MinPrice'];
	$maxPrice     = $_GET['MaxPrice'];
	$paged        = get_query_var( 'paged' ) ? : 1;
	?>

    <script>
      var community = <?php echo json_encode( $community );?>,
        propertyType = <?php echo $propertyType;?>,
        minBedroom = <?php echo $minBedroom;?>,
        maxBedroom = <?php echo $maxBedroom;?>,
        minPrice = <?php echo $minPrice;?>,
        maxPrice = <?php echo $maxPrice;?>,
        paged = <?php echo $paged;?>;
    </script>

	<?php
	if ( 2 >= $paged && $_COOKIE['sorting'] ):
		if ( 'views' === $_COOKIE['sorting'] || 'price' === $_COOKIE['sorting'] ):
			$arr = array(
				'post_type'  => 'estate_object',
				'orderby'    => 'meta_value_num',
				'order'      => 'ASC',
				'meta_key'   => $_COOKIE['sorting'],
				'paged'      => $paged,
				'tax_query'  => [
					'relation' => 'AND',
					[
						'taxonomy' => 'emirate',
						'field'    => 'id',
						'terms'    => $community,
					],
					[
						'taxonomy' => 'property_type',
						'field'    => 'id',
						'terms'    => $propertyType,
					],
				],
				'meta_query' => [
					'relation' => 'AND',
					[
						'key'     => 'price',
						'value'   => array( $minPrice, $maxPrice ),
						'compare' => 'BETWEEN',
						'type'    => 'NUMERIC',
					],
					[
						'key'     => 'bedrooms',
						'value'   => array( $minBedroom, $maxBedroom ),
						'compare' => 'BETWEEN',
						'type'    => 'NUMERIC',
					],
				],
			);

        elseif ( 'ASC' === $_COOKIE['sorting'] || 'DESC' === $_COOKIE['sorting'] ):
			$arr = array(
				'post_type'  => 'estate_object',
				'orderby'    => 'date',
				'order'      => $_COOKIE['sorting'],
				'paged'      => $paged,
				'tax_query'  => [
					'relation' => 'AND',
					[
						'taxonomy' => 'emirate',
						'field'    => 'id',
						'terms'    => $community,
					],
					[
						'taxonomy' => 'property_type',
						'field'    => 'id',
						'terms'    => $propertyType,
					],
				],
				'meta_query' => [
					'relation' => 'AND',
					[
						'key'     => 'price',
						'value'   => array( $minPrice, $maxPrice ),
						'compare' => 'BETWEEN',
						'type'    => 'NUMERIC',
					],
					[
						'key'     => 'bedrooms',
						'value'   => array( $minBedroom, $maxBedroom ),
						'compare' => 'BETWEEN',
						'type'    => 'NUMERIC',
					],
				],
			);
		endif;
	else:
		$arr = array(
			'post_type'  => 'estate_object',
			'orderby'    => 'date',
			'order'      => 'DESC',
			'paged'      => $paged,
			'tax_query'  => [
				'relation' => 'AND',
				[
					'taxonomy' => 'emirate',
					'field'    => 'id',
					'terms'    => $community,
				],
				[
					'taxonomy' => 'property_type',
					'field'    => 'id',
					'terms'    => $propertyType,
				],
			],
			'meta_query' => [
				'relation' => 'AND',
				[
					'key'     => 'price',
					'value'   => array( $minPrice, $maxPrice ),
					'compare' => 'BETWEEN',
					'type'    => 'NUMERIC',
				],
				[
					'key'     => 'bedrooms',
					'value'   => array( $minBedroom, $maxBedroom ),
					'compare' => 'BETWEEN',
					'type'    => 'NUMERIC',
				],
			],
		);
	endif;

	query_posts( $arr );

endif;
?>

<?php get_template_part( 'template-parts/content', 'object_search' ); ?>
    <div class="clearfix"></div>
    <div class="featureProjects estate-object" style="padding-top: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 order-1 order-sm-0">
                    <div id="object-post" class="row propertiesRow" style="padding-top: 0;">
                        <script> var locationArr = []; </script>
						<?php $ref = 1;

						if ( have_posts() ) :
							while ( have_posts() ) : the_post(); ?>
                                <div class="col-md-6">
									<?php get_template_part( 'template-parts/content', 'object' ); ?>
                                </div>
                                <script>
                                  locationArr.unshift( {
                                    lat: <?php echo substr( get_field( 'lat' ), 0, - 1 ) . random_int( 0, 9 ); ?>,
                                    lng: <?php echo substr( get_field( 'lng' ), 0, - 1 ) . random_int( 0, 9 );?>,
                                    ref: <?php echo $ref;?>,
                                    txt: '<?php the_field( 'unit_reference' )?>',
                                  } );
                                </script>
								<?php $ref ++;
							endwhile;
						else:
							get_template_part( 'template-parts/content', 'none' );
						endif; ?>
                    </div>
                </div>

				<?php get_template_part( 'template-parts/content', 'mapbox' ); ?>

            </div>

        </div>
    </div>


    <script>
      var geocoder, map, mylocation, marker, MarkerIndex, infowindow;
      var $window = jQuery( window );
      var windowsize = $window.width();
      var allMarkers = new Array();

      function initMap () {

        map = new google.maps.Map( document.getElementById( 'mapProperty' ), {
          zoom: 12,
          center: new google.maps.LatLng( 25.1127555, 55.1887018 ),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          fullscreenControl: false,
          mapTypeControl: false,
        } );
        for ( var i = 0; i < locationArr.length; i++ ) {
          var latlng = new google.maps.LatLng( locationArr[i]['lat'], locationArr[i]['lng'] );

          var infoContent = '<p style="margin-bottom:0;font-weight:bold;">' + locationArr[i]['txt'] + '</p>';
          infowindow = new google.maps.InfoWindow( {
            content: infoContent,
          } );
          infowindow.setPosition( latlng );
          infowindow.open( map );

        }
      }
    </script>

<?php
get_template_part( 'template-parts/pagination' );
get_footer();