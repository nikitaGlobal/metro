
<?php get_header(); ?>

<div class="featureProjects" style="padding-top: 0px;">
	<div class="container">

		<div class="postdetails">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content' );
				endwhile;
			endif;
			?>
		</div>

	</div>
</div>

<?php get_footer();