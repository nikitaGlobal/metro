<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framework
 */

$newsletter = get_field( 'newsletter', 'option' );
$copyright = get_field( 'copyright', 'option' );
$privacy_policy_url = get_field( 'privacy_policy_url', 'option' );
$terms_of_use_url = get_field( 'terms_of_use_url', 'option' );
?>

<div class="newsLetter <?php if(is_page('contact-us') || is_page_template(  array('template-services.php','template-join.php','template-sell.php', 'single-property-listing-template-2.php' )) || is_singular( 'vacancies' )) echo "noPd"; ?>">
    <div class="container">
        <h4><?php echo pll__('Our newsletter');?></h4>
        <p><?php echo pll__('Sign up for our weekly newsletter for market updates!');?></p>
    <div class="newsletterF">
        <?php
        if ( 'ru' === pll_current_language() ){ echo $newsletter['form_ru']; }
        if ( 'ar' === pll_current_language() ){ echo $newsletter['form_ar']; }
        else {
            echo $newsletter['form_en'];
        } ?>
    </div>
    <div class="clearfix"></div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row">

	        <?php get_sidebar();?>

            <div class="hline"></div>
        </div>
    </div>

    <div class="container copyRightCont">
        <div class="row">
            <?php /*
            <!--
            <div class="col-md-7">
                <p><?php //echo date('Y'); echo "\n\r". pll__('All Rights Reserved. Metropolitan Premium Properties');?>&copy;</p>
            </div>
            -->
            */ ?>

	        <?php if ( $privacy_policy_url ): ?>
                <div class="col-md-3">
                    <a href="/<?php echo $privacy_policy_url->post_name; ?>"><?php echo $privacy_policy_url->post_title;?></a>
                </div>
	        <?php endif; ?>

	        <?php if ( $terms_of_use_url ): ?>
                <div class="col-md-3">
                    <a href="/<?php echo $terms_of_use_url->post_name; ?>"><?php echo $terms_of_use_url->post_title;?></a>
                </div>
	        <?php endif; ?>

        </div>
    </div>
</div>
<?php if(isset($GLOBALS["headBtn"])): ?>
    <!-- Modal -->
    <div id="headBtnModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">
                        <?php echo do_shortcode('[contact-form-7 id="28983"]'); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>
<?php if(isset($GLOBALS["fwForm"])): ?>
    <!-- Modal -->
    <div id="fwBtnModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/menuclose.svg" style="width: 20px;">
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modalH"><?php echo get_field( 'footer_header' ) ?></p>
                    <div class="footerForm" style="padding:20px 30px;position: relative;">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/PAttern3.svg" class="modalPat">
                        <?php echo do_shortcode($GLOBALS["fwForm"]); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>
<?php wp_footer(); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");

    ym(61700275, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/61700275" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script src="//code.jivosite.com/widget/fKvFbM8J7Z" async></script>
</body>
</html>
