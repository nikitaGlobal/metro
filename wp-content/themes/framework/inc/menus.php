<?php

/**
 * Register the navigation menus. This theme uses wp_nav_menu() in one locations.
 */
register_nav_menus( array(
	'menu-1' => esc_html__( 'Primary', 'fireworks' ),
) );