<?php
/**
 * Class social
 *
 */

class social extends WP_Widget {

	/**
	 * Sets up a new Social url widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		// Запускаем родительский класс
		parent::__construct(
			"social_widget",                               //идентификатор_виджета
			"Social",                                      //Название виджета
			array( "description" => "Contact for social" ) //Описание виджета
		);
	}


	/**
	 * Outputs the content for the current Social url widget instance.
	 *
	 * @param array $args     Display arguments including .
	 * @param array $instance Settings for the current Custom categories widget instance.
	 */
	public function widget( $args, $instance ) {
		$instagram = $instance["instagram"];
		$facebook  = $instance["facebook"];
		$twitter   = $instance["twitter"];
		$youtube   = $instance["youtube"];


		if ( ! empty( $instagram ) or ! empty( $facebook ) or ! empty( $twitter ) or ! empty( $youtube ) ): ?>
            <div class="socialIcons">

				<?php if ( ! empty( $instagram ) ): ?>
                    <a class="instagram-social" href="<?php echo $instagram; ?>">
                        <i class="icon-instagram"></i>
                    </a>
				<?php endif;

				if ( ! empty( $facebook ) ): ?>
                    <a class="facebook-social" href="<?php echo $facebook; ?>">
                        <i class="icon-facebook"></i>
                    </a>
				<?php endif;

				if ( ! empty( $twitter ) ): ?>
                    <a class="twitter-social" href="<?php echo $twitter; ?>">
                        <i class="icon-twitter"></i>
                    </a>
				<?php endif;

				if ( ! empty( $youtube ) ): ?>
                    <a class="youtube-social" href="<?php echo $youtube; ?>">
                        <i class="icon-youtube"></i>
                    </a>
				<?php endif; ?>
            </div>
		<?php endif;

	}

	/**
	 * Handles updating settings for the current Social url widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance["instagram"] = htmlentities( $new_instance["instagram"] );
		$instance["facebook"]  = htmlentities( $new_instance["facebook"] );
		$instance["twitter"]   = htmlentities( $new_instance["twitter"] );
		$instance["youtube"]   = htmlentities( $new_instance["youtube"] );

		return $instance;
	}

	/**
	 * Outputs the settings form for the Categories widget.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$instagram = "";
		$facebook  = "";
		$twitter   = "";
		$youtube   = "";

		// если instance не пустой, достанем значения
		if ( ! empty( $instance ) ) {
			$instagram = $instance["instagram"];
			$facebook  = $instance["facebook"];
			$twitter   = $instance["twitter"];
			$youtube   = $instance["youtube"];
		}
		?>

		<?php
		$instagramId   = $this->get_field_id( "instagram" );
		$instagramName = $this->get_field_name( "instagram" );
		?>
        <p>
            <label for="<?php echo $instagramId; ?>">Instagram</label><br>
            <input id="<?php echo $instagramId ?>" type="text" name="<?php echo $instagramName; ?>" value="<?php echo $instagram; ?>" style="width:100%;">
        <p/>

		<?php
		$facebookId   = $this->get_field_id( "facebook" );
		$facebookName = $this->get_field_name( "facebook" );
		?>
        <p>
            <label for="<?php echo $facebookId; ?>">Facebook</label><br>
            <input id="<?php echo $facebookId ?>" type="text" name="<?php echo $facebookName; ?>" value="<?php echo $facebook; ?>" style="width:100%;">
        <p/>

		<?php
		$twitterId   = $this->get_field_id( "twitter" );
		$twitterName = $this->get_field_name( "twitter" );
		?>
        <p>
            <label for="<?php echo $twitterId; ?>">Twitter</label><br>
            <input id="<?php echo $twitterId ?>" type="text" name="<?php echo $twitterName; ?>" value="<?php echo $twitter; ?>" style="width:100%;">
        <p/>

		<?php
		$youtubeId   = $this->get_field_id( "youtube" );
		$youtubeName = $this->get_field_name( "youtube" );
		?>
        <p>
            <label for="<?php echo $youtubeId; ?>">Youtube</label><br>
            <input id="<?php echo $youtubeId ?>" type="text" name="<?php echo $youtubeName; ?>" value="<?php echo $youtube; ?>" style="width:100%;">
        <p/>

		<?php
	}

}

// Регистрация класса виджета
add_action( "widgets_init", function() {
	register_widget( "social" );
} );