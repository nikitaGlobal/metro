<?php

class areaSelect extends WP_Widget {

	/**
	 * Sets up a new Area widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		// Запускаем родительский класс
		parent::__construct(
			"areaSelect_widget",                                 //идентификатор_виджета
			"areaSelect",                                        //Название виджета
			array( "description" => "Area Select SQ FT / SQ M" ) //Описание виджета
		);
	}


	/**
	 * Outputs the content for the current Area widget instance.
	 *
	 * @param array $args     Display arguments including .
	 * @param array $instance Settings for the current Custom categories widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! $_COOKIE['convert-ft'] ){
			$convert_active = 'active' ;
		}
		?>

        <div class="areaSelect">
	        <button type="button" title="feet" data-convert="F" class="areaSelect-btn <?php echo $convert_active;?>">SQ FT</button>
	        <button type="button" title="meters" data-convert="M" class="areaSelect-btn">SQ M</button>
        </div>

		<?php
	}


}

// Register widget class
add_action( "widgets_init", function() {
	register_widget( "areaSelect" );
} );