<?php

/**
 * Loads the theme's javascript
 *
 */
function fireworks_scripts() {

	wp_enqueue_script( 'fireworks-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'fireworks-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '4.3.1', true );

	if ( is_singular() && ! is_front_page() ) {
		wp_enqueue_script( 'rating', get_template_directory_uri() . '/assets/ratings/rating.js', array( 'jquery' ), '',true );
	}

	if ( is_front_page() || is_tax() ){
		wp_enqueue_script( 'select2', get_template_directory_uri() . '/assets/js/select2.min.js', array(), '', true );
	}

	if ( is_tax() ) {
		wp_enqueue_script( 'googlemaps', '//maps.googleapis.com/maps/api/js?key=AIzaSyD__FJYhUXG23iDkRUjm8Rinm0lByPJ-3E&callback=initMap', array(),'', true );
	}

	wp_enqueue_script( 'cookiejs', get_template_directory_uri() . '/assets/js/js.cookie.min.js',array( 'jquery' ), '2.2.1', true );
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js',array( 'jquery' ), '2.2.1', true );
	
	if (is_tax( 'plan_types' )){
		wp_enqueue_script( 'find-replace', get_template_directory_uri() . '/assets/js/findandreplace.js',array( 'jquery' ), '2.2.1', true );
	}
	wp_enqueue_script( 'scripts-site', get_template_directory_uri() . '/assets/js/scripts.js',array( 'jquery', 'bootstrap', 'cookiejs' ), '1.0.0', true );
	



//	if ( ! is_page_template('template-contact.php') ){
//		wp_dequeue_script('contact-form-7');
//	}
    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '',true );

}

add_action( 'wp_enqueue_scripts', 'fireworks_scripts' );