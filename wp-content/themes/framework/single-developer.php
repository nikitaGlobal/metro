<?php
    get_header();
    if (have_posts()) {
        the_post();
        $developerID        = get_the_id();
        $args               = array(
            'post_type'      => 'properties',
            'posts_per_page' => 999,
            //     'post__not_in'   => array($currentProperty),
            'meta_query'     => array(
                array(
                    'key'     => 'of_property_developer',
                    'value'   => $developerID,
                    'compare' => 'IN',
                    //'type'    => 'NUMERIC'
                )
            )
        );
        $args1              = $args;
        $args1['tax_query'] = array(
            array(
                'taxonomy' => 'plan_types',
                'field'    => 'slug',
                'terms'    => array('off_plan_properties')
            )
        );
        $areas              = get_transient('areas' . $developerID);
        $types              = get_transient('types' . $developerID);
        if ( ! $areas || ! $types) {
            $query = new WP_Query($args);
            while ($query->have_posts()) {
                $query->the_post();
                $terms = wp_get_post_terms(get_the_id(), 'property_categ');
                foreach ($terms as $term) {
                    $types[$term->term_id] = $term->slug;

                }
                $areaid = get_field('of_associated_area')->ID;
                if ($areaid > 0) {
                    $areas[$areaid] = get_field('of_associated_area')->post_title;
                }
                $keys = get_fields();
                $b    = 1;
            }
            set_transient('areas' . $developerID, $areas, 600);
            set_transient('types' . $developerID, $types, 600);
            wp_reset_postdata();
            wp_reset_query();
        }
        ?>
        <section class="p2-f-screen"
                 style="background-image:<?php
                     $image = get_field('cover_image');
                     if ($image) {
                         echo 'url(' . $image . ')';
                     } else {
                         echo 'none';
                     }
                 ?>!important;">
            <div class="container d-flex h-100 align-items-end">
                <div class="p2-f-screen__text col-12">
                    <div class="d-flex align-items-lg-center align-items-start justify-content-start flex-column flex-lg-row">
                        <div class="d-flex align-items-start align-items-lg-center flex-column flex-lg-row">
                            <img class="mw-100 mr-5"
                                 src="<?php the_post_thumbnail_url('devlogo'); ?>"
                                 alt="" style="background-color: #fff;">
                            <h1 class="offplan-property-title">
                                <?php the_title(); ?>
                            </h1>
                        </div>
                        <div class="ml-0 ml-lg-auto  mr-auto mr-lg-5">
                            <button>
                                KNOW MORE
                            </button>
                        </div>
                    </div>
                    
                    <?php if (have_rows('payment_plan')) { ?>
                        <div class="offplan-special-section"
                             style="margin-top:50px;">
                            <?php while (have_rows('payment_plan')) {
                                the_row(); ?>
                                <div class="offplan-special-ent">
                                    <div>
				        <span class="offplan-special-heading"><?php
                                echo the_sub_field('big_numbers');
                            ?></span>
                                        <span class="offplan-special-text"><?php ?><?php
                                                echo the_sub_field('description')
                                            ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <section class="p2-adout">

            <div class="container">
                <div class="p2-adout__wrapper flex-column flex-lg-row d-flex align-items-start justify-content-between">

                    <div class="p2-adout__text col-12 col-lg-6">
                        <h2 style="font-weight:700;">
                            <?php the_title(); ?>
                        </h2>
                        <?php the_content(); ?>
                    </div>
                    <div class="col-12 col-lg-5 p2-adout__video p-0">
                        <?php echo the_field('video'); ?>
                    </div>
                </div>
            </div>
        </section>
        
        <?php
        include(locate_template('single-developer-areas.php'));
        include(locate_template('single-developer-properties.php'));
    }
    get_footer();
?>
