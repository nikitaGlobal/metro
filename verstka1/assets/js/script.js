$('.sliderOffProject').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<div class="prev"><img class="mw-100" src="../assets/dev/img/btnSlick-left.png" alt=""></div>',
    nextArrow: '<div class="next"><img class="mw-100" src="../assets/dev/img/btnSlick-right.png" alt=""></div>',

    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },{
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$('.adoutSlider').slick({
    slidesToShow: 2 ,
    slidesToScroll: 1,
    arrows: false,
    dots: true,

});

$('.slider').slick({
    centerMode: true,
    centerPadding: '80px',
    slidesToShow: 3,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }
    ]
});



