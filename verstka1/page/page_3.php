<?php
require_once 'template/__header.php';
?>



<main>


<!--	f-screen begin-->
	<section class="p3-f-screen">
		<div class="container d-flex h-100 align-items-end">
			<div class="p3-f-screen__text col-12">

				<div class="p3-f-screen__title col-12 col-lg-8 col-xl-6">
					<h1 class="pb-4 text-uppercase">
						Yas Acres
					</h1>
					<div class="span mb-3">
						<span>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus beatae, cupiditate dolore labore necessitatibus nulla placeat quidem ut. Atque, esse!
						</span>
					</div>
					<div class="pt-4">
						<button class="mr-1">GET OFFER</button>
						<button class="p3-f-screen__title__btn-active ml-1">DOWNLOAD BROCHURE</button>
					</div>
				</div>

				<div class="p3-f-screen-info d-flex justify-content-around align-items-center">
					<div class="p3-f-screen-info__item col-4 col-lg-2">
						<h3>100%</h3>
						<span>
							ADM FREE WAIVER
						</span>
					</div>

					<div class="p3-f-screen-info__item d-flex align-items-center col-3 col-lg-2">
						<h3 class="h1 mr-3">3</h3>
						<span>
							Fixed Interest Rate for
							3 or 5 years
						</span>
					</div>
					<div class="p3-f-screen-info__item d-none d-sm-flex align-items-center col-3 col-lg-2">
						<h3 class="h1 mr-3">3</h3>
						<span>
							Fixed Interest Rate for
							3 or 5 years
						</span>
					</div>
					<div class="p3-f-screen-info__item align-items-center col-3 col-lg-2 d-none d-md-flex">
						<h3 class="h1 mr-3">3</h3>
						<span>
							Fixed Interest Rate for
							3 or 5 years
						</span>
					</div>
					<div class="p3-f-screen-info__item d-lg-flex align-items-center col-3 col-lg-2 d-none">
						<h3 class="h1 mr-3">3</h3>
						<span>
							Fixed Interest Rate for
							3 or 5 years
						</span>
					</div>


				</div>
			</div>
		</div>
	</section>
<!--	f-screen end-->


<!--	adout begin-->
	<section class="p3-adout">

		<div class="container">
			<div class="p3-adout__wrapper flex-column flex-lg-row d-flex align-items-center justify-content-between">

				<div class="p3-adout__text col-12 col-lg-6">
					<h2>
						Property Name
					</h2>
					<p class="pb-2">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aspernatur dolor id in magni odio provident quasi sit ullam voluptatum. Accusamus, assumenda deserunt ea, eos esse excepturi exercitationem expedita explicabo facilis in ipsa ipsam ipsum laborum maxime molestias nam officiis optio quasi quis quod reprehenderit temporibus tenetur totam ullam unde vel voluptas. Ad animi architecto blanditiis commodi deserunt ea in incidunt ipsum labore neque nihil officia perferendis qui, quidem quod rerum, saepe totam voluptatum? Alias amet debitis earum expedita illum itaque, magni mollitia nihil pariatur, praesentium quam quasi suscipit veniam. Distinctio eum hic ipsa minus officiis omnis porro, reiciendis sit.
					</p>
					<p class="pt-2">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aspernatur dolor id in magni odio provident quasi sit ullam voluptatum. Accusamus, assumenda deserunt ea, eos esse excepturi exercitationem expedita explicabo facilis in ipsa ipsam ipsum laborum maxime molestias nam officiis optio quasi quis quod reprehenderit temporibus tenetur totam ullam unde vel volupta
					</p>
				</div>
				<div class="col-12 col-lg-5 p3-adout__image pt-3 p-lg-0">
					<img class="mw-100" src="../assets/dev/img/adoutImage_page3.png" alt="">
				</div>


			</div>
		</div>
	</section>
<!--	adout end-->
<!--	video begin-->
	<section>
		<div class="container">
			<div class="">
				<iframe width="100%" height="600px"
						src="https://www.youtube.com/embed/CwU-OiS_PEQ"
						frameborder="0" allowfullscreen>
				</iframe>
			</div>
		</div>
	</section>
	<!--	video end-->

<!--	property begin-->
	<section class="p3-property">
		<div class="container-fluid d-flex align-items-center justify-content-between p-0">
			<div class="col-6 p-0 d-none d-lg-block">
				<img class="mw-100" src="../assets/dev/img/f-screen_page3.png" alt="">
			</div>
			<div class="p3-property__text col col-lg-4 mr-auto">
				<h2 class="mb-4 mb-lg-3 mb-xl-5 ml-0 ml-xl-5">
					Property Facilities
				</h2>
				<div class="row align-items-center justify-content-between text-center">


					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-1.png" alt="">
						<span class="mt-2">Playground</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-2.png" alt="">
						<span class="mt-2">12</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-3.png" alt="">
						<span class="mt-2">Beach & Sunset</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-4.png" alt="">
						<span class="mt-2">Beach & Sunset</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-5.png" alt="">
						<span class="mt-2">12</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-6.png" alt="">
						<span class="mt-2">Mosque</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-7.png" alt="">
						<span class="mt-2">12</span>
					</div>
					<div class="p3-property__text-item col-6 col-sm-3 flex-column align-items-center d-flex mb-3 mb-xl-5">
						<img class="mw-100" src="../assets/dev/img/p3-property-8.png" alt="">
						<span class="mt-2">12</span>
					</div>
				</div>
			</div>
		</div>
	</section>
<!--	property end-->

<!--	floor begin-->
	<section class="p3-floor">
		<div class="container">

			<div class="d-flex justify-content-center mb-5">
				<h2>
					Floor Plan
				</h2>
			</div>
			<div class="row justify-content-center ">
				<button class="m-1 btnActive">THREE BEDROOM SEMI-DETTACHED VILLA TYPE A</button>
				<button class="m-1">THREE BEDROOM TOWNHOUSE TYPE B</button>
				<button class="m-1">THREE BEDROOM TOWNHOUSE TYPE B</button>
				<button class="m-1 d-none d-md-block">Yas Acres Abu Dhabi. Three Bedroom Semi-Detached Villa Type B</button>
				<button class="m-1 d-none d-md-block">Four Bedroom Semi-Detached Villa Type A</button>
			</div>

			<div class="d-flex flex-column-reverse flex-lg-row justify-content-around pt-5">

				<div class="col-12 col-lg-4 d-flex flex-column pt-5">
					<h3>
						Three Bedroom Semi-Detached  Villa Type A
					</h3>
					<span class="py-4">
						Ground Floor / First Floor
					</span>
					<div>
						<button class="btnActive">
							INQUIRE
						</button>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<img class="mw-100" src="../assets/dev/img/p3-florImg.png" alt="">
				</div>
			</div>
		</div>
	</section>
<!--	floor end-->

<!--	slider begin-->
	<section class="pt-3">
		<div class="container-fluid">
			<div class="slider d-flex align-items-center">

				<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>
				<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>
				<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>
				<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>
				<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>	<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>
				<div class="px-1">
					<img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/f-screen_page3.png" alt="">
				</div>

			</div>
		</div>
	</section>
	<!--	slider end-->

<!--	payment begin-->
	<section class="p3-payment py-5">
		<div class="container">

			<div class="d-flex justify-content-center mb-4">
				<h2>
					Payment Plan
				</h2>
			</div>
			<div class="d-flex justify-content-center mb-4">
				<button class="m-1 btnActive">OPTION 1</button>
				<button class="m-1">OPTION 2</button>
			</div>

			<div class="p3-payment__wrapper row justify-content-around py-4">

				<div class="col-6 col-md-3 mb-3 mb-md-0">
					<div class="p3-payment__item w-100 d-flex flex-column align-items-center py-3">
						<span>
							1st Installment
						</span>
						<h3>
							5%
						</h3>
						<p>
							on booking
						</p>
					</div>
				</div>
				<div class="col-6 col-md-3 mb-3 mb-md-0">
					<div class="p3-payment__item w-100 d-flex flex-column align-items-center py-3">
						<span>
							1st Installment
						</span>
						<h3>
							5%
						</h3>
						<p>
							on booking
						</p>
					</div>
				</div>
				<div class="col-6 col-md-3 mb-3 mb-md-0">
					<div class="p3-payment__item w-100 d-flex flex-column align-items-center py-3">
						<span>
							1st Installment
						</span>
						<h3>
							5%
						</h3>
						<p>
							on booking
						</p>
					</div>
				</div>
				<div class="col-6 col-md-3 mb-3 mb-md-0">
					<div class="p3-payment__item w-100 d-flex flex-column align-items-center py-3">
						<span>
							1st Installment
						</span>
						<h3>
							5%
						</h3>
						<p>
							on booking
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<!--	payment end-->

<!--	aldar begin-->
	<section class="p3-aldar py-5">
		<div class="container d-flex justify-content-center align-items-center">

			<div class="pr-5 d-none d-lg-block">
				<img class="mw-100" src="../assets/dev/img/p3-aldar.png" alt="">
			</div>

			<div class="col-12 col-lg-5 d-flex flex-column">
				<div class="d-flex align-items-center">
					<div class="col-2 d-block d-lg-none">
						<img class="mw-100" src="../assets/dev/img/p3-aldar.png" alt="">
					</div>
					<h2>
						Aldar Properties
					</h2>
				</div>
				<span class="pt-2 pb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea eveniet fugiat id ipsam, quos reprehenderit sapiente temporibus. Iste, qui, tempora?
				</span>
				<div class="mw-100">
					<button>READ MORE</button>
				</div>
			</div>

		</div>
	</section>
<!--	aldar end-->
	<?
		include 'template/__map.php';
	?>
<!--	offer begin-->
	<section class="p3-offer">
		<div class="container d-flex justify-content-center justify-content-lg-between align-items-center">

			<div class="col col-sm-10 col-md-8 col-lg-5">
				<div class="p3-offer__title pb-4 mb-4">
					<h2 class="text-uppercase">
						GET SPECIAL OFFER
					</h2>
					<span>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, temporibus.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, temporibus.
					</span>
				</div>
				<form action="" class="d-flex flex-column">

					<label for="" class="mw-100">
						<input placeholder="Full Name" type="text">
					</label>
					<label for="" class="mw-100">
						<input placeholder="Phone with code" type="number">
					</label>
					<label for="" class="mw-100">
						<input placeholder="Email" type="email">
					</label>
					<select name="" id="">
						<option value="">How many bedrooms?</option>
						<option value="">How many bedrooms?</option>
						<option value="">How many bedrooms?</option>
					</select>
					<button type="submit">GET OFFER</button>
				</form>
			</div>
			<div class="p3-offer__img col-6 d-none d-lg-block">
				<img class="mw-100" src="../assets/dev/img/p3-offer.png" alt="">
			</div>
		</div>
	</section>
<!--	offer end-->

<!--	bg begim-->
	<section class="p3-bg">
		<div class="container">
			<div class="col col-md-8 col-lg-5">
				<h2>
					Yas Island
				</h2>
				<p class="pt-4 pb-5">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum earum eos numquam qui, quia sed tenetur totam voluptate. Et eum fuga inventore, iste iure nisi odit porro praesentium similique voluptatum.
				</p>
				<div>
					<button>Lear more</button>
				</div>
			</div>
		</div>
	</section>
<!--	bg end-->



    <?php
		include "template/__sliderOffProject.php";
		include "template/__sliderReadyProject.php";
    ?>
</main>





<?php
require_once 'template/__footer.php';
?>

</body>
</html>
