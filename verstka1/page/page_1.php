<?php
require_once 'template/__header.php';
?>

<main>
<!--	f-screen begin-->
<section class="p1-f-screen">
  <div class="container d-flex h-100 align-items-end">
    <div class="p1-f-screen__text w-100 d-flex flex-column flex-sm-row align-items-start  align-items-sm-center justify-content-between">
      <h1>Yas Island</h1>
      <button>
      KNOW MORE
      </button>

    <div class="p1-f-screen-info d-flex d-sm-none align-items-center justify-content-between">
      <div class="col-4">
        <h6>1.99%</h6>
        <span>
          Fixed Interest Rate for
          3 or 5 years
        </span>
      </div>
      <div class="col-4">
        <h6>1.99%</h6>
        <span>
        Fixed Interest Rate for
        3 or 5 years
        </span>
      </div>
      <div class="col-4">
        <h6>1.99%</h6>
        <span>
          Fixed Interest Rate for
          3 or 5 years
        </span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--	f-screen end-->
<!--	adout begin-->
<section class="p1-adout">

<div class="container">
<div class="p1-adout__wrapper flex-column flex-lg-row d-flex align-items-start justify-content-between">

<div class="p1-adout__text col-12 col-lg-6">
<h2>
  About Yas Island
</h2>
<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aspernatur dolor id in magni odio provident quasi sit ullam voluptatum. Accusamus, assumenda deserunt ea, eos esse excepturi exercitationem expedita explicabo facilis in ipsa ipsam ipsum laborum maxime molestias nam officiis optio quasi quis quod reprehenderit temporibus tenetur totam ullam unde vel voluptas. Ad animi architecto blanditiis commodi deserunt ea in incidunt ipsum labore neque nihil officia perferendis qui, quidem quod rerum, saepe totam voluptatum? Alias amet debitis earum expedita illum itaque, magni mollitia nihil pariatur, praesentium quam quasi suscipit veniam. Distinctio eum hic ipsa minus officiis omnis porro, reiciendis sit.
</p>
</div>
<div class="col-12 col-lg-5">
<h2 class="d-none d-lg-block">
  Main Attraction
</h2>

<div class="adoutSlider">
  <div>
      <div class="d-flex align-items-center mb-3">
          <img class="mw-100 mr-3" src="../assets/dev/img/aboutImage.png" alt="">
          <span>Ferrari World<br>Abu Dhabi</span>
      </div>
      <div class="d-flex align-items-center mt-3">
          <img class="mw-100 mr-3" src="../assets/dev/img/aboutImage.png" alt="">
          <span>Ferrari World<br>Abu Dhabi</span>
      </div>
  </div>
  <div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-3" src="../assets/dev/img/aboutImage.png" alt="">
          <span>Ferrari World<br>Abu Dhabi</span>
      </div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-3" src="../assets/dev/img/aboutImage.png" alt="">
          <span>Ferrari World<br>Abu Dhabi</span>
      </div>
  </div>
  <div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-3" src="../assets/dev/img/aboutImage.png" alt="">
          <span>Ferrari World<br>Abu Dhabi</span>
      </div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-3" src="../assets/dev/img/aboutImage.png" alt="">
          <span>Ferrari World<br>Abu Dhabi</span>
      </div>
  </div>
</div>
</div>

</div>

<div class="d-none d-lg-block">
<iframe width="100%" height="450px"
  src="https://www.youtube.com/embed/CwU-OiS_PEQ"
  frameborder="0" allowfullscreen>
</iframe>
</div>
</div>
</section>
<!--	adout end-->


<?
include 'template/__map.php';

  include "template/__sliderOffProject.php";
  include "template/__sliderReadyProject.php";
?>
</main>

<?php
require_once 'template/__footer.php';
?>

</body>
</html>
