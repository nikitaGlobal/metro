<!--offProject begin-->
<section class="p1-offProject">
  <div class="container">
    <div class="d-flex justify-content-center"">
      <h2>
	    Off-Plan projects in Yas Island
      </h2>
    </div>
    <div class="p1-offProject__wrapperBtn col-12 col-md-10 col-lg-7 col-xl-5 ml-auto mr-auto d-flex align-items-center justify-content-between">
      <button class="btnActive">EMAAR</button>
	  <button>MERAAS</button>
      <button>BLUE WATERS</button>
    </div>

    <div class="sliderOffProject">
      <div class="p1-offProject__item sliderItem">
        <div class="p1-offProject__item-img">
	      <img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
	      <span class="p1-offProject__item-text">RESIDENTIAL VILLAS</span>
		</div>
	<div class="d-flex align-items-end justify-content-between px-4 px-xl-0">
	<div>
		<h3>
			Yas Acres
		</h3>
		<span class="p1-offProject__item-span">
			Abu Dhabi
		</span>
	</div>
	<div class="mr-2">
		<img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
	</div>
</div>
</div>
<div class="p1-offProject__item sliderItem">
<div class="p1-offProject__item-img">
	<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
	<span class="p1-offProject__item-text">RESIDENTIAL VILLAS</span>
</div>
<div class="d-flex align-items-end justify-content-between px-4 px-xl-0">
	<div>
		<h3>
			Yas Acres
		</h3>
		<span class="p1-offProject__item-span">
					Abu Dhabi
				</span>
	</div>
	<div class="mr-2">
		<img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
	</div>
</div>
</div>
<div class="p1-offProject__item sliderItem">
<div class="p1-offProject__item-img">
	<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
	<span class="p1-offProject__item-text">RESIDENTIAL VILLAS</span>
</div>
<div class="d-flex align-items-end justify-content-between px-4 px-xl-0">
	<div>
		<h3>
			Yas Acres
		</h3>
		<span class="p1-offProject__item-span">
					Abu Dhabi
				</span>
	</div>
	<div class="mr-2">
		<img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
	</div>
</div>
</div>
<div class="p1-offProject__item sliderItem">
<div class="p1-offProject__item-img">
	<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
	<span class="p1-offProject__item-text">RESIDENTIAL VILLAS</span>
</div>
<div class="d-flex align-items-end justify-content-between px-4 px-xl-0">
	<div>
		<h3>
			Yas Acres
		</h3>
		<span class="p1-offProject__item-span">
					Abu Dhabi
				</span>
	</div>
	<div class="mr-2">
		<img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
	</div>
</div>
</div>
<div class="p1-offProject__item sliderItem">
<div class="p1-offProject__item-img">
	<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
	<span class="p1-offProject__item-text">RESIDENTIAL VILLAS</span>
</div>
<div class="d-flex align-items-end justify-content-between px-4 px-xl-0">
	<div>
		<h3>
			Yas Acres
		</h3>
		<span class="p1-offProject__item-span">
					Abu Dhabi
				</span>
	</div>
	<div class="mr-2">
		<img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
	</div>
</div>
</div>
</div>
</section>
<!--offProject end-->
