<!--readyProject begin-->
<section class="p1-readyProject">
  <div class="container">
    <div class="d-flex justify-content-center"">
      <h2>
        Ready Properties in Yas Island
      </h2>
    </div>
    <div class="p1-readyProject__wrapperBtn col-8 col-md-7 col-lg-5 col-xl-3 ml-auto mr-auto d-flex align-items-center justify-content-between">
      <button class="btnActive">APARTMENT</button>
      <button>VILLAS</button>
    </div>

  <div class="sliderOffProject">

    <div class="p1-readyProject__item sliderItem">
      <div class="p1-readyProject__item-img">
        <img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/project_1.png" alt="">
        <div class="p1-readyProject__item-text">
          <span class="p1-readyProject__item-left mr-2">RENT</span>
          <span class="p1-readyProject__item-right ml-2">VILLA</span>
      </div>
    </div>
    <div class="px-4 px-xl-2">
      <div>
        <h3>
          Lorem ipsum dolor sit amet.
        </h3>
      <div class="p1-readyProject__item-span pt-2">
        <span>
          Abu Dhabi
        </span>
      </div>
    </div>
    <div class="images d-flex align-items-center py-3">
      <div class="d-flex align-items-center mr-3">
        <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
        <span>Beds: 3</span>
      </div>
      <div class="d-flex align-items-center mr-3">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Baths: 4</span>
      </div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Sq Ft: 1838.00</span>
      </div>
    </div>
    <div class="d-flex align-items-center justify-content-between">
      <h5>
        AED 400,000
      </h5>
      <img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
      </div>
      </div>
    </div>

    <div class="p1-readyProject__item sliderItem">
      <div class="p1-readyProject__item-img">
        <img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/project_1.png" alt="">
          <div class="p1-readyProject__item-text">
            <span class="p1-readyProject__item-left mr-2">RENT</span>
            <span class="p1-readyProject__item-right ml-2">VILLA</span>
          </div>
        </div>
        <div class="px-4 px-xl-2">
          <div>
            <h3>
              Lorem ipsum dolor sit amet.
            </h3>
          <div class="p1-readyProject__item-span pt-2">
          <span>
            Abu Dhabi
          </span>
        </div>
    </div>

    <div class="images d-flex align-items-center py-3">
      <div class="d-flex align-items-center mr-3">
        <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
        <span>Beds: 3</span>
      </div>
      <div class="d-flex align-items-center mr-3">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Baths: 4</span>
      </div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Sq Ft: 1838.00</span>
      </div>
    </div>
    <div class="d-flex align-items-center justify-content-between">
      <h5>
        AED 400,000
      </h5>
      <img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
      </div>
      </div>
    </div>

    <div class="p1-readyProject__item sliderItem">
      <div class="p1-readyProject__item-img">
        <img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/project_1.png" alt="">
          <div class="p1-readyProject__item-text">
            <span class="p1-readyProject__item-left mr-2">RENT</span>
            <span class="p1-readyProject__item-right ml-2">VILLA</span>
          </div>
        </div>
        <div class="px-4 px-xl-2">
          <div>
            <h3>
              Lorem ipsum dolor sit amet.
            </h3>
          <div class="p1-readyProject__item-span pt-2">
          <span>
            Abu Dhabi
          </span>
        </div>
    </div>

    <div class="images d-flex align-items-center py-3">
      <div class="d-flex align-items-center mr-3">
        <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
        <span>Beds: 3</span>
      </div>
      <div class="d-flex align-items-center mr-3">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Baths: 4</span>
      </div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Sq Ft: 1838.00</span>
      </div>
    </div>
    <div class="d-flex align-items-center justify-content-between">
      <h5>
        AED 400,000
      </h5>
      <img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
      </div>
      </div>
    </div>

    <div class="p1-readyProject__item sliderItem">
      <div class="p1-readyProject__item-img">
        <img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/project_1.png" alt="">
          <div class="p1-readyProject__item-text">
            <span class="p1-readyProject__item-left mr-2">RENT</span>
            <span class="p1-readyProject__item-right ml-2">VILLA</span>
          </div>
        </div>
        <div class="px-4 px-xl-2">
          <div>
            <h3>
              Lorem ipsum dolor sit amet.
            </h3>
          <div class="p1-readyProject__item-span pt-2">
          <span>
            Abu Dhabi
          </span>
        </div>
    </div>

    <div class="images d-flex align-items-center py-3">
      <div class="d-flex align-items-center mr-3">
        <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
        <span>Beds: 3</span>
      </div>
      <div class="d-flex align-items-center mr-3">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Baths: 4</span>
      </div>
      <div class="d-flex align-items-center">
          <img class="mw-100 mr-2" src="../assets/dev/img/beds.png" alt="">
          <span>Sq Ft: 1838.00</span>
      </div>
    </div>
    <div class="d-flex align-items-center justify-content-between">
      <h5>
        AED 400,000
      </h5>
      <img class="mw-100" src="../assets/dev/img/whatsapp.png" alt="">
      </div>
      </div>
    </div>

    <div class="p1-readyProject__item sliderItem">
      <div class="p1-readyProject__item-img">
        <img class="mw-100 mr-auto ml-auto" src="../assets/dev/img/project_1.png" alt="">
          <div class="p1-readyProject__item-text">
            <span class="p1-readyProject__item-left mr-2">RENT</span>
            <span class="p1-readyProject__item-right ml-2">VILLA</span>
          </div>
        </div>
        <div class="px-4 px-xl-2">
          <div>
            <h3>
              Lorem ipsum dolor sit amet.
            </h3>
          <div class="p1-readyProject__item-span pt-2">
          <span>
            Abu Dhabi
          </span>
        </div>
    </div>

<!--readyProject end-->