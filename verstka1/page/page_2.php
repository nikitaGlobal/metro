<?php
require_once 'template/__header.php';
?>

<main>
<!--	f-screen begin-->
    <section class="p2-f-screen">
        <div class="container d-flex h-100 align-items-end">
            <div class="p2-f-screen__text col-12">

               <div class="d-flex align-items-lg-center align-items-start justify-content-start flex-column flex-lg-row">
				   <div class="d-flex align-items-start align-items-lg-center flex-column flex-lg-row">
					   <img class="mw-100 mr-5" src="../assets/dev/img/f-screen_page2_logo.png" alt="">
					   <h1>
						   ALDAR Properties
					   </h1>
				   </div>
				   <div class="ml-0 ml-lg-auto  mr-auto mr-lg-5">
					   <button>
						   KNOW MORE
					   </button>
				   </div>
			   </div>

                <div class="p2-f-screen-info d-flex align-items-center">
                    <div class="p2-f-screen-info__item col-4 col-lg-2">
                        <h3>1.99%</h3>
                        <span>
						Fixed Interest Rate for
						3 or 5 years
					</span>
                    </div>
					<div class="p2-f-screen-info__item col-4 col-lg-2">
						<h3>1.99%</h3>
                        <span>
						Fixed Interest Rate for
						3 or 5 years
					</span>
                    </div>
					<div class="p2-f-screen-info__item col-4 col-lg-2">
						<h3>1.99%</h3>
                        <span>
						Fixed Interest Rate for
						3 or 5 years
					</span>
                    </div>
					<div class="p2-f-screen-info__item col-2 d-none d-lg-block">
						<h3>1.99%</h3>
						<span>
						Fixed Interest Rate for
						3 or 5 years
					</span>
					</div>
                </div>
            </div>
        </div>
    </section>
	<!--f-screen end-->


<!--	adout begin-->
	<section class="p2-adout">

		<div class="container">
			<div class="p2-adout__wrapper flex-column flex-lg-row d-flex align-items-start justify-content-between">

				<div class="p2-adout__text col-12 col-lg-6">
					<h2>
						About Aldar Properties
					</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aspernatur dolor id in magni odio provident quasi sit ullam voluptatum. Accusamus, assumenda deserunt ea, eos esse excepturi exercitationem expedita explicabo facilis in ipsa ipsam ipsum laborum maxime molestias nam officiis optio quasi quis quod reprehenderit temporibus tenetur totam ullam unde vel voluptas. Ad animi architecto blanditiis commodi deserunt ea in incidunt ipsum labore neque nihil officia perferendis qui, quidem quod rerum, saepe totam voluptatum? Alias amet debitis earum expedita illum itaque, magni mollitia nihil pariatur, praesentium quam quasi suscipit veniam. Distinctio eum hic ipsa minus officiis omnis porro, reiciendis sit.
					</p>
				</div>
				<div class="col-12 col-lg-5 p2-adout__video p-0">
					<iframe width="100%" height="300px"
							src="https://www.youtube.com/embed/CwU-OiS_PEQ"
							frameborder="0" allowfullscreen>
					</iframe>
				</div>
			</div>
		</div>
	</section>
<!--	adout end-->
<!--	second begin-->
	<section class="p2-second">
		<div class="container">
			<div class="d-flex justify-content-center"">
			<h2 class="mb-5">
				Areas with Properties from Aldar
			</h2>
		</div>

		<div class="sliderOffProject">

			<div class="p2-second__item mx-1">
				<div>
					<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
				</div>
				<div class="d-flex align-items-end justify-content-between pt-2 px-4 px-xl-1">
					<div class="px-4 px-lg-0">
						<h3>
							Yas Acres
						</h3>
						<span class="p2-second__item-span">
							Abu Dhabi
						</span>
					</div>
				</div>
			</div>
			<div class="p2-second__item mx-1">
				<div>
					<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
				</div>
				<div class="d-flex align-items-end justify-content-between pt-2 px-4 px-xl-1">
					<div class="px-4 px-lg-0">
						<h3>
							Yas Acres
						</h3>
						<span class="p2-second__item-span">
							Abu Dhabi
						</span>
					</div>
				</div>
			</div>
			<div class="p2-second__item mx-1">
				<div>
					<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
				</div>
				<div class="d-flex align-items-end justify-content-between pt-2 px-4 px-xl-1">
					<div class="px-4 px-lg-0">
						<h3>
							Yas Acres
						</h3>
						<span class="p2-second__item-span">
							Abu Dhabi
						</span>
					</div>
				</div>
			</div>

			<div class="p2-second__item mx-1">
				<div>
					<img class="mw-100 ml-auto mr-auto" src="../assets/dev/img/project_1.png" alt="">
				</div>
				<div class="d-flex align-items-end justify-content-between pt-2 px-4 px-xl-1">
					<div class="px-4 px-lg-0">
						<h3>
							Yas Acres
						</h3>
						<span class="p2-second__item-span">
							Abu Dhabi
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
<!--	second end-->

    <?php
    include "template/__sliderOffProject.php";
    include "template/__sliderReadyProject.php";
    ?>
</main>

<?php
require_once 'template/__footer.php';
?>
</body>
</html>